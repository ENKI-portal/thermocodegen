# Copyright (C) 2018 Columbia University in the City of New York and others.
#
# Please see the AUTHORS file in the main source directory for a full list
# of contributors.
#
# This file is part of enki-codegen.
#
# enki-codegen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# enki-codegen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with enki-codegen. If not, see <http://www.gnu.org/licenses/>.
#


include "spud_base.rnc"
include "variable.rnc"
include "parameter.rnc"
include "function.rnc"


generic_free_energy_model =
  (
	## Free Energy model:  sets global variables (e.g. T,P for Gibbs Free energy, T,V, for Helmholtz)
	element free_energy_model {
	  attribute name { xsd:string},
	  generic_variable+,
	  generic_parameter+,
	  comment
	}
 )

gibbs_free_energy_model =
  (
	## Gibbs Free Energy model:  G(T,P)
	element free_energy_model {
	  attribute name { 'Gibbs' },
	  variable_T,
	  variable_P,
	  parameter_Tr,
	  parameter_Pr,
	  comment
	}
 )
 
 helmholtz_free_energy_model =
  (
	## Helmholtz Free Energy model:  A(T,V)
	element free_energy_model {
	  attribute name { 'Helmholtz' },
	  variable_T,
	  variable_V,
	  parameter_Tr,
	  parameter_Vr,
	  comment
	}
 )

phase_gibbs_free_energy_model =
  (
	## Gibbs Free Energy model:  G(T,P,n)
	element free_energy_model {
	  attribute name { 'Gibbs' },
	  variable_T,
	  variable_P,
	  variable_n,
	  function_mu_G,
	  comment
	}
 )

phase_helmholtz_free_energy_model =
  (
	## Helmholtz Free Energy model:  A(T,V,n)
	element free_energy_model {
	  attribute name { 'Helmholtz' },
	  variable_T,
	  variable_V,
	  variable_n,
	  function_mu_A,
	  comment
	}
 )

reference =
(
	##  string to hold reference information (url, doi, SHA) for
	element reference {
	   anystring,
	   comment
	}
)


expression =
(
	## Expression:  sympy expression, assumes sympy namespace as sym
	  element expression {
	  	  python_code
          }
)

residual =
(
	## Residual:  sympy expression F, such that F(u)=0, assumes sympy namespace as sym,  
	  element residual {
	  	  attribute name {'F'},
	  	  python_code
          }
)

generic_potential =
(
	element potential {
		attribute name { xsd:string },
		generic_parameter*,
		generic_function*,
		expression,
		reference?,
		comment
	}
)

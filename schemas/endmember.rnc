# Copyright (C) 2018 Columbia University in the City of New York and others.
#
# Please see the AUTHORS file in the main source directory for a full list
# of contributors.
#
# This file is part of enki-codegen.
#
# enki-codegen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# enki-codegen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with enki-codegen. If not, see <http://www.gnu.org/licenses/>.

# ALL dependencies need to be specified here
# always included when using with spud package:
include "spud_base.rnc"

# direct dependencies of this file
#include "potential.rnc"

# indirect dependencies of the direct dependencies:
#include "spud_extension.rnc"
include "variable.rnc"
include "parameter.rnc"
include "function.rnc"
include "potentials.rnc"


start =
   (
      ## The root node of the options dictionary for EndMembers.
      ##
      ## When viewed in diamond, options which are shaded blue are incomplete and require input.  Greyed out options are optional.  Black options are active.
      element endmember_options {
      	endmember_base,
	 	free_energy_model,
	 	global_parameters,
	 	global_functions,
        # in potential.rnc
        generic_potential*,
        comment
      }
   )

endmember_base = 
(
     ## an endmember name, must be unique  across all end-members in the data-set
     element name {
     	     attribute name { xsd:string },
	     comment
     },
     ## Chemical Composition of the EndMember 
     element formula {
       	  attribute name { xsd:string },
	  comment
       },
     reference
)

free_energy_model =
(
	( gibbs_free_energy_model |  helmholtz_free_energy_model | generic_free_energy_model )
)



global_functions =
   (
     ## Global parameters: optional parameters that are available to all energy potentials
     element functions {
       ## global parameters available to all functions below
       implicit_function*,
       external_function*,
       comment
     }
   )

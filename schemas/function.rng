<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0" xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <!--
    Copyright (C) 2018 Columbia University in the City of New York and others.
    
    Please see the AUTHORS file in the main source directory for a full list
    of contributors.
    
    This file is part of enki-codegen.
    
    enki-codegen is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    enki-codegen is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with enki-codegen. If not, see <http://www.gnu.org/licenses/>.
    
  -->
  <include href="spud_base.rng"/>
  <include href="potentials.rng"/>
  <include href="variable.rng"/>
  <define name="explicit">
    <element name="type">
      <a:documentation>Explicit Function: &lt;symbol&gt; = Expression(variables)</a:documentation>
      <attribute name="name">
        <value>explicit</value>
      </attribute>
      <ref name="expression"/>
    </element>
  </define>
  <define name="implicit">
    <element name="type">
      <a:documentation>Implicit Function:
	    if scalar: &lt;symbol&gt; = sym.Function(variables)
	    if vector: &lt;symbol&gt; = sym.MatrixSymbol(size)?

                Residual(&lt;symbol&gt;) = 0.
                optional sympy expressions  for initial guess and/or bounds</a:documentation>
      <attribute name="name">
        <value>implicit</value>
      </attribute>
      <choice>
        <ref name="scalar"/>
        <ref name="vector"/>
      </choice>
      <ref name="residual"/>
      <optional>
        <element name="initial_guess">
          <a:documentation>Initial_Guess:  Optional sympy expression, assumes sympy namespace as sym and is a function of base variables and parameters</a:documentation>
          <ref name="python_code"/>
        </element>
      </optional>
      <optional>
        <element name="bounds">
          <a:documentation>Bounds: Optional Boolean sympy expression that evaluates if solution is in bounds</a:documentation>
          <ref name="python_code"/>
        </element>
      </optional>
    </element>
  </define>
  <!-- careful...apparently external is a key word here -->
  <define name="external_func">
    <element name="type">
      <a:documentation>External Function:  an externally derived function that depends on defined variables

 these symbols must be resolved by the parsing scripts

 examples include  mu(T,P) (chemical potentials defined elsewhere) or Debye(x)
 can be scalar or vector valued</a:documentation>
      <attribute name="name">
        <value>external</value>
      </attribute>
      <choice>
        <ref name="scalar"/>
        <ref name="vector"/>
      </choice>
      <oneOrMore>
        <choice>
          <ref name="variable_T"/>
          <ref name="variable_P"/>
          <ref name="variable_V"/>
          <ref name="generic_variable"/>
        </choice>
      </oneOrMore>
      <ref name="comment"/>
    </element>
  </define>
  <define name="implicit_function">
    <a:documentation>New coder based unrolled implicit and external functions</a:documentation>
    <element name="implicit_function">
      <a:documentation>Implicit Function:  a scalar  implicit function that is resolved with a non-linear solve
each function has:
    a name (e.g. V)
	   symbol (e.g. V(T,P) = sym.Function('name')(T,P)
	   residual:  a function f(name, variables) such that solutions are roots f(0)
	   initial guess:  a sympy expression for an initial guess</a:documentation>
      <attribute name="name">
        <data type="string"/>
      </attribute>
      <ref name="expression"/>
      <element name="residual">
        <attribute name="name">
          <data type="string"/>
        </attribute>
        <ref name="expression"/>
        <ref name="comment"/>
      </element>
      <element name="initial_guess">
        <attribute name="name">
          <data type="string"/>
        </attribute>
        <ref name="expression"/>
        <ref name="comment"/>
      </element>
      <ref name="comment"/>
    </element>
  </define>
  <define name="ordering_function">
    <a:documentation>New coder based order_function for complex solutions.</a:documentation>
    <element name="ordering_function">
      <a:documentation>Ordering Function:  a vector valued non-linear function   needed to resolve local order-disorder
equilibria through a non-linear solve
each function has:
    a name
	   dependent variable  (e.g. s) as sympy expression
	   residual:  a vector valued function F[s] (list of sympy expressions) st. solutions have F(s) = 0
	   initial guess:  a sympy expression for an initial guess
	   bounds:  a sympy boolean and expression to determine if the variable is in permissible bounds</a:documentation>
      <attribute name="name">
        <data type="string"/>
      </attribute>
      <ref name="variable_s"/>
      <element name="residual">
        <attribute name="name">
          <data type="string"/>
        </attribute>
        <ref name="expression"/>
        <ref name="comment"/>
      </element>
      <element name="guess">
        <attribute name="name">
          <data type="string"/>
        </attribute>
        <ref name="expression"/>
        <ref name="comment"/>
      </element>
      <element name="bounds">
        <attribute name="name">
          <data type="string"/>
        </attribute>
        <ref name="expression"/>
        <ref name="comment"/>
      </element>
      <ref name="comment"/>
    </element>
  </define>
  <define name="external_function">
    <element name="external_function">
      <a:documentation>External Function:  an externally derived function that depends on defined variables

 these symbols must be resolved by the parsing scripts

 examples include  mu(T,P) (chemical potentials defined elsewhere) or Debye(x)
 can be scalar or vector valued</a:documentation>
      <attribute name="name">
        <data type="string"/>
      </attribute>
      <choice>
        <ref name="scalar"/>
        <ref name="vector"/>
      </choice>
      <oneOrMore>
        <choice>
          <ref name="variable_T"/>
          <ref name="variable_P"/>
          <ref name="variable_V"/>
          <ref name="generic_variable"/>
        </choice>
      </oneOrMore>
      <ref name="comment"/>
    </element>
  </define>
  <define name="generic_function">
    <element name="function">
      <a:documentation>Function:  Sympy expression: assuming namespace sym to describe function that depends on global variables f(T,P) e.g and local/global parameters
Functions can be either  implicit  or explicit or external
All functions have a name, an optional sympy symbol
explict and implicit functions also have an expression (unless external functions)</a:documentation>
      <attribute name="name">
        <data type="string"/>
      </attribute>
      <choice>
        <ref name="explicit"/>
        <ref name="implicit"/>
        <ref name="external_func"/>
      </choice>
      <optional>
        <ref name="generic_symbol">
          <a:documentation>  sympy expression that either evaluates to the function of interest (explicit)
 or is an implicit function of the function that evaluates to zero for the correct value (requires a root solver)
 Optional sympy symbol for use with this function, otherwise will default to the function name</a:documentation>
        </ref>
      </optional>
      <optional>
        <ref name="reference"/>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
  <define name="function_mu_G">
    <element name="function">
      <a:documentation>reserved symbol for vector of chemical potentials of endmembers mu(T,P) or mu(T,V)
size will be set by parser from number  of endmembers K</a:documentation>
      <attribute name="name">
        <value>mu</value>
      </attribute>
      <element name="type">
        <attribute name="name">
          <value>external</value>
        </attribute>
        <ref name="vector_K"/>
        <ref name="variable_T"/>
        <ref name="variable_P"/>
        <ref name="comment"/>
      </element>
      <optional>
        <ref name="generic_symbol">
          <a:documentation> Optional sympy symbol for use with this function</a:documentation>
        </ref>
      </optional>
      <optional>
        <ref name="reference"/>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
  <define name="function_mu_A">
    <element name="function">
      <a:documentation>reserved symbol for vector of chemical potentials of endmembers mu(T,P) or mu(T,V)
size will be set by parser from number  of endmembers K</a:documentation>
      <attribute name="name">
        <value>mu</value>
      </attribute>
      <element name="type">
        <attribute name="name">
          <value>external</value>
        </attribute>
        <ref name="vector_K"/>
        <ref name="variable_T"/>
        <ref name="variable_V"/>
        <ref name="comment"/>
      </element>
      <optional>
        <ref name="generic_symbol">
          <a:documentation> Optional sympy symbol for use with this function</a:documentation>
        </ref>
      </optional>
      <optional>
        <ref name="reference"/>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
  <define name="function_A">
    <element name="function">
      <a:documentation>reserved symbol for vector of affinities A.
Size will be set by parser from number of reactions J</a:documentation>
      <attribute name="name">
        <value>A</value>
      </attribute>
      <element name="type">
        <attribute name="name">
          <value>external</value>
        </attribute>
        <ref name="vector_J"/>
        <ref name="variable_T"/>
        <ref name="variable_P"/>
        <ref name="variable_C"/>
        <ref name="comment"/>
      </element>
      <optional>
        <ref name="generic_symbol">
          <a:documentation>  sympy expression that either evaluates to the function of interest (explicit)
 or is an implicit function of the function that evaluates to zero for the correct value (requires a root solver)
 Optional sympy symbol for use with this function, otherwise will default to the function name</a:documentation>
        </ref>
      </optional>
      <optional>
        <ref name="reference"/>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
  <define name="function_rho">
    <element name="function">
      <a:documentation>reserved symbol for vector of densities of products for each reaction.
Size will be set by parser from number of reactions J</a:documentation>
      <attribute name="name">
        <value>rho</value>
      </attribute>
      <element name="type">
        <attribute name="name">
          <value>external</value>
        </attribute>
        <ref name="vector_J"/>
        <ref name="variable_T"/>
        <ref name="variable_P"/>
        <ref name="comment"/>
      </element>
      <optional>
        <ref name="generic_symbol">
          <a:documentation>  sympy expression that either evaluates to the function of interest (explicit)
 or is an implicit function of the function that evaluates to zero for the correct value (requires a root solver)
 Optional sympy symbol for use with this function, otherwise will default to the function name</a:documentation>
        </ref>
      </optional>
      <optional>
        <ref name="reference"/>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
</grammar>

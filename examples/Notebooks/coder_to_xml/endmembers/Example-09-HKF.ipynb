{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Revised HKF - Standard State\n",
    "\n",
    "**This notebook develops code for the revised HKF (Helgeson-Kirkham-Flowers) formalism for the thermodynamic properties of aqueous species.**\n",
    "\n",
    "It is a variant of Example 3 but uses the new features of StdStateModel\n",
    "\n",
    "The derivation follows that in Appendix B of  \n",
    "\n",
    "Shock EL, Oelkers EH, Johnson JW, Sverjensky DA, Helgeson HC (1992) Calculation of the thermodynamic properties of aqueous species at high pressures and temperatures. Journal of the Chemical Society Faraday Transactions, 88(6), 803-826  \n",
    "\n",
    "and in  \n",
    "\n",
    "Tanger JC, Helgeson HC (1988) Calculation of the thermodynamic and transport properties of aqueous species at high pressures and temperatures: Revised equations of state for the standard partial molal properties if ions and electrolytes. American Journal of Science, 288, 19-98\n",
    "\n",
    "Required system packages and initialization  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import sympy as sym\n",
    "import os\n",
    "import hashlib\n",
    "import time\n",
    "sym.init_printing()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from thermocodegen.coder import coder"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set a reference string for this Notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reference = 'Thermocodegen-v0.6/share/thermocodegen/examples/Notebooks/coder_to_xml/endmembers/Example-09-HKF.ipynb'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### let's set up some directory names for clarity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HOME_DIR = os.path.abspath(os.curdir)\n",
    "SPUD_DIR = HOME_DIR+'/spudfiles_custom'\n",
    "\n",
    "try:\n",
    "    os.mkdir(SPUD_DIR)\n",
    "except:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "examine the external functions supported by coder (here we will need the born functions B,Y,Q,gSolvent)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "external_functions = coder.get_external_functions()\n",
    "coder.list_external_functions()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a standard state model instance ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = coder.StdStateModel.from_type()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and retrieve sympy symbols for standard variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = model.get_symbol_for_t()\n",
    "P = model.get_symbol_for_p()\n",
    "Tr = model.get_symbol_for_tr()\n",
    "Pr = model.get_symbol_for_pr()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Equation of State (EOS)\n",
    "Shock et al., 1992, eqn. B10:   \n",
    "\n",
    "${\\bar v^o} = {a_1} + \\frac{{{a_2}}}{{\\Psi  + P}} + \\left( {{a_3} + \\frac{{{a_4}}}{{\\Psi  + P}}} \\right)\\frac{1}{{T - \\theta }} - \\omega Q - \\left( {B + 1} \\right){\\left( {\\frac{{\\partial \\omega }}{{\\partial P}}} \\right)_T}$  \n",
    "\n",
    "$\\Psi$ has a value of 2600 bars as determined by Tanger and Helgeson (1988)  \n",
    "$\\theta$ has a value of 228 K as determined by Helgeson and Kirkham (1976)  \n",
    "\n",
    "$\\omega$ is defined in terms of the charge on the ion (*z*) and a mysterious function, *g*, as derived by Shock et al. (1992),building on the work of Tanger and Helgeson (1988):  \n",
    "\n",
    "$\\omega  = \\eta z\\left( {\\frac{z}{{{r_{e,ref}} + \\left| z \\right|g}} - \\frac{1}{{{r_{e,{H^ + }}} + g}}} \\right)$  \n",
    "\n",
    "where $\\eta$ is a conversion constant.  The reference ionic radius is:\n",
    "\n",
    "${r_{e,ref}} = \\frac{{{z^2}}}{{\\frac{{{\\omega _{ref}}}}{\\eta } + \\frac{z}{{{r_{e,{H^ + }}}}}}}$   \n",
    "\n",
    "which when inserted into the definition becomes  \n",
    "\n",
    "$\\omega  = \\left( {\\frac{{{\\omega _{ref}} + \\frac{{z\\eta }}{{{r_{e,{H^ + }}}}}}}{{1 + \\frac{{\\sqrt {{z^2}} }}{{{z^2}}}g\\left( {\\frac{{{\\omega _{ref}}}}{\\eta } + \\frac{z}{{{r_{e,{H^ + }}}}}} \\right)}} - \\frac{{\\eta z}}{{{r_{e,{H^ + }}} + g}}} \\right)$  \n",
    "\n",
    "The above expression is cast into a slightly different form than the one provided by Shock et al. (1992), but is otherwise identical. This form is applicable to both charged and neutral species, as demonstrated below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "B = external_functions['B']\n",
    "Q = external_functions['Q']\n",
    "gSolvent = external_functions['gSolvent']\n",
    "eta, rH, omega0, z = sym.symbols('eta rH omega0 z', real=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega_def = sym.Piecewise((omega0, sym.Eq(z,0)), \\\n",
    "            ((omega0 + z*eta/rH)/(1 + sym.Abs(z)*gSolvent(T,P)*(omega0/eta + z/rH)/z**2) - eta*z/(rH + gSolvent(T,P)), True))\n",
    "omega_def"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hence, ${\\bar v^o} = {a_1} + \\frac{{{a_2}}}{{\\Psi  + P}} + \\left( {{a_3} + \\frac{{{a_4}}}{{\\Psi  + P}}} \\right)\\frac{1}{{T - \\theta }} - \\omega Q - \\left( {B + 1} \\right){\\left( {\\frac{{\\partial \\omega }}{{\\partial P}}} \\right)_T}$ is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a1,a2,a3,a4 = sym.symbols('a1 a2 a3 a4')\n",
    "Psi,theta = sym.symbols('Psi theta')\n",
    "omega = sym.Function('omega')(T,P)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vtp = a1 + a2/(Psi+P) + (a3 + a4/(Psi+P))/(T-theta) - omega*Q(T,P) - (B(T,P)+1)*omega.diff(P)\n",
    "vtp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the derivative of $ - \\left[ {\\left( {{B_{T,P}} + 1} \\right){\\omega _{T,P}} - \\left( {{B_{T,{P_r}}} + 1} \\right){\\omega _{T,{P_r}}}} \\right]$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "-(1+B(T,P))*omega + (1+B(T,Pr))*(omega.subs(P,Pr))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(-(1+B(T,P))*omega + (1+B(T,Pr))*(omega.subs(P,Pr))).diff(P)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So that $\\int_{{P_r}}^P {{{\\bar v}^o}} dP$ may be written:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "deltaG = sym.integrate(a1 + a2/(Psi+P) + (a3 + a4/(Psi+P))/(T-theta), (P,Pr,P)) - (1+B(T,P))*omega + (1+B(T,Pr))*omega.subs(P,Pr)\n",
    "deltaG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second derivative of this volume integral gives the contribution to the heat capacity, i.e. $\\frac{{{\\partial ^2}G}}{{\\partial {T^2}}} =  - \\frac{{\\partial S}}{{\\partial T}} =  - \\frac{{{C_P}}}{T}$ so that  \n",
    "\n",
    "${{\\bar c}_P}\\left( {T,P} \\right) - {{\\bar c}_P}\\left( {T,{P_r}} \\right) =  - T\\frac{{{\\partial ^2}\\int_{{P_r}}^P {{{\\bar v}^o}} dP}}{{\\partial {T^2}}}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "-T*deltaG.diff(T,2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Heat Capacity functions\n",
    "Heat capacity is parameterized as (Tanger and Helgeson, 1988, eq A-4, p 78):\n",
    "\n",
    "${{\\bar c}_P} = {c_1} + \\frac{{{c_2}}}{{{{\\left( {T - \\theta } \\right)}^2}}} - \\left[ {\\frac{{2T}}{{{{\\left( {T - \\theta } \\right)}^3}}}} \\right]\\left[ {{a_3}\\left( {P - {P_r}} \\right) + {a_4}\\ln \\left( {\\frac{{\\Psi  + P}}{{\\Psi  + {P_r}}}} \\right)} \\right] + \\omega TX + 2TY\\frac{{\\partial \\omega }}{{\\partial T}} - T\\left( {\\frac{1}{\\varepsilon } - 1} \\right)\\frac{{{\\partial ^2}\\omega }}{{\\partial {T^2}}}$  \n",
    "\n",
    "or equivalently, as the Born function is defined as $B =  - \\frac{1}{\\varepsilon }$  \n",
    "\n",
    "${{\\bar c}_P} = {c_1} + \\frac{{{c_2}}}{{{{\\left( {T - \\theta } \\right)}^2}}} - \\left[ {\\frac{{2T}}{{{{\\left( {T - \\theta } \\right)}^3}}}} \\right]\\left[ {{a_3}\\left( {P - {P_r}} \\right) + {a_4}\\ln \\left( {\\frac{{\\Psi  + P}}{{\\Psi  + {P_r}}}} \\right)} \\right] + \\omega TX + 2TY\\frac{{\\partial \\omega }}{{\\partial T}} + T\\left( {B + 1} \\right)\\frac{{{\\partial ^2}\\omega }}{{\\partial {T^2}}}$  \n",
    "\n",
    "at the reference pressure this expression becomes  \n",
    "\n",
    "${\\bar c_{{P_r}}} = {c_1} + \\frac{{{c_2}}}{{{{\\left( {T - \\theta } \\right)}^2}}} + {\\omega _{{P_r}}}T{X_{T,{P_r}}} + 2T{Y_{T,{P_r}}}{\\left. {\\frac{{\\partial \\omega }}{{\\partial T}}} \\right|_{T,{P_r}}} + T\\left( {{B_{T,{P_r}}} + 1} \\right){\\left. {\\frac{{{\\partial ^2}\\omega }}{{\\partial {T^2}}}} \\right|_{T,{P_r}}}$  \n",
    "\n",
    "Note that the \"Born\" function terms are the equivalent of $T{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|_{T,{P_r}}}$, so that the reference pressure heat capacity can be equivalently written:  \n",
    "\n",
    "${{\\bar c}_{{P_r}}} = {c_1} + \\frac{{{c_2}}}{{{{\\left( {T - \\theta } \\right)}^2}}} + T{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|_{T,{P_r}}}$\n",
    "\n",
    "as demonstrated here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c1,c2 = sym.symbols('c1 c2')\n",
    "ctpr = c1 + c2/(T-theta)**2 + (T*((B(T,P)+1)*omega).diff(T,2)).subs(P,Pr)\n",
    "ctpr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gibbs free energy\n",
    "The above analysis gives a way to write the Gibbs free energy using the identity $dG =  - SdT + VdP$, from which:  \n",
    "\n",
    "${G_{T,P}} = {G_{{T_r},{P_r}}} - \\int_{{T_r}}^T {{S_{T,{P_r}}}dT}  + \\int_{{P_r}}^P {{V_{T,P}}dP}$  \n",
    "\n",
    "The entropy term is given by  \n",
    "\n",
    "${S_{T,{P_r}}} = {S_{{T_r},{P_r}}} + \\int_{{T_r}}^T {\\frac{{{{\\bar c}_{T,{P_r}}}}}{T}dT}$  \n",
    "\n",
    "Combining expressions  \n",
    "\n",
    "${G_{T,P}} = {G_{{T_r},{P_r}}} - {S_{{T_r},{P_r}}}\\left( {T - {T_r}} \\right) - \\int_{{T_r}}^T {\\int_{{T_r}}^T {\\frac{{{{\\bar c}_{T,{P_r}}}}}{T}dT} dT}  + \\int_{{P_r}}^P {{V_{T,P}}dP}$  \n",
    "\n",
    "Now, using the above expression for the reference pressure heat capacity we can write  \n",
    "\n",
    "${G_{T,P}} = {G_{{T_r},{P_r}}} - {S_{{T_r},{P_r}}}\\left( {T - {T_r}} \\right) - \\int_{{T_r}}^T {\\int_{{T_r}}^T {\\frac{{\\left( {{c_1} + \\frac{{{c_2}}}{{{{\\left( {T - \\theta } \\right)}^2}}} + T{{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}} \\right)}}{T}dT} dT}  + \\int_{{P_r}}^P {{V_{T,P}}dP}$  \n",
    "\n",
    "or  \n",
    "\n",
    "${G_{T,P}} = {G_{{T_r},{P_r}}} - {S_{{T_r},{P_r}}}\\left( {T - {T_r}} \\right) - \\int_{{T_r}}^T {\\int_{{T_r}}^T {\\left[ {\\frac{{{c_1}}}{T} + \\frac{{{c_2}}}{{T{{\\left( {T - \\theta } \\right)}^2}}} + {{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}} \\right]dT} dT}  + \\int_{{P_r}}^P {{V_{T,P}}dP}$  \n",
    "\n",
    "which expands to  \n",
    "\n",
    "${G_{T,P}} = {G_{{T_r},{P_r}}} - {S_{{T_r},{P_r}}}\\left( {T - {T_r}} \\right) - \\int_{{T_r}}^T {\\int_{{T_r}}^T {\\left[ {\\frac{{{c_1}}}{T} + \\frac{{{c_2}}}{{T{{\\left( {T - \\theta } \\right)}^2}}}} \\right]dT} dT}  - \\int_{{T_r}}^T {\\int_{{T_r}}^T {{{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}dT} dT}  + \\int_{{P_r}}^P {{V_{T,P}}dP}$  \n",
    "\n",
    "Note that $\\int_{{T_r}}^T {\\int_{{T_r}}^T {{{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}dT} dT}$ evaluates to:  \n",
    "\n",
    "$\\int_{{T_r}}^T {\\int_{{T_r}}^T {{{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}dT} dT}  = \\int_{{T_r}}^T {\\left[ {{{\\left. {\\frac{{\\partial \\left( {B + 1} \\right)\\omega }}{{\\partial T}}} \\right|}_{T,{P_r}}} - {{\\left. {\\frac{{\\partial \\left( {B + 1} \\right)\\omega }}{{\\partial T}}} \\right|}_{{T_r},{P_r}}}} \\right]dT}$  \n",
    "\n",
    "and further to:  \n",
    "\n",
    "$\\int_{{T_r}}^T {\\int_{{T_r}}^T {{{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}dT} dT}  = \\left( {{B_{T,{P_r}}} + 1} \\right){\\omega _{T,P_r}} - \\left( {{B_{{T_r},{P_r}}} + 1} \\right){\\omega _{{T_r},{P_r}}} - {\\left. {\\frac{{\\partial \\left( {B + 1} \\right)\\omega }}{{\\partial T}}} \\right|_{{T_r},{P_r}}}\\left( {T - {T_r}} \\right)$  \n",
    "\n",
    "and still further to:  \n",
    "\n",
    "$\\int_{{T_r}}^T {\\int_{{T_r}}^T {{{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}dT} dT}  = \\left( {{B_{T,{P_r}}} + 1} \\right){\\omega _{T,{P_r}}} - \\left( {{B_{{T_r},{P_r}}} + 1} \\right){\\omega _{{T_r},{P_r}}} - {\\left. {\\frac{{\\partial B}}{{\\partial T}}} \\right|_{{T_r},{P_r}}}{\\omega _{{T_r},{P_r}}}\\left( {T - {T_r}} \\right) - {\\left. {\\left( {{B_{{T_r},{P_r}}} + 1} \\right)\\frac{{\\partial \\omega }}{{\\partial T}}} \\right|_{{T_r},{P_r}}}\\left( {T - {T_r}} \\right)$  \n",
    "\n",
    "recognizing that $Y = \\frac{{\\partial B}}{{\\partial T}}$, we have finally  \n",
    "\n",
    "$\\int_{{T_r}}^T {\\int_{{T_r}}^T {{{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}dT} dT}  = \\left( {{B_{T,{P_r}}} + 1} \\right){\\omega _{T,{P_r}}} - \\left( {{B_{{T_r},{P_r}}} + 1} \\right){\\omega _{{T_r},{P_r}}} - {Y_{{T_r},{P_r}}}\\left( {T - {T_r}} \\right){\\omega _{{T_r},{P_r}}} - {\\left. {\\left( {{B_{{T_r},{P_r}}} + 1} \\right)\\left( {T - {T_r}} \\right)\\frac{{\\partial \\omega }}{{\\partial T}}} \\right|_{{T_r},{P_r}}}$  \n",
    "\n",
    "Note that:  \n",
    "\n",
    "$ - \\int_{{T_r}}^T {\\int_{{T_r}}^T {{{\\left. {\\frac{{{\\partial ^2}\\left( {B + 1} \\right)\\omega }}{{\\partial {T^2}}}} \\right|}_{T,{P_r}}}dT} dT}  =  - \\left( {{B_{T,{P_r}}} + 1} \\right){\\omega _{T,{P_r}}} + \\left( {{B_{{T_r},{P_r}}} + 1} \\right){\\omega _{{T_r},{P_r}}} + {Y_{{T_r},{P_r}}}\\left( {T - {T_r}} \\right){\\omega _{{T_r},{P_r}}} + {\\left. {\\left( {{B_{{T_r},{P_r}}} + 1} \\right)\\left( {T - {T_r}} \\right)\\frac{{\\partial \\omega }}{{\\partial T}}} \\right|_{{T_r},{P_r}}}$  \n",
    "\n",
    "and\n",
    "\n",
    "$\\int_{{P_r}}^P {{V_{T,P}}dP}  = f\\left( {T,\\theta ,{a_1},{a_2},{a_3},{a_4},T,P,{P_r}} \\right) - \\left( {{B_{T,P}} + 1} \\right){\\omega _{T,P}} + \\left( {{B_{T,{P_r}}} + 1} \\right){\\omega _{T,{P_r}}}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Add to the above parameters the reference state enthalpy, $H_{ref}$ and the reference state entropy, $S_{ref}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Gref,Sref = sym.symbols('G_ref S_ref')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  Derive an expression for the Gibbs free energy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Y = external_functions['Y']\n",
    "gtp = Gref - Sref*(T-Tr) - sym.integrate(sym.integrate((c1 + c2/(T-theta)**2)/T, (T,Tr,T)), (T,Tr,T)) \\\n",
    "    - (B(T,Pr)+1)*omega.subs(P,Pr) + (B(Tr,Pr)+1)*omega.subs(P,Pr).subs(T,Tr) \\\n",
    "    + Y(Tr,Pr)*(T-Tr)*omega.subs(P,Pr).subs(T,Tr) + (B(Tr,Pr)+1)*(T-Tr)*omega.diff(T).subs(P,Pr) \\\n",
    "    + sym.integrate(a1 + a2/(Psi+P) + (a3 + a4/(Psi+P))/(T-theta), (P,Pr,P)) - (1+B(T,P))*omega + (1+B(T,Pr))*omega.subs(P,Pr)\n",
    "gtp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the reference pressure, $\\omega$ is a constant, $\\omega_0$, and its derivatives are zero too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gtp = gtp.subs(omega.subs(T,Tr).subs(P,Pr), omega0)\n",
    "gtp = gtp.subs(omega.diff(T).subs(P,Pr),0)\n",
    "gtp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Substitute the definition of the omega function into the expression for the Gibbs free energy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gtp = gtp.subs(omega,omega_def)\n",
    "gtp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The \"charge\" contribution to the Gibbs free energy is "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gtp-gtp.subs(z,0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Add the derived expression for *G(T,P)* and its parameter list to the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params = [('G_ref','J',Gref), ('S_ref','J/K',Sref), \n",
    "          ('a1','J/bar-m',a1), ('a2','J/bar^2-m',a2), ('a3','J/bar-m',a3),  ('a4','J/bar^2-m',a4), \n",
    "          ('c1','J/K-m',c1), ('c2','J-K/m',c2), \n",
    "          ('Psi', 'bar', Psi), ('eta', 'Å-J/mole', eta), ('rH', 'Å', rH), ('omega0','J',omega0), \n",
    "          ('theta','K',theta), ('z', '', z)]\n",
    "model.add_potential_to_model('G_hk',gtp, params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save the model to a spud file and Code Print the Model\n",
    "Get/Set parameter values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.get_values()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define a parameter dictionary for a specific aqueous species. Parameters from DEW."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "use_charged_species_example = True\n",
    "\n",
    "if use_charged_species_example:\n",
    "    phase_name = \"Na_1_cation_hkf\"\n",
    "    species     = 'Na+'\n",
    "    formula     = 'Na(1)'\n",
    "    DEW_g       = -62591.0     #       cal/mol\n",
    "    DEW_h       = -57433.0     #       cal/mol\n",
    "    DEW_s       = 13.96        #       cal/mol/K\n",
    "    DEW_v       = -1.106808692 #       cm^3/mol\n",
    "    DEW_cp      = 9.076552599  #       cal/mol/K\n",
    "    DEW_omega   = 0.3306       # 10^-5 cal/mol\n",
    "    DEW_z       = 1.0          #       charge\n",
    "    DEW_a1      = 1.839        # 10    cal/mol/bar\n",
    "    DEW_a2      = -2.285       # 10^-2 cal/mol\n",
    "    DEW_a3      = 3.256        #       cal-K/mol/bar\n",
    "    DEW_a4      = -2.726       # 10^-4 cal-K/mol\n",
    "    DEW_c1      = 18.18        #       cal/mol/K\n",
    "    DEW_c2      = -2.981       # 10^-4 cal-K/mol\n",
    "    DEW_comment = \"Shock & Helgeson (1988)\"\n",
    "else:\n",
    "    phase_name  = \"NaCl_hkf\"\n",
    "    species     = 'NaCl'\n",
    "    formula     = \"Na(1)Cl(1)\"\n",
    "    DEW_g       = -92910.0     #       cal/mol\n",
    "    DEW_h       = -96120.0     #       cal/mol\n",
    "    DEW_s       = 28.00        #       cal/mol/K\n",
    "    DEW_v       = 24.00006579  #       cm^3/mol\n",
    "    DEW_cp      = 8.508360325  #       cal/mol/K\n",
    "    DEW_omega   = -0.038       # 10^-5 cal/mol\n",
    "    DEW_z       = 0.0          #       charge\n",
    "    DEW_a1      = 5.0363       # 10    cal/mol/bar\n",
    "    DEW_a2      = 4.7365       # 10^-2 cal/mol\n",
    "    DEW_a3      = 3.4154       #       cal-K/mol/bar\n",
    "    DEW_a4      = -2.9748      # 10^-4 cal-K/mol\n",
    "    DEW_c1      = 10.8         #       cal/mol/K\n",
    "    DEW_c2      = -1.3         # 10^-4 cal-K/mol\n",
    "    DEW_comment = \"Shock & Helgeson (1988)\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Scaling constants .."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SCALEforOmega = 1.0e+5\n",
    "SCALEforA1    = 1.0e-1\n",
    "SCALEforA2    = 1.0e+2\n",
    "SCALEforA4    = 1.0e+4\n",
    "SCALEforC2    = 1.0e+4\n",
    "CALtoJOULES   = 4.184"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "param_dict = {'name':phase_name,\n",
    "              'formula':formula,\n",
    "              'G_ref':DEW_g*CALtoJOULES, \n",
    "              'S_ref':DEW_s*CALtoJOULES, \n",
    "              'a1':DEW_a1*CALtoJOULES*SCALEforA1, \n",
    "              'a2':DEW_a2*CALtoJOULES*SCALEforA2, \n",
    "              'a3':DEW_a3*CALtoJOULES,  \n",
    "              'a4':DEW_a4*CALtoJOULES*SCALEforA4, \n",
    "              'c1':DEW_c1*CALtoJOULES, \n",
    "              'c2':DEW_c2*CALtoJOULES*SCALEforC2, \n",
    "              'omega0':DEW_omega*CALtoJOULES*SCALEforOmega, \n",
    "              'theta':228.0,\n",
    "              'Psi':2600.0,\n",
    "              'eta':1.66027e5*CALtoJOULES,\n",
    "              'rH':3.082,\n",
    "              'z':DEW_z,\n",
    "              'T_r':298.15,\n",
    "              'P_r':1.0\n",
    "             }\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict = model.get_values()\n",
    "values_dict.update(param_dict)\n",
    "values_dict['reference']=reference\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Write out spud files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.set_values(values_dict)\n",
    "model.to_xml(path=SPUD_DIR)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.optimize import minimize, LinearConstraint, Bounds\n",
    "from scipy.integrate import solve_ivp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calculate \"Adiabats\" in the Fo-SiO$_2$ system\n",
    "\n",
    "This notebook derives and calculates self-gravitating isentropic profiles for a 1-D solid-state upwelling problem with no phase separation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  Model Derivation\n",
    "\n",
    "Conservation of Mass, Composition, and Entropy along with lithostatic pressure in  a 1-D steady state upwelling column for $N$ phases all traveling at the same upwelling velocity $W$ can be written\n",
    "\n",
    "$$\n",
    "    \\frac{d\\ }{dz} \\rho_i \\phi_i W = \\Gamma_i \n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d\\ }{dz} \\rho_i \\phi_i W c_i^k = \\Gamma_i^k \n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d\\ }{dz} \\bar{\\rho} W = 0\n",
    "$$\n",
    "\n",
    "$$\n",
    "   \\frac{d\\ }{dz} \\sum_i \\rho_i \\phi_i s_i W  = 0\n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d P}{dz} = -\\bar{\\rho} g\n",
    "$$\n",
    "\n",
    "Where $\\rho_i$ is the intrinsic density of phase $i$ ($i = 0,\\ldots,N-1$), $\\phi_i$ is the volume fraction and .  $c_i^k$ is the mass fraction of endmember $k$ in phase $i$ and $\\Gamma_i^k(T,P,\\Phi,C)$ is the net mass-transfer rate of component $k$ in phase $i$ over all reactions.  \n",
    "\n",
    "Because \n",
    "\n",
    "$$\n",
    "\\sum_k c_i^k = 1\n",
    "$$ \n",
    "\n",
    "it follows that \n",
    "\n",
    "$$\n",
    "\\Gamma_i(T,P,\\Phi,C) = \\sum_k \\Gamma_i^k(T,P,\\Phi,C)\n",
    "$$\n",
    "\n",
    "is the net mass transfer of phase $i$ over all reactions. \n",
    "\n",
    "Moreover by construction and definition\n",
    "\n",
    "$$\n",
    "    \\sum_i \\Gamma_i = 0\\quad \\mathrm{and} \\quad \\bar{\\rho} = \\sum_i \\rho_i\\phi_i\n",
    "$$\n",
    "\n",
    "is the mean density of the $N$ phase assemblage.  Finally $s_i$ is the specific entropy of each phase and the fourth equation is simply a statment of total entropy conservation.\n",
    "\n",
    "In total, given that $\\rho_i$, $\\Gamma_i^k$, $s_i$ are thermodynamic functions of $(T,P,C)$  given by the ``ThermoCodegen`` reaction objects, the actual unknowns in this system are $\\phi_i$, $W$, $c_i^k$, $T$,  and $P$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A useful change of variables\n",
    "\n",
    "If we define the mass fraction of phase $i$ as\n",
    "\n",
    "$$\n",
    "    F_i = \\frac{\\rho_i\\phi_i}{\\bar{\\rho}}\n",
    "$$ \n",
    "\n",
    "we can remove the dependence on $W$ and rewrite the system of equations in terms of the variables $F_i$, $c_i^k$, $s_i$, and $P$.  \n",
    "\n",
    "Integrating the third equation and applying the boundary condition at $z=0$ gives\n",
    "\n",
    "$$\n",
    "    \\bar{\\rho}W = \\bar{\\rho}_0 W_0\n",
    "$$\n",
    "\n",
    "where the RHS is the mean density and upwelling velocity evaluated at $z=0$.  With these definitions it follows that\n",
    "\n",
    "$$\n",
    "    \\rho_i\\phi_i W = \\bar{\\rho}_0W_0 F_i\n",
    "$$ \n",
    "\n",
    "and we can rewrite the system of equations in terms of $\\mathbf{F}$ as \n",
    "\n",
    "$$\n",
    "    \\frac{d\\ }{dz} F_i = \\frac{1}{\\bar{\\rho}_0W_0}\\Gamma_i\n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d\\ }{dz} F_i c_i^k = \\frac{1}{\\bar{\\rho}_0W_0}\\Gamma_i^k \n",
    "$$\n",
    "\n",
    "$$\n",
    "   \\frac{d\\ }{dz} \\sum_i F_i s_i  = 0\n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d P}{dz} = -\\left(\\sum_i \\frac{F_i}{\\rho_i}\\right)^{-1} g\n",
    "$$\n",
    "\n",
    "\n",
    "Where it is relatively straightforward to show that an alternative definition of the mean density is\n",
    "\n",
    "$$\n",
    "    \\bar{\\rho} = \\left(\\sum_i \\frac{F_i}{\\rho_i}\\right)^{-1}\n",
    "$$\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Temperature Equations\n",
    "\n",
    "As written,  Temperature is an implicit function of the entropy conservation equation which could be integrated directly to show\n",
    "\n",
    "$$\n",
    "    \\sum_i F_i s_i = S_0\n",
    "$$ \n",
    "\n",
    "such that total entropy is conserved.  Written in this form, the system becomes a DAE (Differential Algebraic Equation) which can be solved using more sophisticated solvers,  however, using standard relationships for derivatives of entropy and applying the product rule to both the compositional equations and the entropy equation, we can rewrite the full system as a pure ODE of the form\n",
    "\n",
    "$$\n",
    "    \\frac{d\\mathbf{u}}{dt} = \\mathbf{f}(t,\\mathbf{u})\n",
    "$$\n",
    "\n",
    "where \n",
    "$$\n",
    "    \\mathbf{u} = \\begin{bmatrix} \\mathbf{F} \\\\ \\mathbf{C} \\\\ T \\\\ P \\\\\\end{bmatrix}\n",
    "$$\n",
    "\n",
    "Given the Thermodynamic relationships\n",
    "\n",
    "$$\n",
    "    \\frac{\\partial s_i}{\\partial T} = \\frac{c_{pi}}{T}, \\quad \\frac{\\partial s_i}{\\partial P} = -\\frac{\\alpha_i}{\\rho_i}\n",
    "$$ \n",
    "\n",
    "and using the product rule, the system of equations in temperature form becomes\n",
    "\n",
    "\n",
    "$$\n",
    "    \\frac{d\\ }{dz} F_i = \\frac{1}{\\bar{\\rho}_0W_0}\\Gamma_i\n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d\\ }{dz} c_i^k = \\frac{1}{\\bar{\\rho}_0W_0(F_i + \\epsilon)}\\left[\\Gamma_i^k - c_i^k\\Gamma_i\\right] \n",
    "$$\n",
    "\n",
    "$$\n",
    "   \\frac{d T }{dz}  =  \\frac{T}{\\bar{c_p}}\\left[ -\\bar{\\alpha}g + \\frac{1}{\\bar{\\rho}_0W_0} \\sum_i \\left(-s_i\\Gamma_i + \\sum_k \\frac{\\partial s_i}{\\partial c_i^k}\\left(\\Gamma_i^k - c_i^k\\Gamma_i\\right)\\right)\\right]\n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d P}{dz} = -\\left(\\sum_i \\frac{F_i}{\\rho_i}\\right)^{-1} g\n",
    "$$\n",
    "    \n",
    "    \n",
    "where \n",
    "$$\n",
    "    \\bar{c_p} = \\sum_i F_ic_{pi},\\quad \\bar{\\alpha} = \\frac{\\sum_i (F_i/\\rho_i)\\alpha_i}{\\sum_i F_i/\\rho_i}\n",
    "$$\n",
    "\n",
    "are respectively, the mean heat capacity and mean thermal expansivity.  $\\epsilon$ is a small regularization parameter to handle the singularity in compositions that arise when a phase arises or disappears (i.e. $F_i=0$)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scaling\n",
    "\n",
    "For numerical reasons, it is useful to non-dimensionalize the equations. A useful scaling for  a 1-D steady state upwelling column is\n",
    "\n",
    "$$\n",
    "\\begin{matrix}\n",
    "     T = T_0T' & P = P_0 P' & z = \\frac{P_0}{\\bar{\\rho}_0 g}z' & \\Gamma = r_0\\sigma_0\\Gamma'  \\\\\n",
    "\\end{matrix}\n",
    "$$\n",
    "\n",
    "where $T_0, P_0$ is the pressure at the base of the column, $\\bar{\\rho}_0$ is the mean density at $T_0$, $P_0$ thus\n",
    "\n",
    "$$\n",
    "    h = \\frac{P_0}{\\bar{\\rho}_0 g}\n",
    "$$\n",
    "\n",
    "is an *estimate* of the depth of the melting column (and would be the depth if densities were constant).  In general the actual depth needs to be calculated from the integration of density.  $r_0$ is the reaction rate in mass per unit area per time and  $\\sigma_0$ is the  specific surface area available for reaction.  The product $r_0\\sigma_0$ has units of density per time.  \n",
    "\n",
    "Substituting into the dimensional equations and dropping primes yields the final system of ODE's we will solve\n",
    "\n",
    "$$\n",
    "    \\frac{d F_i }{dz}  = Da\\Gamma_i\n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d c_i^k}{dz}  = \\frac{Da}{(F_i + \\epsilon)}\\left[\\Gamma_i^k - c_i^k\\Gamma_i\\right] \n",
    "$$\n",
    "\n",
    "$$\n",
    "   \\frac{d T }{dz}  =  \\frac{T}{\\bar{c_p}}\\left[ -\\frac{P_0}{\\bar{\\rho}_0}\\bar{\\alpha} + Da\\sum_i \\left(-s_i\\Gamma_i + \\sum_k \\frac{\\partial s_i}{\\partial c_i^k}\\left(\\Gamma_i^k - c_i^k\\Gamma_i\\right)\\right)\\right]\n",
    "$$\n",
    "\n",
    "$$\n",
    "    \\frac{d P}{dz} = -\\frac{\\bar{\\rho}}{\\bar{\\rho}_0}\n",
    "$$\n",
    "\n",
    "where \n",
    "$$\n",
    "    Da = \\frac{P_0r_0\\sigma_0}{\\bar{\\rho}_0^2g W_0}\n",
    "$$ \n",
    "\n",
    "is the Dahmköler number which is a measure of the degree of disequilibrium during transport. $Da=0$ is complete disequilibrium and $Da\\rightarrow\\infty$ implies equilibrium.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1-D isentropes in the simplified mafic Fo-SiO2 system\n",
    "\n",
    "Here we consider a reduced version in the mafic end-members of the Fo-SiO2 system that is appropriate to upper mantle conditions.  \n",
    "\n",
    "To view the full system we first import the `ThermoCodegen` reaction object (which needs to be in the python path) appropriate for this system\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import py_fo_sio2_poly_linear_rxns as pfs\n",
    "rxn = pfs.fo_sio2_poly_linear_rxns()\n",
    "rxn.report()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Which currently includes 4 phases and 3 reactions although for problems with no initial SiO2 phase we expect we can ignore the Silica phase.\n",
    "\n",
    "For convenience we will identify some indices for phases and components"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iLq = 0\n",
    "iOl = 1\n",
    "iOpx = 2\n",
    "iSi = 3\n",
    "kSi = 0\n",
    "kFo = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Some utility functions for converting between compositions and Temperature units"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load liquid composition vector given just x_si204\n",
    "set_xliq = lambda xq : np.array([xq,1.-xq])\n",
    "\n",
    "# convert mole fractions between moles of Si2O4 and SiO2\n",
    "si2_to_si = lambda xq : 2.*xq/(1.+xq)\n",
    "si_to_si2 = lambda xq : xq/(2.-xq)\n",
    "\n",
    "# convert temperatures\n",
    "to_Kelvin = lambda T : T + 273.15\n",
    "to_Celsius = lambda T : T - 273.15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create right hand side function of ODEs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(t,u,rxn,scale,epsilon=1.e-3,verbose=False):\n",
    "    '''\n",
    "    return rhs of the dimensionless 1-D isentropic upwelling equations\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    \n",
    "    t: float\n",
    "        time\n",
    "    u: array\n",
    "        solution array [ F, c_(Lq)^{Si2O4}, T, P ]\n",
    "    rxn:  ThermoCodegen Reaction object\n",
    "    scale: dict\n",
    "        dictionary containing scales for T,P,rho\n",
    "    epsilon: float\n",
    "        regularization parameter for composition equation\n",
    "        \n",
    "    \n",
    "    '''\n",
    "    # Extract variables\n",
    "    N = 4\n",
    "    F = u[:N]\n",
    "    csi = u[N]\n",
    "    T = u[N+1]\n",
    "    P = u[N+2]\n",
    "    \n",
    "    # scale Temperature and pressure\n",
    "    \n",
    "    Ts = scale['T']*T\n",
    "    Ps = scale['P']*P\n",
    "    \n",
    "    # set the melt composition\n",
    "    C[iLq] = np.array([csi,1.-csi])\n",
    "    \n",
    "    # calculate thermodynamic properties from the rxn object\n",
    "    gamma_i = np.array(rxn.Gamma_i(Ts,Ps,C,F))\n",
    "    gamma_ik_lq = np.array(rxn.Gamma_ik(Ts,Ps,C,F)[iLq])\n",
    "    if verbose:\n",
    "        print(gamma_i)\n",
    "        print(gamma_ik_lq)\n",
    "    \n",
    "    # liquid properties\n",
    "    dCik_lq = gamma_ik_lq - np.array(C[iLq])*gamma_i[iLq]\n",
    "    dsdc_lq = np.array(rxn.ds_dC(Ts,Ps,C)[iLq])\n",
    "    if verbose:\n",
    "        print(dCik_lq)\n",
    "        print(dsdc_lq)\n",
    "    \n",
    "    # phase properties\n",
    "    rho = np.array(rxn.rho(Ts, Ps, C))\n",
    "    alpha = np.array(rxn.alpha(Ts,Ps,C))\n",
    "    cp = np.array(rxn.Cp(Ts, Ps , C))\n",
    "    s = np.array(rxn.s(Ts, Ps, C))\n",
    "    v = F/rho\n",
    "    \n",
    "    # mean properties\n",
    "    rho_bar = 1./sum(v)\n",
    "    alpha_bar = v.dot(alpha)*rho_bar\n",
    "    cp_bar = F.dot(cp)\n",
    "    s_bar = F.dot(s)\n",
    "    \n",
    "    A = scale['P']/scale['rho']\n",
    "    \n",
    "    dFdz = gamma_i\n",
    "    dcsidz = (gamma_ik_lq[kSi] - csi*gamma_i[iLq])/(F[iLq] + epsilon)\n",
    "    dTdz = T/cp_bar * ( -A*alpha_bar  - s.dot(gamma_i) + dsdc_lq.dot(dCik_lq) )\n",
    "    dPdz = -rho_bar/scale['rho']\n",
    "    \n",
    "    if verbose:\n",
    "        print(T/cp_bar, A*alpha_bar, s.dot(gamma_i), dsdc_lq.dot(dCik_lq))\n",
    "    du = np.empty(u.shape)\n",
    "    du[:N] = dFdz\n",
    "    du[N:] = np.array([dcsidz, dTdz, dPdz])\n",
    "    return du\n",
    "    \n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set Initial conditions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Temperature and pressure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# initial temperature and pressure in K and bars\n",
    "T0 = to_Kelvin(1680.)\n",
    "P0 = 11000."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Approximate Melt composition\n",
    "\n",
    "We will start by assuming the initial liquid is pure liquid Opx which in liquid endmember space is 1/4 mols of Qz4 and 1/2 mol of Fo or `n[iLq] = [ 0.25, 0.5 ]` or `X[iLq] = [1/3, 2/3]`.  This will be useful for calculating initial mass fractions given densities, and then we will refine our initial disequilibrium melt concentration below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# initialize Mol fraction matrix \n",
    "X0 = rxn.zero_C()\n",
    "for i in range(1,len(X0)):\n",
    "    X0[i] = [1.]\n",
    "X0[iLq] = [1/3.,2/3.]\n",
    "print('X = ',X0)\n",
    "\n",
    "C0 = rxn.X_to_C(X0)\n",
    "print('C = ',C0)\n",
    "\n",
    "Lq = pfs.Liquid()\n",
    "print('Liquid Formula = {} (4 oxygen basis)'.format(Lq.formula(T0,P0,X0[iLq])))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Initial Mass fractions\n",
    "\n",
    "We start with initial volume fractions, then convert them to mass fractions given the densities of the phases"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Phi0  = np.array([0., .6, .4, 0.])\n",
    "rho = np.array(rxn.rho(T0,P0,C0))\n",
    "F0 = rho*Phi0/rho.dot(Phi0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set the Da number\n",
    "\n",
    "Here we will actually build the Dahmkohler number into the reaction object by setting internal parameters.  For every reaction object there are sets of parameters that can be set internally at run time.  To view them use"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.list_parameters()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where `Stot` is the available surface area and `r0_melt` and `r0_xtal` are the forward and backwards reaction rates.  Here they are both set to 1 so the reaction rates $\\Gamma$ are already dimensionless.  To scale all the reactions by the Da number we can simply set `Stot=Da`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Da = 100.\n",
    "rxn.set_parameter('Stot',Da)\n",
    "rxn.list_parameters()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solve for initial melt concentration\n",
    "\n",
    "The first incipient disequilbrium liquid at any P,T should satisfy\n",
    "\n",
    "$$\n",
    "    \\Gamma_{Lq}^k(T,P,C,\\Phi) - c_{Lq}^k\\Gamma_{Lq}(T,P,C,\\Phi) = 0\n",
    "$$ \n",
    "\n",
    "which is a non-linear problem in $C$ (assuming $F_{Lq}=0$). However,  it should be cast as a non-linear constrained optimization problem with the additional constraints that $\\mathbf{1}^T\\mathbf{c}_{Lq}=1.$ and all concentrations are bounded in $[0,1]$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Calculate the initial  liquid composition\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def func(cLq,T,P,C,F):\n",
    "    \"\"\" function to return || MdC/dT|| \"\"\"\n",
    "    C[iLq] = cLq\n",
    "    res = np.array(rxn.Gamma_ik(T,P,C,F)[iLq]) - np.array(C[iLq])*rxn.Gamma_i(T,P,C,F)[iLq]\n",
    "    return np.linalg.norm(res)\n",
    "\n",
    "#constraints and bounds\n",
    "lc = LinearConstraint(np.ones(2),1.,1.)\n",
    "bnds = Bounds(np.zeros(2),np.ones(2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the optimizer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c0 = C0[iLq].copy()\n",
    "sol = minimize(func, c0, args=(T0, P0, C0, F0),bounds=bnds,constraints=lc)\n",
    "print(sol.message)\n",
    "print('residual = {}, sum(cf)= {}'.format(sol.fun,np.sum(sol.x)))\n",
    "print('Liquid Composition = {}'.format(sol.x))\n",
    "C0[iLq] = sol.x\n",
    "C = C0.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialize Density and scaling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rho = np.array(rxn.rho(T0, P0, C0))\n",
    "v = F0/rho\n",
    "rho_bar = 1/sum(v)\n",
    "\n",
    "# depth/distance scale in km\n",
    "g = 9.81 # m/s^2\n",
    "h = P0*1e5/(rho_bar*100.*g)/1000\n",
    "\n",
    "scale= {'T':T0, 'P':P0, 'rho':rho_bar, 'h':h}\n",
    "print(scale)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set epsilon and initial condition u0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "epsilon = 1.e-3\n",
    "N = len(F0)\n",
    "u0=np.empty(N+3)\n",
    "u0[:N] = F0\n",
    "u0[N:] = np.array([ C0[iLq][kSi], 1., 1.])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solve the system of ODE's\n",
    "\n",
    "Here we will solve the system of ODE's from P0 to the surface, by setting an event which terminates the solution when the pressure reaches Pmin = 1 bar"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Pmin = 1\n",
    "event = lambda t, y, rxn, scale: P0*y[-1]-Pmin\n",
    "event.terminal = True\n",
    "\n",
    "sol = solve_ivp(f,[0,1.1],u0,args=(rxn,scale),dense_output=True,method='Radau',rtol=1.e-6,atol=1.e-10, events=event)\n",
    "print('{} P_end = {} bar.  Used {} steps: '.format(sol.message, sol.y[-1][-1]*scale['P'], len(sol.t)))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### and plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z = np.linspace(0,1,100)\n",
    "depth = -scale['h']*(1 - z)\n",
    "fig = plt.figure(figsize=(12,8))\n",
    "ax = fig.add_subplot(1,3,1)\n",
    "ax.plot(sol.sol(z)[:N].T,depth)\n",
    "ax.set_xlabel('F')\n",
    "ax.set_ylabel('depth (km)')\n",
    "ax.legend(['Lq', 'Ol', 'Opx', 'Qz'],loc='best')\n",
    "ax.grid()\n",
    "\n",
    "ax = fig.add_subplot(1,3,2)\n",
    "ax.plot(sol.y[N].T,-scale['h']*(1 - sol.t),'o-')\n",
    "ax.set_xlabel('cSi_lq')\n",
    "ax.set_title('Da = {}'.format(Da))\n",
    "ax.grid()\n",
    "\n",
    "ax = fig.add_subplot(1,3,3)\n",
    "ax.plot(to_Celsius(sol.sol(z)[-2].T*scale['T']),depth)\n",
    "ax.set_xlabel('T (C)')\n",
    "ax.grid()\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot thermodynamic properties"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "F = sol.sol(z)[:N].T\n",
    "cfsi = sol.sol(z)[N]\n",
    "T = scale['T']*sol.sol(z)[-2]\n",
    "P = scale['P']*sol.sol(z)[-1]\n",
    "\n",
    "gamma_i = np.zeros(F.shape)\n",
    "s = gamma_i.copy()\n",
    "rho = gamma_i.copy()\n",
    "stot = np.zeros(T.shape)\n",
    "\n",
    "for i,t in enumerate(T):\n",
    "    C[iLq]  = [ cfsi[i], 1. - cfsi[i]]\n",
    "    gamma_i[i] = rxn.Gamma_i(T[i],P[i],C,F[i])\n",
    "    s[i]       = rxn.s(T[i],P[i],C)\n",
    "    rho[i]     = rxn.rho(T[i], P[i], C)\n",
    "    stot[i] = F[i].dot(s[i])\n",
    "\n",
    "s_err = np.linalg.norm((stot - stot[0]))/stot[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12,8))\n",
    "ax = fig.add_subplot(1,3,1)\n",
    "ax.plot(gamma_i,depth)\n",
    "ax.set_xlabel('$\\Gamma_i$')\n",
    "ax.set_ylabel('depth (km)')\n",
    "ax.legend(['Lq', 'Ol', 'Opx', 'Qz'],loc='best')\n",
    "ax.grid()\n",
    "\n",
    "ax = fig.add_subplot(1,3,2)\n",
    "ax.plot(s,depth)\n",
    "ax.plot(stot,depth,'k--')\n",
    "ax.set_xlabel('$s$')\n",
    "ax.set_ylabel('depth (km)')\n",
    "ax.legend(['Lq', 'Ol', 'Opx', 'Qz', '$S_{tot}$'], loc='best')\n",
    "ax.set_title('$\\Delta S/S_0$= {}'.format(s_err))\n",
    "ax.grid()\n",
    "\n",
    "ax = fig.add_subplot(1,3,3)\n",
    "ax.plot(rho[:,:3]*100,depth)\n",
    "ax.set_xlabel('$\\\\rho$ kg/m$^3$')\n",
    "ax.legend(['Lq', 'Ol', 'Opx', 'Qz'],loc='best')\n",
    "ax.grid()\n",
    "plt.show()\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simple Solution SymPy Code Generation\n",
    "\n",
    "This notebook is based on Mark Ghiorso's Simple-Solution notebook in ThermoEngine for generating an asymmetric regular solution model as Modified by Lucy Tweed for an Asymmetric Binary solution in the Fo-SiO2 system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os,sys\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import sympy as sym\n",
    "import hashlib\n",
    "import warnings\n",
    "import time\n",
    "sym.init_printing()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Required ENKI packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from thermocodegen.coder import coder"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### let's set up some directory names for clarity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HOME_DIR = os.path.abspath('../..')\n",
    "SPUD_DIR = HOME_DIR+'/phases'\n",
    "try:\n",
    "    os.makedirs(SPUD_DIR)\n",
    "except:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set a reference string for this Notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reference = 'fo_sio2/notebooks/Generate_asymmetric_binary_fosi_solution.ipynb'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simple Solution Properties - Structure of the Equations\n",
    "There are three terms:\n",
    "- Terms describing standard state contributions\n",
    "- Terms describing the configurational entropy of solution\n",
    "- Terms describing the excess enthalpy of solution  \n",
    "\n",
    "Assumptions:\n",
    "- There are $c$ components in the system\n",
    "- There are as many endmember species, $s$, as there are components\n",
    "- The configurational entropy is described as a simple $x_i log(x_i)$ sum\n",
    "- The excess enthalpy is described using an asymmetric regular solution described below\n",
    "- Ternary interaction terms are not included in a binary system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Number of solution components\n",
    "This notebook illustrates a two component solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a simple solution model\n",
    "... with the specified number of endmember thermodynamic components"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = coder.SimpleSolnModel.from_type(nc=c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Retrieve primary compositional variables\n",
    "- $n$ is a vector of mole numbers of each component  \n",
    "- $n_T$ is the total number of moles in the solution\n",
    "### and construct a derived mole fraction variable\n",
    "- $X$ is a vector of mole fractions of components in the system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = model.n\n",
    "nT = model.nT\n",
    "X = n/nT\n",
    "n, nT, X"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Retrieve the temperature, pressure, and standard state chemical potentials\n",
    "- $T$ is temperature in $K$\n",
    "- $P$ is pressure in $bars$\n",
    "- $\\mu$ in Joules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = model.get_symbol_for_t()\n",
    "P = model.get_symbol_for_p()\n",
    "Pr = model.get_symbol_for_pr()\n",
    "mu = model.mu\n",
    "T,P,mu,Pr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check model dictionary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the standard state contribution to solution properties"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G_ss = (n.transpose()*mu)[0]\n",
    "G_ss"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define configurational entropy and configurational Gibbs free energy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S_config,R = sym.symbols('S_config R')\n",
    "S_config = 0\n",
    "for i in range(0,c):\n",
    "    S_config += X[i]*sym.log(X[i])\n",
    "S_config *= -R*nT\n",
    "S_config"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G_config = sym.simplify(-T*S_config)\n",
    "G_config"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameterize the excess free energy\n",
    "This is based on Lucy Tweed's pyezthermo model for G_excess of mixing\n",
    "\n",
    "where $W$, $dWdT$ and $dWdP$  are two-vectors (i.e.)\n",
    "$$\n",
    "    W = \\begin{bmatrix} W_1\\\\ W_2\\end{bmatrix}\\quad dWdT = \\begin{bmatrix} dWdT_1\\\\ dWdT_2\\end{bmatrix}\\quad dWdP = \\begin{bmatrix} dWdP_1\\\\ dWdP_2\\end{bmatrix}\n",
    "$$\n",
    "with total Margules parameters\n",
    "\n",
    "$$\n",
    "    W_{tot} = W + T*dWdT + (P-Pr)*dWdT\n",
    "$$\n",
    "and\n",
    "$$\n",
    "    G_{excess} = (n\\cdot W_{tot})X_1X_2\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params = ['R']\n",
    "units = ['J/mol/K']\n",
    "symparam = (R,)\n",
    "W0 = sym.Matrix\n",
    "G_excess = sym.symbols('G_excess')\n",
    "G_excess = 0\n",
    "for i in range(1,c+1):\n",
    "        param = 'W{}'.format(i); params.append(param); units.append('J/m')\n",
    "        h_term = sym.symbols(param); symparam += (h_term,)\n",
    "        param = 'dWdT{}'.format(i); params.append(param); units.append('J/K-m')\n",
    "        dT_term = sym.symbols(param); symparam += (dT_term,)\n",
    "        param = 'dWdP{}'.format(i); params.append(param); units.append('J/bar-m')\n",
    "        dP_term = sym.symbols(param); symparam += (dP_term,)\n",
    "        w_term = h_term + T*dT_term + (P-Pr)*dP_term\n",
    "        G_excess += w_term*n[i-1]\n",
    "G_excess *= X[0]*X[1]\n",
    "G_excess"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(params)\n",
    "print(units)\n",
    "symparam"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the Gibbs free energy of solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = G_ss + G_config + G_excess\n",
    "G"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Add the Gibbs free energy of solution to the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.add_potential_to_model('G',G,list(zip(params, units, symparam)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### let's inspect the dictionary and unset parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.model_dict"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict = model.get_values()\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... assign a formula string for code generation  \n",
    "... assign a conversion string to map element concentrations to moles of end members"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict.update(dict(name='Liquid',\n",
    "                        abbrev='Lq',\n",
    "                        reference=reference,\n",
    "                        formula_string='Mg[Mg]Si[Si]O[O]',\n",
    "                        conversion_string=['[0]=([Si] - [Mg]/2.)/2.', '[1]=0.5*[Mg]' ],\n",
    "                        test_string = ['[0] > 0.0', '[1] > 0.0' ],\n",
    "                        endmembers=[ 'Quartz4_liquid','Forsterite_xmelts'],\n",
    "                        R=8.31446261815324,\n",
    "                        W1=35168., W2= -56504.,\n",
    "                        dWdT1=0., dWdT2= 0.,\n",
    "                        # original\n",
    "                        dWdP1=0.7959, dWdP2= -1.8783\n",
    "                        # Tweed 2021, thesis\n",
    "                        #dWdP1=0.7059, dWdP2= -1.8783\n",
    "                       ))\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.set_values(values_dict)\n",
    "model.to_xml(path=SPUD_DIR)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fo-SiO$_2$ system\n",
    "\n",
    "This notebook generates a reaction `.rxml` file for the full Fo-SiO$_2$ system  This system is described in detail in Lucy Tweed's thesis (2021)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy as sym\n",
    "import os\n",
    "from glob import glob\n",
    "import warnings\n",
    "sym.init_printing()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from thermocodegen.codegen import reaction\n",
    "from thermocodegen.coder import coder"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "choose a version tag for generating different versions of Berman endmember numbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### let's set up some directory names for clarity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HOME_DIR = os.path.abspath('../../')\n",
    "SPUD_DIR = HOME_DIR+'/reactions'\n",
    "try:\n",
    "    os.makedirs(SPUD_DIR)\n",
    "except:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set a reference string for this Notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reference = 'fo_sio2/notebooks/Generate_fo_sio2_poly_linear_rxn.ipynb'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List of phases present in this set of reactions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "phase_names = ['Liquid', 'Olivine', 'Orthopyroxene', 'Silica_polymorph' ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Path to the thermodynamic database tarball file that this set of reactions are built with\n",
    "\n",
    "Here we use an absolute path to a local file which is safer than a relative path, but can make the rxml files harder to share"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#db = HOME_DIR+'/database/fo_sio2_db.tar.gz' "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, we could use a remote thermodynamic database, for example the one available as a DOI from zenodo \n",
    "\n",
    "[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7976277.svg)](https://doi.org/10.5281/zenodo.7976277)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "db = 'https://zenodo.org/record/7976277/files/fo_sio2_db.tar.gz'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instantiate a Reaction object. This is initialized using the 'from_database' class method. Initializing in this way requires the total number of reaction (total_reactions), a list of phase names (phase_names) and a path to a thermocodegen generated thermodynamic database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn = reaction.Reaction.from_database(total_reactions=3, phase_names=phase_names, database=db)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The 'model_dict' attribute of this object contains the current state of the information that we have given and what has been extracted from the thermodynamic database 'fo_sio2_db.tar.gz'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set some utility indices for referencing phases and endmembers (this should be automated somehow)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iLq = 0\n",
    "iOl = 1\n",
    "iOpx = 2\n",
    "iSi = 3\n",
    "kSi = 0\n",
    "kFo = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define the reactions in this system, and then write SymPy that describes the reactions rates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we pull out the SymPy symbols for the independent variables (temperature T, pressure P, concentration C and phase fraction Phi). Concentration C is a sym.MatrixSymbol of dimension N (number of phases) by Kmax (maximum number of endmembers). Phase fraction Phi is a sym.MatrixSymbol of dimension N (number of phases) by 1 (i.e., a vector)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Variables\n",
    "T = rxn.T\n",
    "P = rxn.P\n",
    "C = rxn.C\n",
    "Phi = rxn.Phi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we pull out the special symbol reserved for the affinity, A. This is a sym.MatrixSymbol of dimension J (number of reactions) by 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Affinity\n",
    "A = rxn.A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we define the parameters used in the reaction rate expressions. These should be sym.Symbol objects. We also need to create lists that contain the parameter names as strings, their units (in string form) and the corresponding SymPy symbols. Note that these need to be ordered correctly.\n",
    "\n",
    "**Global Parameters**\n",
    "* T0: reference temperature in K\n",
    "* R: Gas Constant\n",
    "* r0_xtal: reaction rate of crystallization (kg m$^{-2}$ s$^{-1}$)\n",
    "* r0_melt: reaction rate of melting (kg m$^{-2}$ s$^{-1}$)\n",
    "* Stot: total available surface area\n",
    "* eps:  Phase Fraction regularization parameter such that $S = Stot\\frac{\\phi}{\\phi + \\epsilon}$\n",
    "* M0: reference reaction molar mass (g/m)\n",
    "* M_fo: reference molar mass fo reaction (g/m)\n",
    "* M_en: reference molar mass Opx reaction (g/m)\n",
    "* M_en: reference molar mass Si reaction (g/m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Global Parameters\n",
    "param_strings = ['T0', 'R', 'r0_xtal', 'r0_melt', 'Stot', 'eps']\n",
    "units = ['K','J/(mol K)', 'kg/m^2/s', 'kg/m^2/s', 'm^-1','' ]\n",
    "\n",
    "# mass parameters for reactions\n",
    "param_strings += ['M0', 'M_fo', 'M_en', 'M_qz']\n",
    "units += ['g/mol' for i in range(4)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given Parameter strings and units,  we can use some coder convenience functions to create coder parameters and a dictionary of symbols to make available to the local dictionary"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "params = coder.set_coder_params(param_strings,units)\n",
    "symbols_dict = coder.get_symbol_dict_from_params(params)\n",
    "locals().update(symbols_dict)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Olivine melting reaction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set up the forsterite melting reaction (Forsterite_berman -> Forsterite_xmelts). This requires a list of tuples for both the reactants and products. Each tuple consists of strings of \n",
    "\n",
    "(name,phase,endmember)\n",
    "\n",
    "where \n",
    "    **name**:  is an arbitrary name describing the reactant endmenber,\n",
    "    **phase**: is the phase containing the endmember\n",
    "    **endmember**: the name of the endmember that is reacting in that phase\n",
    "    \n",
    "The phase and endmember names should be consistent with whatever they are called in the thermodynamic database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reaction 0\n",
    "j = 0\n",
    "\n",
    "# Reactants\n",
    "Fo_Ol = ('Fo_Ol','Olivine','Forsterite_berman')\n",
    "\n",
    "# Products\n",
    "Fo_Lq = ('Fo_Lq','Liquid','Forsterite_xmelts')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we write down the SymPy expression for the reaction. This, along with the list of reactants, products, parameters, units and variable symbols are passed into the 'add_reaction_to_model' function. This function also requires a 'name' field, which should be consistent with the variable assigned to the SymPy expression; for example, here our expression is Fo_melting = ..., so our name is 'Fo_melting'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SymPy expression for reaction rate\n",
    "# Rate constant\n",
    "rp = r0_melt*sym.exp(-T0/T)\n",
    "rm = r0_xtal*sym.exp(-T0/T)\n",
    "\n",
    "# Availability\n",
    "Sp = Stot*Phi[iOl]/(Phi[iOl] + eps)\n",
    "Sm = Stot*Phi[iLq]/(Phi[iLq] + eps)\n",
    "\n",
    "# Affinity function\n",
    "fA = (M0/M_fo)*A[j]/R/T\n",
    "\n",
    "Ol_melting = sym.Piecewise((rp*Sp*fA,A[0]>=0),(rm*Sm*fA,A[0]<0),(0,True))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reactants = [ Fo_Ol ]\n",
    "products = [ Fo_Lq ]\n",
    "rxn.add_reaction_to_model('Ol_melting', reactants, products, Ol_melting, params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Enstatite melting reaction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reaction 1\n",
    "j = 1\n",
    "\n",
    "# Reactants\n",
    "En_Opx = ('En_Opx','Orthopyroxene','Orthoenstatite_berman')\n",
    "\n",
    "# Products\n",
    "#Fo_Lq = ('Fo_Lq','Liquid','Forsterite_xmelts')\n",
    "Si_Lq = ('Si_lq', 'Liquid', 'Quartz4_liquid')\n",
    "\n",
    "# Rate constant\n",
    "rp = r0_melt*sym.exp(-T0/T)\n",
    "rm = r0_xtal*sym.exp(-T0/T)\n",
    "\n",
    "# Availability\n",
    "Sp = Stot*Phi[iOpx]/(Phi[iOpx] + eps)\n",
    "Sm = Stot*Phi[iLq]/(Phi[iLq] + eps)\n",
    "\n",
    "# Affinity function\n",
    "fA = (M0/M_en)*A[j]/R/T\n",
    "\n",
    "Opx_melting = sym.Piecewise((rp*Sp*fA,A[j]>=0),(rm*Sm*fA,A[j]<0),(0,True))\n",
    "Opx_melting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reactants = [ En_Opx ] \n",
    "products = [ Fo_Lq, Si_Lq ]\n",
    "rxn.add_reaction_to_model('Opx_melting', reactants, products, Opx_melting, params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Si melting reaction"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reaction 2\n",
    "j = 2\n",
    "\n",
    "# Reactants\n",
    "Si_Si = ('Si_Si','Silica_polymorph','Silica_polymorph_berman')\n",
    "\n",
    "# Products\n",
    "#Si_Lq = ('Si_lq', 'Liquid', 'Quartz4_liquid')\n",
    "\n",
    "# Rate constant\n",
    "rp = r0_melt*sym.exp(-T0/T)\n",
    "rm = r0_xtal*sym.exp(-T0/T)\n",
    "\n",
    "# Availability\n",
    "Sp = Stot*Phi[iSi]/(Phi[iSi] + eps)\n",
    "Sm = Stot*Phi[iLq]/(Phi[iLq] + eps)\n",
    "\n",
    "# Affinity function\n",
    "fA = (M0/M_qz)*A[j]/R/T\n",
    "\n",
    "Si_melting = sym.Piecewise((rp*Sp*fA,A[j]>=0),(rm*Sm*fA,A[j]<0),(0,True))\n",
    "Si_melting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reactants = [ Si_Si ] \n",
    "products = [ Si_Lq ]\n",
    "rxn.add_reaction_to_model('Si_melting', reactants, products, Si_melting, params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The model_dict has now been updated to contain all of the information for these reactions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Return a dictionary of settable name value pairs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict = rxn.get_values()\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Update some of these values...\n",
    "\n",
    "Here we will set the forward and backward reaction rates \n",
    "\n",
    "$$\n",
    "    r_j^+=r_j^- =1\n",
    "$$\n",
    "\n",
    "and the total available reactive surface area $S_0=1$.  But these will be scaled using pre-factors and dimensionless Dahmkohler numbers in the geodynamics codes \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict.update(dict(name='fo_sio2_poly_linear_rxns',\n",
    "                        reference=reference,\n",
    "                        T0 = 1750.,\n",
    "                        R = 8.314,\n",
    "                        r0_xtal = 1.,\n",
    "                        r0_melt = 1.,\n",
    "                        Stot = 1.,\n",
    "                        eps = 0.1,\n",
    "                        M0 = 100.,\n",
    "                        M_fo = 140.691,\n",
    "                        M_en = 100.387,\n",
    "                        M_qz = 60.083                  \n",
    "                       ))\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and update them in the model_dict using the 'set_values' function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.set_values(values_dict)\n",
    "rxn.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generate Spud XML files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.to_xml(path=SPUD_DIR)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

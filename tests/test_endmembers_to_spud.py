import filecmp
import os
import shutil
from glob import glob
import subprocess

def test_endmembers_to_emml():
	tcg_home = os.environ['THERMOCODEGEN_HOME']
	path = os.path.abspath(tcg_home+'/share/thermocodegen/examples/Notebooks/coder_to_xml/endmembers')
	notebook = 'Example-00_Generate_endmembers.ipynb'
	subprocess.run(['jupyter', 'nbconvert', '--to', 'notebook', '--execute', path+'/'+notebook] )
	spuddirs = glob(path+'/spudfiles_*')
	try:
		shutil.rmtree('tmp')
	except:
		pass
	tmp_path = 'tmp/endmember_spudfiles'
	os.makedirs(tmp_path, exist_ok=True)
	for _dir in spuddirs:
		print('moving {}'.format(os.path.basename(_dir)))
		shutil.move(_dir, tmp_path)

def test_emmls():
	emmls = glob('data/endmember_spudfiles/*/*.emml')
	for file in emmls:
		test_file = file.replace('data', 'tmp')
		comp = filecmp.cmp(test_file, file)
		if not comp:
			print('Error:  mismatch in file {}'.format(os.path.basename(file)))
			assert filecmp.cmp(dev_file, file)

	print('Success! all files compare true')



if __name__ == "__main__":
	#test_generate_endmembers()
	test_emmls()
# -*- coding: utf-8 -*-
"Script to generate C++ and pythonbinding code from C code"

import sys
import os
import warnings
import thermocodegen as tcg
from thermocodegen.spudio import endmember
from thermocodegen.coder.coder import StdStateModel
from string import Template as template
import pandas as pd
import hashlib
import time

from ast import literal_eval


warnings.simplefilter('ignore')

def set_identifier(filename):
    """
    set a global identifier based on the current date and time and a hash
    of the spudfile

    :param filename: str
        name of the *ml file for code generation
    :return: str
        identifier composed of a hash of *ml file and current date and time
    """
    with open(filename,'rb') as f:
        sha = hashlib.sha1(f.read()).hexdigest()

    timestamp = time.asctime(time.localtime(time.time()))
    identifier = '{}:{}:{}'.format(os.path.basename(filename), sha, timestamp)
    return identifier

def main():
    # Path to templates
    tcg_home = os.environ.get('THERMOCODEGEN_HOME')
    if tcg_home is None:
        raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

    template_dir = tcg_home + '/share/thermocodegen/templates/cpp'

    # emml file
    emml_file = sys.argv[1]

    # Get date, time and hash for this spud file
    identifier = set_identifier(emml_file)

    # Path to build directory
    build_dir = sys.argv[2]

    # Get Spud derived endmember dictionary
    m_dict = endmember.read_endmember(emml_file, active='all',
                                      flatten_potentials=False)

    # Endmember name and filename
    endmembername = m_dict['name']
    filename = m_dict['filename']

    # Coder files prefix
    C_name = '{}_coder'.format(endmembername)

    # Create directory to put coder generated C source
    try:
        coder_src_dir = '{}/src/coder'.format(build_dir)
        os.makedirs(coder_src_dir)
    except:
        pass

    os.chdir(coder_src_dir)
    model = StdStateModel.from_dict(m_dict)

    # If --calibfile argument is an existing calibration file, extract the parameters
    # for this endmember
    calib_params = []
    calibration_file = sys.argv[3]
    if os.path.isfile(calibration_file):
        df = pd.read_csv(calibration_file,converters={'params': literal_eval})

    try:
        df_en = df.loc[df['type'] == 'endmember']
        # there has to be a cleaner way than this
        calib_params = df_en['params'].loc[df_en.name == endmembername].tolist()[0]
        print('{}: using calibration parameters -  {}'.format(endmembername, calib_params))
    except:
        pass

    model.write_code(silent=True,
                     identifier=identifier,
                     calib_params=calib_params)
    os.chdir(build_dir)

    # Make sure that directories exist
    try:
        os.makedirs('{}/include/endmembers'.format(build_dir))
    except FileExistsError:
        pass
    try:
        os.makedirs('{}/src/endmembers'.format(build_dir))
    except FileExistsError:
        pass

    # Touch include file if it doesn't exist
    open(build_dir + "/include/" + "endmembers.h", 'a').close()

    # Check if endmember needs to be included
    write_to_include = True
    with open(build_dir + "/include/" + "endmembers.h") as include_file:
        line = include_file.readlines()
        if '#include \"endmembers/{}.h\"\n'.format(endmembername) in line:
            write_to_include = False

    # Add endmember to include file
    if write_to_include:
        include_file = open(build_dir + "/include/" + "endmembers.h", "a")
        include_file.write('#include \"endmembers/{}.h\"\n'.format(endmembername))
        include_file.close()

    # Add include and src dirs
    include_dir = build_dir + "/include/endmembers/"
    src_dir = build_dir + "/src/endmembers/"

    # Load header template
    with open(template_dir + '/endmembers/endmember.h', 'r') as _file:
        header = _file.read()

    # Load source template
    with open(template_dir + '/endmembers/endmember.cpp', 'r') as _file:
        source = _file.read()

    ndict = dict(endmembername=endmembername,
                 C_name=C_name,
                 endmembername_uppercase=endmembername.upper(),
                 tcg_version=tcg.__version__,
                 tcg_git_sha=tcg.__git_sha__)
    header = template(header).safe_substitute(ndict)
    source = template(source).safe_substitute(ndict)

    # Write header file
    with open(include_dir + "{name}.h".format(name=endmembername), "w") as header_file:
        header_file.write(header)

    # Write source file
    with open(src_dir + "{name}.cpp".format(name=endmembername), "w") as source_file:
        source_file.write(source)

if __name__ == '__main__':
    main()

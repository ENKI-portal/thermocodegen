# -*- coding: utf-8 -*-
"Script to generate C++ code from rxml file"

import sys
import os
import thermocodegen as tcg
import thermocodegen.spudio as spudio
from string import Template as template
from thermocodegen.codegen.reaction import Reaction

# rxml file
rxml_file = sys.argv[1]

# Path to build directory
build_dir = sys.argv[2]

# Path to templates
tcg_home = os.environ.get('THERMOCODEGEN_HOME')
if tcg_home is None:
    raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

template_dir = tcg_home + '/share/thermocodegen/templates/cpp'

# Get Spud derived reaction dictionary
parser = spudio.Parser(rxml_file)
rx_dict = parser.generate_dict()

# Make sure that directories exist
try:
    os.makedirs('{}/include/reactions'.format(build_dir))
except OSError:
    pass
try:
    os.makedirs('{}/src/reactions'.format(build_dir))
except OSError:
    pass

# Touch include file if it doesn't exist
open(build_dir + "/include/" + "reactions.h", 'a').close()

# Add include and src dirs
include_dir = build_dir + "/include/reactions/"
src_dir = build_dir + "/src/reactions/"

# Endmember name and filename
rxname = rx_dict['name']

# Check if reaction needs to be included
write_to_include = True
with open(build_dir + "/include/" + "reactions.h") as include_file:
    line = include_file.readlines()
    if '#include \"reactions/{}.h\"\n'.format(rxname) in line:
        write_to_include = False
include_file.close()

# Add reaction to include file
if write_to_include:
    include_file = open(build_dir + "/include/" + "reactions.h", "a")
    include_file.write('#include \"reactions/{}.h\"\n'.format(rxname))
    include_file.close()

# Load header template
with open(template_dir + '/reactions/reaction.h', 'r') as _file:
    header = _file.read()

# Load c++ template
with open(template_dir + '/reactions/reaction.cpp', 'r') as _file:
    src = _file.read()

# Safe substitute templates
rx = Reaction.from_dict(model_dict=rx_dict)
rdict = rx.reaction_functions()

# Add tcg_version to rdict
rdict['tcg_version'] = tcg.__version__
rdict['tcg_git_sha'] = tcg.__git_sha__

header = template(header).safe_substitute(rdict)
src = template(src).safe_substitute(rdict)

# Write header file
with open(include_dir + "{name}.h".format(name=rxname), "w") as header_file:
    header_file.write(header)

header_file.close()

# Write cpp file
with open(src_dir + "{name}.cpp".format(name=rxname), "w") as cpp_file:
    cpp_file.write(src)

cpp_file.close()

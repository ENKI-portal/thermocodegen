Build Scripts
=============

ThermoCodegen provides three primary scripts for generating

    * Thermodynamic Databases (:doc:`tcg_builddb`)
    * Kinetic Reaction Objects (:doc:`tcg_buildrx`)
    * Thermo DB parameter space files (:doc:`tcg_dbparams`)

.. toctree::
   :maxdepth: 1
   :caption: Contents
   
   tcg_builddb
   tcg_buildrx
   tcg_dbparams

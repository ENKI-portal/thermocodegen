Tester Class
===============

:class:`thermocodegen.testing.Tester` is a convenience  class that
provides routines  for testing all the interfaces of ThermoCodegen
generated python objects (i.e., :doc:`EndMember <py_endmember>`, :doc:`Phase <py_phase>`,
and :doc:`Reaction <py_reaction>` classes).
as well as the :class:`thermoengine.model.Database` class.

The primary functionality is to produce pandas dataframes with both
input data (e.g. temperature and pressure) for each thermodynamic
object as well as the output from all available methods that take
those parameters such as :class:`Endmember.g(T,P)`.

These dataframes can then be saved  as ``.csv`` files or used to generate tests for pytest.

Use of :class:`thermocodegen.testing.Tester` is demonstrated in several notebooks in the examples
e.g. :doc:`examples/Systems/fo_fa/notebooks/Generate_tests.ipynb <notebooks/fo_fa/Generate_tests>`. The
pytest files are also used as regression tests for further development
of ThermoCodegen and ThermoEngine.


.. currentmodule:: thermocodegen.testing
.. autoclass:: Tester
   :members:
   :undoc-members:
.. autoclass:: DBTester
   :members:
   :undoc-members:
   :show-inheritance:
.. autoclass:: ModelDBTester
   :members:
   :undoc-members:
   :show-inheritance:

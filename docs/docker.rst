Running TCG with docker
===================================

The most current docker container is available at gitlab via

.. code-block:: bash

   docker pull registry.gitlab.com/enki-portal/thermocodegen:tf-focal

which also contains a full implementation of
`TerraFERMA <https://terraferma.github.io>`_  as well as jupyterlab support.

MacOS
-----

To use the option file GUI ``diamond`` you will need to run docker
with X11 functionality.  On MacOS we use `XQuartz <https://www.xquartz.org/>`_
as an X server, with "Allow connections from network clients" enabled under 
the security preferences (a reboot may be required if this setting is being applied for the first time), and set the display address with

.. code-block:: bash

   open -a XQuartz
   ip=$(ifconfig | grep broadcast | awk '$1=="inet" {print $2}')
   xhost + ${ip}

.. note::
    This command may produce ambiguous IP addresses on some systems.  In this case please modify it to select the active address on the machine.

After setting the IP address, docker can be run using

.. code-block:: bash

    cd <working directory>
    docker run --rm -it -p 8888:8888 -e DISPLAY=${ip}:0 -v ${PWD}:/home/tfuser/shared registry.gitlab.com/enki-portal/thermocodegen:tf-focal

This command will also mount your working directory from within the container at   ``/home/tfuser/shared/``.

.. note::

    The option ``-p 8888:8888`` sets port forwarding from the container to the host machine and  is necessary for running jupyter-lab
    from within the container. If port 8888 is already in use, you can use any other port but it must be consistent
    between docker and the jupyter server (see more below).

When you are finished using ThermoCodegen and exit docker you should run

.. code-block:: bash

    xhost - ${ip}

to remove the IP address from xhost.

Linux
-----

On linux, we recommend using :doc:`singularity<singularity>` but docker may also be used with the command

.. code-block:: bash

    cd <working directory>
    docker run --rm -it -p 8888:8888 -e DISPLAY=${DISPLAY} -v ${PWD}:/home/tfuser/shared -e HOST_UID=`id -u` -e HOST_GID=`id -g` -v /tmp/.X11-unix:/tmp/.X11-unix registry.gitlab.com/enki-portal/thermocodegen:tf-focal

The additional options ``-e HOST_UID=`id -u``` and ``-e HOST_GID=`id -g``` set the user and group ids of the user within the docker
container to match those on the host machine.  ``-v /tmp/.X11-unix:/tmp/.X11-unix`` is necessary for X11 functionality.

Working with the examples
----------------------------------

A set of example thermodynamic systems are included in the container. To get started, on the host machine

.. code-block:: bash

    mkdir examples
    cd examples
    docker run  --rm -it -p 8888:8888 -e DISPLAY=${ip}:0 -v ${PWD}:/home/tfuser/shared registry.gitlab.com/enki-portal/thermocodegen:tf-focal

then from within the container

.. code-block:: bash

    cp -r systems shared
    cd shared/systems
    ls

which should show 4 different directories for the Example Systems

* ``fo_fa``: :doc:`fo_fa`.
* ``fo_sio2``: :doc:`fo_sio2`. A custom asymmetric regular solution from Tweed (2021).
* ``MgFeSiO4_Stixrude``: :doc:`Stixrude`: A selection of phases in the MgFeSiO4 system using the
  thermodynamic models of Stixrude and Lithgow-Bertelloni (2005, 2011).
* ``fo_h2o``: :doc:`fo_h20` using the Ghiorso SWIM Water model.

Quick check
------------

To check that the docker installation is working, try

.. code-block:: bash

    cd fo_fa
    bash build_system.sh

Which will generate  all the  thermodynamic model descriptions and source code and compile it into
a loadable python module and test it using ``pytest``.

A successful test will complete with

.. code-block:: bash

    ===================================================================== test session starts ======================================================================
    platform linux -- Python 3.8.10, pytest-4.6.9, py-1.11.0, pluggy-0.13.1
    rootdir: /home/tfuser/shared/Systems/fo_fa
    collected 274 items

    tests/test-v0.6.9-2022-07-21_00:15:09/test_endmembers_21-Jul-2022_00:15:09.py .......................................................................... [ 27%]
    ..................................                                                                                                                       [ 39%]
    tests/test-v0.6.9-2022-07-21_00:15:09/test_phases_21-Jul-2022_00:15:10.py .............................................................................. [ 67%]
    ......................................................                                                                                                   [ 87%]
    tests/test-v0.6.9-2022-07-21_00:15:09/test_rxns_21-Jul-2022_00:15:11.py ..................................                                               [100%]

    ================================================================== 274 passed in 1.63 seconds ==================================================================

Full details of the workflow described in ``build_system.sh`` are provided for each example in  :doc:`examples`.

Running Jupyter Lab
--------------------

To test your ``jupyter-lab`` installation in the container and view it from your local browser start by loading the environment module (assuming you are still in the ``fo_fa`` directory)

.. code-block:: bash

    module load ./reactions/fo_fa_binary/fo_fa_binary.module

Then start ``jupyter-lab`` within the container

.. code-block:: bash

    jupyter-lab --ip 0.0.0.0 --port=8888 --no-browser --allow-root

.. note::

    the port number passed to jupyter lab must agree with the `-p` option passed to docker

On start up, the jupyter server should produce terminal output similar to

.. code-block:: bash

    To access the server, open this file in a browser:
        file:///home/tfuser/.local/share/jupyter/runtime/jpserver-629-open.html
    Or copy and paste one of these URLs:
        http://18238b848dc0:8888/lab?token=5a8136d8efa7f969189ba5c971e42f8b40a4988bc73158f6
     or http://127.0.0.1:8888/lab?token=5a8136d8efa7f969189ba5c971e42f8b40a4988bc73158f6

Cut and paste the last line, e.g.

.. code-block:: bash

    http://127.0.0.1:8888/lab?token=5a8136d8efa7f969189ba5c971e42f8b40a4988bc73158f6

into the browser on the host machine and you should be good to go.

As a test, in jupyter, open and run the notebook ``notebooks/Examples_Fo_Fa_system.ipynb`` and compare the output
to :doc:`Examples_Fo_Fa_system.ipynb <notebooks/fo_fa/Examples_Fo_Fa_system>`.






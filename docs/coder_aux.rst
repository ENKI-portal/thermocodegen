Auxiliary Coder Functions:
===========================

:mod:`thermocodegen.coder` also supplies some additional utility functions for
interacting with coder inputs.  The most common user facing functions are


.. currentmodule:: thermocodegen.coder.coder

.. autofunction::  set_coder_params

.. autofunction:: get_symbol_dict_from_params

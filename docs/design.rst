Software Design
===================================

The design of ThermoCodegen  follows the hierarchical structure of
thermodynamic modeling commonly used in Earth Sciences and illustrated in
:ref:`figure-1`.

.. _figure-1:

.. figure:: images/hierarchical_thermodynamics.png
  :width: 90%
  :align: center

  **Figure 1**

  The hierarchical structure of thermodynamic modeling (image adapted from [Tweed_2021]_).

At the lowest level are thermodynamic **Endmembers** which provide chemical
potentials, :math:`\mu(T,P)`, for  stoichiometric chemical components.  The actual
components can be arbitrary (e.g. oxides, minerals, ionic species etc.) but have
fixed chemical composition and whose thermodynamic properties only depend on
temperature and pressure (or temperature and volume, for models of the Helmholtz
free energy).

Given a space of thermodynamic endmembers, we can  construct homogeneous  **Phases**
as combinations of endmembers and develop thermodynamic models for  their Gibbs
free energy, :math:`G(T,P,\mathbf{n})`, that depend on temperature, pressure and the
number of mols of each endmember :math:`n_k`.

.. note::

  In ThermoCodegen we don't make a distinction between `pure` phases and `solution` phases. A pure phase is simply a phase with a single endmember and total Gibbs Free Energy:

  .. math::

    G(T,P,n) = n\mu(T,P)



Given :math:`G(T,P,\mathbf{n})` for each phase,  all of the
other thermodynamic properties can be derived  by partial differentiation of :math:`G`.
For example

.. math::

	\begin{matrix}
    S = -\frac{\partial G}{\partial T} & V = \frac{\partial G}{\partial P} & \mu^k = \frac{\partial G}{\partial n_k}\\
    c_p = T\frac{\partial S}{\partial T}=-T\frac{\partial^2 G}{\partial T^2} & &
    \alpha = \frac{1}{V}\frac{\partial V}{\partial T} = \frac{1}{V}\frac{\partial^2 G}{\partial P^2}\\
  \end{matrix}

Thus if we can define an analytic model for :math:`G(T,P,\mathbf{n})` for any phase,
we can use symbolic differentiation and code-generation to provide  custom routines to
generate all thermodynamic properties consistently.

We define a set of phases, together with their constituent thermodynamic
endmembers as a **Thermodynamic Database** corresponding to the blue box in
:ref:`figure-1`. Standard databases available in the literature include the
mineral database of  [Berman_1988]_ and its extension to silicate liquids
through the  MELTs family models [Ghiorso_and_Sack_1995]_,
[Ghiorso_et_al_2002]_, as well as the high-pressure mineral phases of
[Stixrude_Lithgow-Bertelloni_2005]_, [Stixrude_Lithgow-Bertelloni_2011]_, or
metamorphic phases of e.g. [Holland_and_Powell_2011]_.  All of these are
currently available through python bindings to ObjectiveC code in  `ThermoEngine <https://gitlab.com/ENKI-portal/ThermoEngine>`_. However, the ObjectiveC
objects are not readily incorporated in other codes.

ThermoEngine also provides support for generating custom thermodynamic models of
endmembers and phases  through the :mod:`thermoengine.coder` module written by
Mark Ghiorso.  :mod:`thermoengine.coder`  uses `SymPy <https://www.sympy.org/en/index.html>`_ to
describe the Gibbs Free Energy of endmembers and phases and then use automatic
differentiation and code-generation to write efficient  C source code (and wrap
it in `Cython <https://cython.org/>`_) for all thermodynamic properties of phases.
:mod:`thermocodegen.coder` generates the same underlying C code and interfaces
as :mod:`thermoengine.coder` but extends its functionality in several critical
ways  for better integration with large scale geodynamic modeling.  The primary
extensions are listed in the :doc:`introduction<intro>` and detailed below.

ThermoCodegen
-------------

ThermoCodegen is designed to produce  code for just the colored boxes in
:ref:`figure-1`.

Custom Thermodynamic Databases
______________________________

For **Thermodynamic Databases** of endmembers and phases,
ThermoCodegen extends  :mod:`thermoengine.coder` to record the internal state of
each thermodynamic model as a python dictionary in order to  store and generate
code from xml files that completely describe all variables, parameters and SymPy
models for endmembers and phases.  These xml files are parsed and viewed using the `spud <https://github.com/FluidityProject/spud>`_
package (see :doc:`coder` for details).  Endmembers are stored with the extension ``.emml`` and
phases are stored as ``.phml`` files.  Both can be viewed with the spud GUI ``diamond``.
Given a set of consistent endmember and phase description files,  the ThermoCodegen script :ref:`tcg_builddb`
can be used to inspect, validate and autogenerate code for a custom **ThermoDynamic Database**
as well as provide C++ wrappers and python bindings using `pybind11
<https://pybind11.readthedocs.io/en/stable/>`_.

At this point,  all source code, together with ``.emml`` and ``.phml`` files
describing the full thermodynamic models, and optionally compiled C++ and python
libraries are made available as  a compressed tarball.  This object is
considered a **Thermodynamic Database** and can be used locally or made
available as a web-accessible URL,  for example with a zenodo DOI

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.7976277.svg
   :target: https://doi.org/10.5281/zenodo.7976277

Given a custom thermodynamic database,  these can then be used in optimization codes such as
:mod:`thermoengine.equilibrate` to find equilibrium assemblages that minimize the global Gibbs free energy.

Reaction Kinetic Objects
________________________

For better integration with open-system geodynamics codes, however, that don't
assume equilibrium,  ThermoCodegen also provides code for generating C++
libraries and python bindings for custom **Reaction Kinetics Models** that describe
a set of reactions  between endmembers and phases from a given thermodynamic
database. The software provides interfaces for affinities, reaction rates, and
thermodynamic properties of phases as well as their exact derivatives for use in
non-linear solvers that take Jacobians. These reaction kinetic models are stored
as xml files with the extension ``.rxml`` from which custom C++ libraries and
python bindings are generated using the build script :ref:`tcg_buildrx`. C++ interfaces for
reaction objects are described in detail in :doc:`reaction` along with their respective
:doc:`python bindings <py_reaction>`.


These reaction libraries, along with the underlying database objects can then be
incorporated in external modeling software such as `ThermoEngine
<https://gitlab.com/ENKI-portal/ThermoEngine>`_, `TerraFERMA
<https://terraferma.github.io>`_, `ASPECT
<https://aspect.geodynamics.org/>`_ and generic jupyter notebooks.
Inclusion in dynamic models is considered external to ThermoCodegen however,
the supplied containers also include a full distribution of TerraFERMA and
the examples provide a few simple TerraFERMA input files as examples.

Specific examples for simple thermodynamic systems can be found in the
:doc:`examples` section of this documentation and are provided with the
software.   The remainder of this section outlines the general workflow for
using ThermoCodegen.

Overview of Thermodynamic modeling workflow
-------------------------------------------

Generating Thermodynamic Databases
___________________________________

There are several possible workflows for creating a custom ThermoDynamic Database of endmembers and phases:

1. Generate endmember (``.emml``) and phase description files (``.phml``) from scratch.

  a. Use SymPy to derive and describe  Thermodynamic models of :math:`G(T,P)` for endmembers and store complete models and parameters in xml files with extension ``.emml`` (endmember markup language).  These models can be quite complicated and are best derived and described in jupyter notebooks and many examples are included in the :doc:`examples<examples>`.
  b. Use SymPy  to derive and describe thermodynamic models of  :math:`G(T,P,\mathbf{n})` for phases in jupyter notebooks and store in ``.phml`` files (phase markup language).

2. Or:  Use precompiled ``.emml`` and ``.phml`` files to construct a custom set of phases with corresponding end-members.
3. Use the script :ref:`tcg_builddb`  to inspect, validate and auto-generate source code for a **Thermodynamic Database** from a set of ``.emml`` and ``.phml`` files.

Generating Reaction Kinetics models
___________________________________

Given a thermodynamic database (either created locally or pre-generated and
accessible through a URL), we can then construct a set of kinetic reaction
models over the space of endmembers and phases described in the database.  The
workflow for these steps are:

1. Use SymPy to derive and describe kinetic models of chemical Reactions  :math:`R_j(T,P,C,\Phi)`  between endmembers and phases available in the database using the :ref:`Reaction` class.  The full description of the kinetics models and a link to the underlying database is stored as a ``.rxml``  (reaction markup language) file. Again,  the derivation and description of these kinetics models is most usefully done in jupyter notebooks.
2. Use the script :ref:`tcg_buildrx` to autogenerate C++ source, and compiled C++ libraries and python bindings for these **Reaction Objects**.  The script also creates several files for setting environment variables (e.g. PYTHONPATH) for  using these libraries in other C++ or python codes.

Fully worked out examples of the complete work flow  as well as detailed jupyter
notebooks are described in the :doc:`examples` section of this documentation.

References
----------
.. [Berman_1988] Berman, R.G., 1988. Internally-Consistent Thermodynamic Data for Minerals in the System Na2O-K2O-CaO-MgO-FeO-Fe2O3-Al2O3-SiO2-TiO2-H2O-CO2. J Petrology 29, 445–522. https://doi.org/10.1093/petrology/29.2.445
.. [Ghiorso_and_Sack_1995] Ghiorso, M.S., Sack, R.O., 1995. Chemical mass transfer in magmatic processes IV. A revised and internally consistent thermodynamic model for the interpolation and extrapolation of liquid-solid equilibria in magmatic systems at elevated temperatures and pressures. Contr. Mineral. and Petrol. 119, 197–212. https://doi.org/10.1007/BF00307281
.. [Ghiorso_et_al_2002] Ghiorso, M.S., Hirschmann, M.M., Reiners, P.W., Kress, V.C., 2002. The pMELTS: A revision of MELTS for improved calculation of phase relations and major element partitioning related to partial melting of the mantle to 3 GPa. Geochemistry, Geophysics, Geosystems 3, 1–35. https://doi.org/10.1029/2001GC000217
..
  [unused].. [Holland_and_Powell_1998] Holland, T.J.B., Powell, R., 1998. An internally consistent thermodynamic data set for phases of petrological interest. Journal of Metamorphic Geology 16, 309–343. https://doi.org/10.1111/j.1525-1314.1998.00140.x
..
  [unused].. [Holland_and_Powell_2004] Holland, T.J.B., Powell, R., 2004. An internally consistent thermodynamic data set for phases of petrological interest. Journal of Metamorphic Geology 16, 309–343. https://doi.org/10.1111/j.1525-1314.1998.00140.x
.. [Holland_and_Powell_2011] Holland, T.J.B., Powell, R., 2011. An improved and extended internally consistent thermodynamic dataset for phases of petrological interest, involving a new equation of state for solids. Journal of Metamorphic Geology 29, 333–383. https://doi.org/10.1111/j.1525-1314.2010.00923.x
.. [Stixrude_Lithgow-Bertelloni_2005] Stixrude, L., Lithgow-Bertelloni, C., 2005. Thermodynamics of mantle minerals - I. Physical properties. Geophysical Journal International 162, 610–632. https://doi.org/10.1111/j.1365-246X.2005.02642.x
.. [Stixrude_Lithgow-Bertelloni_2011] Stixrude, L., Lithgow-Bertelloni, C., 2011. Thermodynamics of mantle minerals - II. Phase equilibria. Geophysical Journal International 184, 1180–1213. https://doi.org/10.1111/j.1365-246X.2010.04890.x
.. [Tweed_2021] Tweed, Lucy, 2021, Ph.D, Thesis: Coupling the Thermodynamics, Kinetics and Geodynamics of Multiphase Reactive Transport in Earth’s Interior. https://doi.org/10.7916/d8-wh72-vb08

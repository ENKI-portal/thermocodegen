Contributing
============

Contributions are welcome to ThermoCodegen along with suggestions for improvements.  Please either propose a merge request with
suggested changes or raise an issue to start a discussion about a bug or feature request.

Development
###########

ThermoCodegen has two principal branches, ``dev`` and ``main``:

* ``main`` is the most stable branch, on which the documentation and docker images are based
* ``dev`` is the development branch, where the latest features are first made available

Merge Requests
##############

If you have a new feature or bug fix that you would like to see merged back into the principal `ThermoCodegen
repository <https://gitlab.com/ENKI-portal/ThermoCodegen>`_ please open a new merge request through `ThermoCodegen's merge request
tracker <https://gitlab.com/ENKI-portal/ThermoCodegen/-/merge_requests>`_.  Please base any changes off and propose merge requests into the ``dev`` branch.

A typical workflow for proposing a merge request follows:

1. Fork the repository to your username on gitlab.
2. Clone your fork of the respository to a local machine.
3. Create and checkout a new feature branch based off the ``dev`` branch.
4. Commit any changes to the new feature branch and push it to your fork of the repository.
5. Open a merge request from your feature branch (on your fork) to the ``dev`` branch of the ENKI-portal ThermoCodegen repository.
6. This will trigger automatic regression testing of the feature branch prior to review.
7. Following successful testing, the changes in the feature branch will be reviewed and appropriate modifications suggested by the ThermoCodegen maintainers.
8. Make any agreed upon changes and commit and push them to the feature branch.
9. The tested, reviewed and modified merge request will then be merged into the ``dev`` branch by the ThermoCodegen maintainers.

The ``dev`` branch is periodically merged into the ``main`` branch once new features are considered stable for release.  This
regenerates the online documentation and normally accompanies a rebuild of the docker containers.

Testing
#######

Automatic testing of ThermoCodegen takes places after every commit and prior to any merge request.  Details of these tests can be
found at `ThermoCodegen's test pipeline tracker <https://gitlab.com/ENKI-portal/ThermoCodegen/-/pipelines>`_.

Issues 
######

Please report bugs, make feature requests or start discussions about features of the code 
by raising a new issue through `ThermoCodegen's issue tracker <https://gitlab.com/ENKI-portal/ThermoCodegen/-/issues>`_.

Code of Conduct
###############

Please note that we have a code of conduct. Please follow it in all your interactions while working on ThermoCodegen.

Our Pledge
**********

In the interest of fostering an open and welcoming environment, contributors and maintainers must pledge to making participation in our project and our community a harassment-free experience for everyone.

Our Standards
*************

Examples of behavior that contributes to creating a positive environment:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants:

* Using sexualized language or imagery, and unwelcome sexual attention or advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic address, without explicit permission
* Other conduct that could reasonably be considered inappropriate in a professional setting

Our Responsibilities
********************

Project maintainers are responsible for clarifying the standards of acceptable behavior and are expected to take appropriate and fair corrective action in response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to ban temporarily or permanently any contributor for other behaviors that they deem inappropriate, threatening, offensive, or harmful.

Attribution
***********

This Code of Conduct is adapted from the `Contributor Covenant <http://contributor-covenant.org>`_, version 1.4, available at
`http://contributor-covenant.org/version/1/4 <http://contributor-covenant.org/version/1/4/>`_.


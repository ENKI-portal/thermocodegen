Python API Reference
====================

.. _py_code_generation:

Code Generation Classes
-----------------------

.. toctree::
   :maxdepth: 2

   coder
   reaction_generation

.. _py_thermo_classes:

Thermodynamic object Classes
----------------------------

.. toctree::
   :maxdepth: 1

   py_endmember
   py_phase
   py_reaction


ThermoCodegen Testing Classes
-----------------------------

.. toctree::
   :maxdepth: 1

   testing

Auxiliary Coder functions
-------------------------

.. toctree::
  :maxdepth: 1

  coder_aux

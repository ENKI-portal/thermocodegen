# -*- coding: utf-8 -*-
"Script to generate dummy pybind11 module"

import lxml.etree
import glob
import os
import re
from string import Template as template


def parse_doxygen_xml(xml_type, default_return):
    """Read doxygen xml output and return methods for each class."""

    methods = """"""
    return_str = """ {{
    return {};
  }}\n"""

    xml_file = 'class_' + xml_type + '.xml'
    for filename in glob.glob('../../doxygen/build/xml/' + xml_file):
        f = open(filename, "r")
        xml = lxml.etree.parse(f)
        for func in xml.xpath('//memberdef[@kind="function"]'):
            definition = func.xpath('.//definition/text()')[0]
            definition = definition.split()

            if(len(definition) > 2):
                args = func.xpath('.//argsstring/text()')[0][:-2]
                name = func.xpath('.//name/text()')[0]
                return_type = ''.join(definition[1:-1])
                method = '  ' + return_type + ' ' + name + args
                method += return_str.format(default_return[return_type])
                methods += method + '\n'

    return methods


def main():
    # Path to templates
    template_dir = '../../../templates'
    template_dir += '/cpp/py_bindings/'

    # Load templates
    with open(template_dir + 'py_bindings.cpp', 'r') as _file:
        py_bindings = _file.read()

    with open(template_dir + 'py_endmember.cpp', 'r') as _file:
        py_endmember = _file.read()

    with open(template_dir + 'py_phase.cpp', 'r') as _file:
        py_phase = _file.read()

    with open(template_dir + 'py_reaction.cpp', 'r') as _file:
        py_reaction = _file.read()

    # Substitute into templates
    endmember = template(py_endmember).safe_substitute({'name': 'EndMember'})
    phase = template(py_phase).safe_substitute({'name': 'Phase'})
    reaction = template(py_reaction).safe_substitute({'name': 'Reaction'})

    py_bindings_dict = {'include_files': '',
                        'module_name': 'System',
                        'phases': '{}',
                        'pybind_endmembers': endmember,
                        'pybind_phases': phase,
                        'pybind_reactions': reaction}

    py_bindings = template(py_bindings).safe_substitute(py_bindings_dict)

    # Define dictionary of default return values for each function type
    default = {'std::string': '""',
               'void': '',
               'int': '0',
               'double': '0.',
               'std::vector<double>': '{}',
               'std::vector<std::string>': '{}',
               'std::vector<std::shared_ptr<EndMember>>': '{}',
               'std::vector<std::vector<double>>': '{{}}',
               'std::vector<std::shared_ptr<Phase>>': '{}',
               'std::vector<std::vector<std::vector<double>>>': '{{{}}}'}

    # Generate list of methods for each class
    endmember_methods = parse_doxygen_xml('end_member', default)
    phase_methods = parse_doxygen_xml('phase', default)
    reaction_methods = parse_doxygen_xml('reaction', default)

    # pybind11 module template
    src = """#include <string>
#include <vector>
#include <memory>

class EndMember {
public:
${endmember_methods}
};

class Phase {
public:
${phase_methods}
};

class Reaction {
public:
${reaction_methods}
};

${py_bindings}"""

    # Substitute methods into template and write to .cpp file
    src_dict = {'endmember_methods': endmember_methods,
                'phase_methods': phase_methods,
                'reaction_methods': reaction_methods,
                'py_bindings': py_bindings}

    src = template(src).safe_substitute(src_dict)

    text_file = open("main.cpp", "w")
    text_file.write(src)
    text_file.close()


if __name__ == '__main__':
    main()

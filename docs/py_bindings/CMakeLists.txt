cmake_minimum_required(VERSION 3.0.0)
project(System)


execute_process(COMMAND python3 ${CMAKE_SOURCE_DIR}/generate_src.py)

set(TCG_INCLUDE_DIR $ENV{THERMOCODEGEN_HOME}/include)
set(SWIMDEW_INCLUDE_DIR $ENV{THERMOCODEGEN_HOME}/include/swimdew)
include_directories(${TCG_INCLUDE_DIR})

# Set some C++ flags
set(CMAKE_CXX_STANDARD 14)

find_package(pybind11)
pybind11_add_module(System main.cpp)

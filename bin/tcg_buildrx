#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

# Copyright (C) 2018 Columbia University in the City of New York and ENKI
# Project.
#
# Please see the AUTHORS file in the main source directory for a full list
# of contributors.
#
# This file is part of ThermoCodegen.
#
# ThermoCodegen is free software released under the MIT license
# you should have recieved a copy of the License along with ThermoCodegen
import os
import sys
from glob import glob
import shutil
import subprocess
import tempfile
import tarfile
import thermocodegen as tcg
import thermocodegen.spudio as spudio
from thermocodegen.spudio.reaction import  fetch_database_message, fetch_database, get_files_parsers, copy_rxml_file
import argparse

try:
    import argcomplete
except ImportError:
    pass


def maxlen(strings):
    """Returns the length of the longest string in a list of strings"""
    maxlen = 0
    for s in strings:
        maxlen = max(maxlen, len(s))
    return maxlen


def print_endmembers(endmembers):
    """Pretty-print names of endmembers and basic information"""
    if len(endmembers) != 0:
        filenames = [os.path.basename(p.filename) for p in endmembers]
        names = [p.name for p in endmembers]
        formulas = [p.formula for p in endmembers]
        df = maxlen(filenames) + 2
        dn = maxlen(names) + 2
        dfor = maxlen(formulas) + 2
        fmtc = '{{:^{dn}}}{{:^{dfor}}}{{:^{df}}}'.format(df=df, dn=dn,
                                                         dfor=dfor)
        fmt = '{{:>{dn}}}{{:>{dfor}}}{{:>{df}}}'.format(df=df, dn=dn,
                                                        dfor=dfor)

        print('**** Available Endmembers ***')
        print(fmtc.format('Name', 'Formula', 'File'))
        for f, n, form in zip(filenames, names, formulas):
            print(fmt.format(n, form, f))
        print('\n')


def print_phases(phases):
    """Pretty print names and basic information for phases"""
    if len(phases) != 0:
        filenames = [os.path.basename(p.filename) for p in phases]
        abbrev = [p.phase_abbrev for p in phases]
        names = [p.name for p in phases]
        df = maxlen(filenames) + 2
        da = maxlen(abbrev) + 2
        dn = maxlen(names) + 2
        fmtc = '{{:^{da}}}{{:^{dn}}}{{:^{df}}}'.format(df=df, da=da,
                                                       dn=dn)
        fmt = '{{:<{da}}}{{:<{dn}}}{{:>{df}}}'.format(df=df, da=da,
                                                      dn=dn)

        print('**** Available Phases ***')
        print(fmtc.format('Abbrev', 'Name', 'File'))
        for f, a, n in zip(filenames, abbrev, names):
            print(fmt.format(a, n, f))
        print('\n')


def validate_phases(rx_dict, phases):
    """Validate phase names in reactions against the  phases in the database.

    rx_dict: reaction dictionary from parsing .rxml file
    phases: list of phase parsers from database
    """

    # generate unique sets of names
    phase_set = set([p.name for p in phases])

    print('**** Validating phases in Reaction file ***')
    rxn_phase_names = [p['name'] for p in rx_dict['phases']]
    bad_phases = [p for p in rxn_phase_names if p not in phase_set]
    if len(bad_phases) > 0:
        for p in bad_phases:
            print('phase {} not in database'.format(p))
    else:
        print('All reaction phases in database\n')


def validate_reactions(rx_dict, phases):
    """Validate endmember and phase names in reactions against the
    endmembers and phases in the database.

    rx_dict: reaction dictionary from parsing .rxml file
    endmembers: list of endmember parsers from database
    phases: list of phase parsers from database
    """

    # generate unique sets of names
    phase_set = set([p.name for p in phases])

    # set dictionary of phase and endmember names
    p_dict = {p.name: p.endmembers for p in phases}

    print('**** Validating reactions against database ***')

    # validate individual reaction pairs
    for j, rx in enumerate(rx_dict['reactions']):
        print('\nreaction {}:\t {}'.format(j, rx['name']))
        # check reactants
        print('  reactants ', end='')
        for r in rx['reactants']:
            rph = r[1]
            rem = r[2]
            if rph not in phase_set or rem not in p_dict[rph]:
                print('\n  Bad reactant pair: {}'.format(r))
            else:
                print(' OK')
        print('  products  ', end='')
        for r in rx['products']:
            rph = r[1]
            rem = r[2]
            if rph not in phase_set or rem not in p_dict[rph]:
                print('\n  Bad product pair: {}'.format(r))
            else:
                print(' OK ', end='')
    print('\n')


def create_build_dir(build_dir, rx_dict):
    """Create build directory, and untar database into it"""
    try:
        os.mkdir(build_dir)
    except OSError:
        pass

    # fetch the database tarball URL to a temporary directory
    with tempfile.TemporaryDirectory() as tmpdirname:
        try:
            path = fetch_database(rx_dict['database'], tmpdirname)
        except TypeError as err:
            print(err)
            sys.exit(1)
        tar = tarfile.open(path, mode="r")
        tar.extractall(path=build_dir)
        tar.close()


def configure(build_dir, rx_file, args):
    """Create build directory, copy spudfiles and run cmake"""
    spud_dir = build_dir+'/spudfiles'
    try:
        os.makedirs(spud_dir)
    except OSError:
        pass
    copy_rxml_file(rx_file, spud_dir)

    # for py_bindings also copy endmember and phase files into spud_dir
    for f in efiles + pfiles:
        shutil.copy(f, spud_dir)

    # run cmake in the builddirectory
    cmake_path = os.getenv('THERMOCODEGEN_CMAKE_PATH')+'/reactions'
    if cmake_path is None:
        raise Exception("Could not find THERMOCODEGEN_CMAKE_PATH in environment.")
    if args.debug:
        buildstring = '-DCMAKE_BUILD_TYPE=Debug'
    else:
        buildstring = '-DCMAKE_BUILD_TYPE=Release'

    p = subprocess.Popen(['cmake', buildstring,
                          '-DCMAKE_INSTALL_PREFIX={}'.format(install_dir),
                          '-DLIBRARY_NAME={}'.format(libraryname),
                          '-DDATABASE_DIR={}'.format(database_dir),
                          '-DINCLUDE_SWIM={}'.format(args.include_swim),
                          '-DPY_LIBRARY_NAME={}'.format(py_libraryname),
                          cmake_path], cwd=build_dir)
    retcode = p.wait()
    if retcode != 0:
        raise Exception("{} returned error code {} in directory {}."
                        .format('cmake', retcode, build_dir))


def make(build_dir):
    """Run make in build directory"""
    p = subprocess.Popen(['make'], cwd=build_dir)
    retcode = p.wait()
    if retcode != 0:
        raise Exception("{} returned error code {} in directory {}."
                        .format('make', retcode, build_dir))


def install(build_dir):
    """Run make in build directory"""
    p = subprocess.Popen(['make', 'install'], cwd=build_dir)
    retcode = p.wait()
    if retcode != 0:
        raise Exception("{} returned error code {} in directory {}."
                        .format('make install', retcode, build_dir))


def tcg_buildrx_parser():
    """Return the parser"""
    parser = argparse.ArgumentParser(
        description="""Generate a library and bindings from a reaction \
        .rxml file""")
    parser.add_argument('rxfile', nargs='?',type=str, default=None, help='name of the .rxml file')
    parser.add_argument('-e', '--endmembers', action='store_const',
                        dest='endmembers', const=True, default=False,
                        required=False,
                        help='List available endmembers from database')
    parser.add_argument('-p', '--phases', action='store_const', dest='phases',
                        const=True, default=False,
                        required=False,
                        help='List available phases from database')
    parser.add_argument('-v', '--validate', action='store_const',
                        dest='validate', const=True, default=False,
                        required=False,
                        help='Validate reactions against available phases '
                             'and endmembers')
    parser.add_argument('-c', '--clean', action='store_const', dest='clean',
                        const=True, default=False, required=False,
                        help='Clean the build directory')
    parser.add_argument('-b', '--build', action='store_const', dest='build',
                        const=True, default=False, required=False,
                        help='Configure and compile library')
    parser.add_argument('-i', '--install', action='store',
                        metavar='install_dir', dest='install_dir', nargs='?',
                        type=str, const='current', default=None,
                        help='Configure, build and install library to '
                             'install_directory (defaults to database '
                             'directory)')
    parser.add_argument('-l', '--libraryname', action='store',
                        metavar='libraryname', dest='libraryname', type=str,
                        default=None,
                        help='Optional name of the library (defaults to '
                             'directoryname)')
    parser.add_argument('--include_swim', action='store_const',
                        dest='include_swim', const=True, default=False,
                        required=False,
                        help='Include SWIM endmember')
    parser.add_argument('--debug', action='store_const', dest='debug',
                        const=True, default=False, required=False,
                        help='Build debuggable version with '
                             'CMAKE_BUILD_TYPE=Debug '
                             '(default is Release)')
    parser.add_argument('--py_libraryname', action='store',
                        metavar='py_libraryname', dest='py_libraryname',
                        type=str, default=None,
                        help='Optional name of the pybind11 object '
                             '(defaults to py_{libraryname})')
    parser.add_argument('--version', action='store_const', dest='version',
                        const=True, default=False, required=False,
                        help='return Thermocodegen Version')
    return parser


if __name__ == "__main__":
    # Create parser instance
    parser = tcg_buildrx_parser()

    try:
        argcomplete.autocomplete(parser)
    except NameError:
        pass
    args = parser.parse_args()

    if args.version:
        print('ThermoCodegen {}'.format(tcg.__version__))
        sys.exit(0)

    # clean out any directories with extension .build exit
    if args.clean:
        build_dirs = glob('*.build')
        for build_dir in build_dirs:
            try:
                shutil.rmtree(build_dir)
            except FileNotFoundError as e:
                pass
        sys.exit(0)

    # parse the reaction file and extract a dictionary
    # this will fail if the rxml file does not include a valid thermodynamic database file
    if args.rxfile is not None:
        rx_file = args.rxfile
        print(fetch_database_message(rx_file))
        rx_parser = spudio.Parser(rx_file)
        rx_dict = rx_parser.generate_dict()
    else:
        print('Error: requires a <>.rxml reaction file')
        sys.exit(1)


    # set library name

    if args.libraryname is None:
        libraryname = rx_dict['name']
    else:
        libraryname = args.libraryname

    # create the Build directory and set the database
    build_dir = '{}/{}.build'.format(os.getcwd(), libraryname)



    if not os.path.isdir(build_dir):
        create_build_dir(build_dir, rx_dict)
        print('Created build directory {} and installed database'.
              format(os.path.basename(build_dir)))
    database_name = rx_dict['database'].rstrip().split('/')[-1][:-7]
    database_dir = os.path.abspath(build_dir + '/' + database_name)
    print('Database = {}'.format(rx_dict['database']))
    print('Database Dir={}'.format(database_name))


    # Parse endmembers and phases from database
    efiles, endmembers = get_files_parsers(database_dir, '.emml')
    pfiles, phases = get_files_parsers(database_dir, '.phml')

    # FIXME:  this logic is wonky for installation
    if args.install_dir is None or args.install_dir == 'current':
        install_dir = os.getcwd()
    else:
        # expand and ~ to user directories
        install_dir = os.path.expanduser(args.install_dir)
        if not os.path.isabs(install_dir):
            install_dir = os.path.abspath(install_dir)

    if args.endmembers:
        print_endmembers(endmembers)

    if args.phases:
        print_phases(phases)

    if args.validate:
        validate_phases(rx_dict, phases)
        validate_reactions(rx_dict, phases)

    if args.py_libraryname is not None:
        py_libraryname = args.py_libraryname
    else:
        py_libraryname = "py_" + libraryname

    if args.build:
        configure(build_dir, rx_file, args)
        make(build_dir)

    if args.install_dir is not None:
        configure(build_dir, rx_file, args)
        install(build_dir)

# ThermoCodegen

ThermoCodegen  is part  of the larger [ENKI](http://enki-portal.org/) project for Thermodynamic modeling

It is designed to provide reproducible, code-generation tools for developing and exploring custom thermodynamic models and reaction kinetics libraries for use in a wide range of applications.

At its heart, ThermoCodegen, shares the same code-generation module `coder.py` from [ThermoEngine](https://gitlab.com/ENKI-portal/ThermoEngine) that generates high-performance C code from sympy expressions for the Gibbs Free Energy of custom  endmembers and phases.  Currently ThermoCodegen supports most models supported by ThermoEngine.

ThermoCodegen also adds the following features

* Model generation is described in Jupyter notebooks, but stored in structured xml files that contain all the information required to describe and auto-generate code for thermodynamic databases of endmembers and phases.
* ThermoCodegen also allows code generation of custom kinetic reaction models built on the phase databases.
* Generates C++ libraries for inclusion in other modeling systems such as  [TerraFERMA](https://terraferma.github.io) or [ASPECT](https://aspect.geodynamics.org)
* Generates python bindings of the C++ classes for direct import into python applications
* These python modules can also be dropped back into ThermoEngine as a custom database for use in equilibration or calibration calculations (requires a separate branch of ThermoEngine)

## Documentation

Extensive documentation for ThermoCodegen is available through [gitlab.io/ThermoCodegen](https://enki-portal.gitlab.io/ThermoCodegen) including multiple examples and workflows.   Here we provide a minimal description to get started.

## Quickstart:  docker or singularity containers

The easiest way to get started with ThermoCodegen is to install docker, or singularity and install one of our pre-compiled containers
which also includes a full implementation of TerraFERMA v2.0.  To run properly you should allow X11 access to the container.
For linux machines, we suggest using the Singularity container as it is self-contained and handles the X11 dependencies and local directories transparently.  
Full documentation and pointers to the most recent containers can be found at [ThermoCodegen/quickstart.html](https://enki-portal.gitlab.io/ThermoCodegen/quickstart.html)

## Dependencies and Installation 

Dependencies and installation directions for native MacOS and linux (Ubuntu 20.04) can be found at [ThermoCodegen/install.html](https://enki-portal.gitlab.io/ThermoCodegen/install.html)

### Docker/Singularity

really...just use the containers...it's much easier (and you get TerraFERMA v.2 for free ;^)




# -*- coding: utf-8 -*-
from molmass import Formula

class Endmember(object):
    '''
    Class describing sympy generated potential energies plus derivatives.
    Also handles derivatives with respect active parameters for use in
    calibration code.
    '''

    def __init__(self, endmember_dict, printer):
        self.endmember_dict = endmember_dict
        self.printer = printer
        self.pfuncs = self.potential_functions()

    def get_molecular_weight(self):
        """
        Returns the molecular weight for given a formula
        (uses Christoph Gohlke’s Molmass.py routines).
        """
        f  = Formula(self.endmember_dict['formula'])
        mw = f.mass
        return mw

    def potential_functions(self):
        """
        Calculates sympy generated derivates for G.
        """
        # Extract sympy symbols for state variables (assuming Gibbs this is T and P)
        T = [var['symbol'] for var in self.endmember_dict['model']['variables'] if var['name'] == 'T'][0]
        P = [var['symbol'] for var in self.endmember_dict['model']['variables'] if var['name'] == 'P'][0]

        # Gibbs free energy
        G = self.endmember_dict['potentials'][-1]['expression']

        # Potential and its derivatives
        pfuncs = {}
        pfuncs['G']        = G
        pfuncs['dGdT']     = G.diff(T)
        pfuncs['dGdP']     = G.diff(P)
        pfuncs['d2GdT2']   = G.diff(T, T)
        pfuncs['d2GdTdP']  = G.diff(T, P)
        pfuncs['d2GdP2']   = G.diff(P, P)
        pfuncs['d3GdT3']   = G.diff(T, T, T)
        pfuncs['d3GdT2dP'] = G.diff(T, T, P)
        pfuncs['d3GdTdP2'] = G.diff(T, P, P)
        pfuncs['d3GdP3']   = G.diff(P, P, P)

        return pfuncs

    def endmember_functions(self):
        """
        Returns a dictionary describing standard properties (name, formula, molmass)
        and potentials (plus derivatives). This is substituted directly
        into the endmember C++ template. Currently works only for Gibbs.
        """
        # Code printed derivatives
        self.pfuncs.update({k: self.printer.doprint(v) for k, v in self.pfuncs.items()})

        # Construct dictionary to be substituted into C++ template
        em_funcs = {'endmembername': self.endmember_dict['name'],
                    'endmembername_uppercase': self.endmember_dict['name'].upper(),
                    'model': self.endmember_dict['model']['name'],
                    'filename': self.endmember_dict['filename'],
                    'filename_uppercase': self.endmember_dict['filename'].upper(),
                    'formula': self.endmember_dict['formula'],
                    'molecular_weight': self.get_molecular_weight()}

        # Add potential functions to dictionary
        em_funcs.update(self.pfuncs)

        return em_funcs

    def calibration_functions(self, active_param):
        """
        Returns a dictionary describing derivatives of functions in 'endmember_functions'
        with respect to active parameters.
        """
        p_a_name, p_a_sym = active_param['name'], active_param['symbol']
        cfuncs = {'endmembername': self.endmember_dict['name'],
                  'param': p_a_name}

        for k, d in self.pfuncs.items():
                deriv_str = 'dparam' '_' + k
                cfuncs[deriv_str] = d.diff(p_a_sym)

        # Code printed derivatives
        cfuncs.update({k: self.printer.doprint(v) for k, v in cfuncs.items()})

        return cfuncs

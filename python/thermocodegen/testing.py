# Routines for generating test dataframes from various thermocodgen and thermoengine objects
import sys
import numpy as np
import pandas as pd
import ast
import inspect
# some useful routines pulled from Stack overflow
from collections.abc import Sequence
from itertools import chain, count
from datetime import datetime




def get_methods(_class):
    """ return a list of strings of all non-magic methods in a class """
    return [m for m in dir(_class) if not m.startswith('__')]


def get_args(func):
    """ return a list of strings of variable names from pybind11 bound
    python functions for each interface in a function (allows overloaded
        functions)
    """
    doc = inspect.getdoc(func)
    # check for overloaded functions
    # FIXME: this breaks with mixed coder interfaces
    try:
        interfaces = doc.split('self:')[1:]
    except ValueError:
        interfaces = []
        pass
    args = []
    for interface in interfaces:
        # just strip out the first line as the signature
        signature= interface.split('\n')[0]
        arglist = signature.split(':')[:-1]
        args.append([s.split(', ')[-1] for s in arglist])
    return args


def get_phases(db):
    """ returns a list of phase names from a thermodynamic database """

    methods = get_methods(db)
    phases = []
    for m in methods:
        obj = db.__dict__[m]()
        if 'endmembers' in get_methods(obj):
            phases.append(m)

    return phases


def get_endmembers(db):
    """ returns a list of endmember names from a thermodynamic database """

    methods = get_methods(db)
    endmembers = []
    for m in methods:
        obj = db.__dict__[m]
        if 'molecular_weight' in get_methods(obj):
            endmembers.append(m)

    return endmembers


def generate_df_endmembers(db, df_in):
    """
    generate a pandas dataframe of test values for all the endmembers
    in a thermodynamic database db
    :param db: thermocodegen  database
    :param df_in: pandas dataframe of input values
    :return: df_out:  pandas dataframe of endmember results

    Usage:  df_out = generate_df_endmembers(db, df_in_
    """

    endmembers = get_endmembers(db)

    # initialize output data frame
    dfs = []
    for endmember in endmembers:
        em = db.__dict__[endmember]()
        dfs.append(set_data(em, df_in))

    df_out = pd.concat(dfs)
    df_out.set_index('name')

    return df_out


def generate_df_phases(db, df_in):
    """
    generate a pandas dataframe of test values for all phases in
    a thermodynamic database db.

    :param db: thermocodegen (pybind11) database
    :param df_in: pandas dataframe of input values for each phase_name
    :return: df_out: pandas dataframe of phase values

    usage:
        df_out = generate_df_phases(db,df_in)
    """

    # initialize output data frame

    dfs = []
    for row in range(len(df_in)):
        phase = df_in['name'].iloc[row]
        obj = db.__dict__[phase]()
        # caution, note the use of [ []] in next line to extra row as df
        dfs.append(set_data(obj, df_in.iloc[[row]]))

    df_out = pd.concat(dfs)
    df_out.set_index('name')

    return df_out


def depth(seq):
    seq = iter(seq)
    try:
        for level in count():
            seq = chain([next(seq)], seq)
            seq = chain.from_iterable(s for s in seq if isinstance(s, Sequence)
                                      and not isinstance(s, str))
    except StopIteration:
        return level


# https://stackoverflow.com/questions/2158395/flatten-an-irregular-list-of-lists?page=1&tab=votes#tab-top
def flatten(l):
    for el in l:
        if isinstance(el, Sequence) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el


def is_float_list(l):
    """ check if a (potentially nested) list is all floats """

    if isinstance(l, list):
        return all(isinstance(x, (float, np.float64)) for x in list(flatten(l)))
    else:
        return False


def convert_tonumpy(df):
    """ converts columns of a dataframe  that are nested lists of floats to numpy arrays"""
    for c in df.columns:
        if isinstance(df[c][0], str):
            try:
                df[c] = df[c].apply(lambda x: np.asarray(ast.literal_eval(x)))
            except RuntimeError:
                print('cant convert {}'.format(c))
                pass
    return df


def allclose_float_list(test, answer, rtol=1.e-5, atol=1.e-8):
    """ compare lists of floats, potentially nested using np.allclose on the
    individual float lists, and return true if all lists are close, else return flase

    test: potentially nested list of floats to test
    answer: potentially nested list of floats in answer
    rtol: optional relative tolerance to pass to np.allclose
    atol: optional absolute tolerance to pass to np.allclose
    """

    if is_float_list(test) and is_float_list(answer):
        t = list(flatten(test))
        a = list(flatten(answer))

        return np.allclose(t, a, rtol=rtol, atol=atol)


def _isDB(db):
    """
    some sanity checks to determine if a database object is pybind11 based
    """
    # FIXME: this is fragile as it only tests the first object and assumes it is an endmember or phase
    members = inspect.getmembers(db)
    m0 = inspect.getmembers(members[0][1])
    methods = [m[0] for m in m0]
    db_test_methods = ['g', 'cp', 'v', 'formula']
    rxn_test_methods = ['A', 'X_to_C', 'C_to_X', 'report']

    is_phase = set(db_test_methods).issubset(set(methods))
    is_rxn = set(rxn_test_methods).issubset(set(methods))
    return is_phase or is_rxn


def _isModelDB(db):
    """  some sanity checks to determine if a database object is a thermoengine database
    """
    members = inspect.getmembers(db)
    methods = [m[0] for m in members]
    test_methods = ['calib', 'database', 'get_phase', 'phase_info']
    return all([t in methods for t in test_methods])

class Tester(object):
    """
    Generic Base class for a database Tester object

    All testers take in a database of endmembers and phases (or pure phases and solution phases) and
    generate data_frames and pytest files for regression testing of all basic phase attributes
    """

    def __init__(self, db):
        self.db = db
        self.get_methods = get_methods
        self._db_methods = self.get_methods(db)
        self._endmembers = []
        self._phase_dict = []
        self._rxns = []

    @staticmethod
    def factory(db):
        """
        Tester.factory(db)  returns a Tester object depending on the type of database object passed

        :param db:
        :type db: either a thermocodegen pybind11 object or :class:`thermoengine.model.Database`
        :return: either :class:`thermocodegen.testing.DBtester` for thermocodegen databases or
            :class:`thermocodegen.testing.ModelDBTester` for thermoengine databases

        Usage:
            for a ThermoCodegen object

            .. code-block:: python

                import my_pydb
                tester = Tester.factory(my_pydb)

            for a ThermoEngine model database

            .. code-block:: python

                from thermoengine import model
                modelDB = model.Database()
                tester = Tester.factory(modelDB)
        """

        if _isDB(db):
            return DBTester(db)
        elif _isModelDB(db):
            return ModelDBTester(db)

class DBTester(Tester):
    """
    Subclass for testing thermodynamic databases created by thermocodegen and pybind11

    :param db:
    :type db: thermocodegen pybind11 object

    """

    def __init__(self, db):
        # FIXME: should have a better test to see if this is a kosher pybind11 generated thermo db
        #assert(_isDB(db))
        super().__init__(db)
        try:
            self._endmembers = self._get_endmembers()
        except:
            raise Warning("no endmembers")
        try:
            self._phase_dict = self._get_phase_dict()
        except:
            raise Warning('no phases')
        try:
            self._rxns = self._get_reactions()
        except:
            pass

    def _get_endmembers(self):
        """ returns a list of endmember names from a thermodynamic database """

        methods = self.get_methods(self.db)
        endmembers = []
        for m in methods:
            obj = self.db.__dict__[m]()
            if 'mw' in get_methods(obj):
                endmembers.append(m)

        return endmembers

    def _get_reactions(self):
        """ returns a list of reaction object names from a reaction db (if it exists)"""

        methods = self.get_methods(self.db)
        rxns = []
        for m in methods:
            obj = self.db.__dict__[m]()
            if 'X_to_C' in get_methods(obj):
                rxns.append(m)

        return rxns

    def _get_phase_dict(self):
        """ return a dictionary of phase:endmembers names for each phase"""
        methods = self.get_methods(self.db)
        phase_dict = {}
        for m in methods:
            if m not in self.endmembers:
                try:
                    ph = self.db.__dict__[m]()
                    endmembers = ph.endmembers()
                    phase_dict.update({ph.name(): [em.name() for em in endmembers]})
                except:
                    pass

        return phase_dict

    @property
    def endmembers(self):
        """
        endmember names in database

        :return: list of endmember names
        :rtype: list of strings
        """
        return self._endmembers

    @property
    def phase_dict(self):
        """
        phase names, with corresponding endmember names in database

        :return: dictionary of {phase_name:[ endmembers]}
        :rtype: dict
        """
        return self._phase_dict

    @property
    def rxns(self):
        """
        reaction names in database

        :return: list of reaction names
        :rtype: list of strings
        """
        return self._rxns


    def _get_args(self, func):
        """ return a list of strings of variable names from pybind11 bound
        python functions for each interface in a function (allows overloaded
            functions)
        """
        doc = inspect.getdoc(func)
        # check for overloaded functions
        # FIXME: this breaks with mixed coder interfaces
        try:
            interfaces = doc.split('self:')[1:]
        except:
            interfaces = []
            pass
        args = []
        for interface in interfaces:
            # just strip out the first line as the signature
            signature= interface.split('\n')[0]
            arglist = signature.split(':')[:-1]
            args.append([s.split(', ')[-1] for s in arglist])
        return args

    def _get_unique_args(self, obj):
        """ returns unique list of arguments for a class (endmember or phase)

        :param obj: input object (and endmember or phase class)
        :return:
            arg_list:  sorted list of unique arguments for the object
        """
        # all objects should include Temperature
        arg_set = {'T'}
        methods = self.get_methods(obj)
        for method in methods:
            try:
                func = getattr(obj,method)
            except ValueError:
                continue
            if inspect.ismethod(func):
                args = self._get_args(func)[0]
                arg_set.update(args)

        arg_list = list(arg_set)
        return sorted(arg_list)

    def _get_endmember_args(self):
        """ returns unique set of arguments for endmember functions"""
        if len(self.endmembers) > 0:
            name = self.endmembers[0]
            em = self.db.__dict__[name]()
            return self._get_unique_args(em)

    def _get_phase_args(self):
        """ returns unique set of arguments for phase functions"""
        if len(self._phase_dict) > 0:
            name = list(self.phase_dict.keys())[0]
            ph = self.db.__dict__[name]()
            return self._get_unique_args(ph)

    def _get_reaction_args(self):
        """ returns unique set of arguments for reaction functions"""
        if len(self.rxns) > 0:
            name = self.rxns[0]
            rxn = self.db.__dict__[name]()
            return self._get_unique_args(rxn)

    def get_endmember_df_in(self):
        """

        :return: empty dataframe with columns for input data for each endmember
        :rtype: :class:`pandas.DataFrame`
        """
        endmembers = self.endmembers
        endmember_args = self._get_endmember_args()
        df_in = pd.DataFrame(columns=['name']+endmember_args)
        df_in['name']= endmembers
        return df_in

    def get_phase_df_in(self):
        """

        :return: empty dataframe with columns for input data for each phase
        :rtype: :class:`pandas.DataFrame`
        """
        phases = list(self._phase_dict.keys())
        phase_args = self._get_phase_args()
        df_in = pd.DataFrame(columns=['name','K_endmembers']+phase_args)
        df_in['name'] = phases
        df_in['K_endmembers'] = np.array([len(em) for em in self._phase_dict.values() ])
        return df_in

    def get_reaction_df_in(self):
        """

        :return: empty dataframe with columns for input data for each reaction object
        :rtype: :class:`pandas.DataFrame`
        """
        rxns = self.rxns
        rxn_args = self._get_reaction_args()
        df_in = pd.DataFrame(columns=['name']+rxn_args)
        df_in['name']= rxns
        return df_in

    def _set_data(self, row):
        """ create a single row of a pandas dataframe with ouput from all the object members
        with inputs from a single row of an input dataframe containing the object name
        """

        # start building output dataframe by copying
        df_out = row.copy()

        # construct object from the database
        name = row['name']
        obj = self.db.__dict__[name]()
        methods = get_methods(obj)
        methods_ignore = ['endmembers', 'phases', 'identifier',
                          'tcg_build_version', 'tcg_build_git_sha',
                          'tcg_generation_version', 'tcg_generation_git_sha' ]
        for m in methods_ignore:
            try:
                methods.remove(m)
            except ValueError:
                pass
        for m in methods:
            func = getattr(obj,m)
            # FIXME:: right now just returns argument for first interface
            args = self._get_args(func)[0]
            # if no arguments just evaluate the function
            if len(args) == 0:
                #vals = '()'
                answer = func()
            else:
                try:
                    vals = tuple(row[args].values)
                except KeyError:
                    vals = None
                    answer = None
                if vals:
                    try:
                        answer = func(*vals)
                    except TypeError:
                        answer = None
            #FIXME: I believe this sets off a pandas warning if m doesn't exist, need to use reindex
            df_out[m] = answer
        return df_out.to_frame().T

    def set_df_out(self,df_in):
        """
        writes a new pandas dateframe with  return values given a small dataframe of input values

        :param df_in: pandas dataframe with input values for each object in the database
        :type df_in: :class:`pandas.DataFrame`
        :return: pandas dataframe with both input and output values for each object in the database
        :rtype: :class:`pandas.DataFrame`
        """
        df_out = pd.DataFrame()
        for index,row in df_in.iterrows():
            out = self._set_data(row)
            #FIXME: might need to worry about concating initially mismatched width dataframes
            df_out = pd.concat((df_out,out),sort=False)

        return df_out

    def write_tests(self, df, fileroot=None, rtol=1.e-5, atol=1.e-8):
        """
        write a  test file for use by pytest that compares the behavior of the current database against solutions
        stored in a pandas dataframe

        :param df: Dataframe containing input parameters and answers for a range of interfaces and functions in the database
        :type df: :class:`pandas.DataFrame`
        :param fileroot: rootname of output file. Defaults to None.
            If None will set a generic name with the current timestamp and name of the database
        :type fileroot: str, optional
        :param rtol: relative tolerance for floating point tests (defaults to 1.e-5)
        :type rtol: float, optional
        :param atol: absolute tolerance for floating point tests (defaults to 1.e-8)
        :type atol: float,optional
        :return: outputs a pytest file named fileroot_<time_stamp>.py file
        """

        db_name = (self.db).__name__
        now = datetime.now().strftime("%d-%b-%Y_%H:%M:%S")

        if fileroot is None:
            fileroot = 'test_{}'.format(db_name)
        file = '{}_{}.py'.format(fileroot, now)

        with open(file, 'w') as f:
            f.write('import numpy as np\nfrom numpy import array\n')
            f.write('from thermocodegen.testing import is_float_list, allclose_float_list\n')
            f.write('import {} as db\n'.format(db_name))
            f.write('import pytest')

            # loop over rows in the dataframe and create a test class for each row
            for index, row in df.iterrows():
                name = row['name']
                obj = self.db.__dict__[name]()
                print("Writing test for {}".format(name))
                f.write('\nclass Test{}:\n'.format(name))
                f.write('    phase = db.{}()\n\n'.format(name))
                methods = list(row.keys())
                for m in methods:
                    try:
                        func = getattr(obj,m)
                    except (ValueError, AttributeError) as e:
                        continue
                    # FIXME: just set test for first interface in overloaded function
                    args = self._get_args(func)[0]
                    # if no arguments just set vals to '()'
                    if len(args) == 0:
                        vals = '()'
                    else:
                        # check if args is subset of row keys
                        if set(args).issubset(set(row.keys())):
                            vals = tuple(row[args].values)
                        else:
                            continue
                    f.write('    def test_{}(self):\n'.format(m))
                    # extract function and arguments
                    test = 'self.phase.{}{}'.format(m, vals)
                    f.write('        test = {}\n'.format(test))
                    answer = row[m]
                    self._write_answer(f, answer, rtol, atol)

    def _write_answer(self, f, answer, rtol, atol):
        np.set_printoptions(threshold=sys.maxsize)
        if is_float_list(answer):
            f.write('        answer ={}\n'.format(answer))
            f.write('        assert(allclose_float_list(test,answer,rtol={},atol={}))\n\n'.format(rtol, atol))
        elif isinstance(answer, np.ndarray):
            f.write('        answer ={}\n'.format(repr(answer)))
            f.write('        assert(np.allclose(test,answer,rtol={},atol={}))\n\n'.format(rtol, atol))
        elif isinstance(answer, str):
            f.write('        answer = \'{}\'\n'.format(answer))
            f.write('        assert(test == answer)\n\n')
        elif isinstance(answer, (float, np.float64)):
            f.write('        answer = {}\n'.format(answer))
            f.write('        assert(np.isclose(test,answer,rtol={},atol={}))\n\n'.format(rtol, atol))
        else:
            f.write('        answer = {}\n'.format(answer))
            f.write('        assert(test == answer)\n\n')


class ModelDBTester(DBTester):
    """
    Subclass for testing thermodynamic database interfaces created by :class:`thermoengine.model.Database`

    :param modelDB: a thermodynamic database object from ThermoEngine
    :type modelDB:  :class:`thermoengine.model.Database`
    """

    def __init__(self, modelDB):
        # FIXME: should have a better test to see if this is a kosher thermoengine database
        #assert _isModelDB(modelDB)
        super().__init__(modelDB)

    def _get_endmembers(self):
        """ returns a list of endmember names from a thermodynamic database """

        self._phase_info = self.db.phase_info
        db_pure = self._phase_info[self._phase_info['phase_type'].str.match('pure')]

        ###FIXME:  need to rethink how to deal with water endmember
        # Don't include 'H2O' endmember
        #endmembers = [em for em in list(db_pure['abbrev']) if em != 'H2O']

        endmembers = [em for em in list(db_pure['abbrev']) ]
        return endmembers

    def _get_phase_dict(self):
        """ return a dictionary of phase:endmembers names for each phase"""
        db_solution = self._phase_info[self._phase_info['phase_type'].str.match('solution')]

        ###FIXME:  should inclued Liquid phase if it's present
        # Don't include 'Liq' phase
        #db_solution = db_solution[db_solution['abbrev'] != 'Liq']

        phases = list(db_solution['abbrev'])
        phase_dict = {phase:list(self.db.get_phase(phase).endmember_names) for phase in phases}

        return phase_dict

    @property
    def endmembers(self):
        return self._endmembers

    @property
    def phase_dict(self):
        return self._phase_dict

    def _get_args(self, func):
        """ return a list of strings of variable names from thermoengine phase functions
        FIXME: at the moment this conflates all optional as well as required parameters
        """
        sig = inspect.signature(func)
        args = [ list(sig.parameters.keys()) ]
        return args

    def _get_endmember_args(self):
        """ returns unique set of arguments for endmember functions"""
        if len(self.endmembers) > 0:
            name = self.endmembers[0]
            em = self.db.get_phase(name)
            return self._get_unique_args(em)

    def _get_phase_args(self):
        """ returns unique set of arguments for phase functions"""
        if len(self._phase_dict) > 0:
            name = list(self.phase_dict.keys())[0]
            ph = self.db.get_phase(name)
            return self._get_unique_args(ph)

    def _set_data(self, row):
        """ create a single row of a pandas dataframe with output from all the object members
        with inputs from a single row of an input dataframe containing the object name
        """

        # start building output dataframe by copying
        df_out = row.copy()

        # construct object from the database
        obj = self.db.get_phase(row['name'])
        methods = get_methods(obj)
        # screen out some methods a priori
        # huge issue, need to turn off enable/disable_gibbs_energy_reference_state as
        # it buggers up G and H for all phases
        methods_ignore = ['Berman_formula','_nudge_solution_comp', 'validate',
                          'enable_gibbs_energy_reference_state','disable_gibbs_energy_reference_state']
        for m in methods_ignore:
            try:
                methods.remove(m)
            except ValueError:
                pass
        for m in methods:
            try:
                func = getattr(obj, m)
            except (ValueError, AttributeError) as e:
                continue
            if inspect.ismethod(func):
                # FIXME:: right now just returns argument for first interface
                args = self._get_args(func)[0]
                # if no arguments just set vals to '()'
                if len(args) == 0:
                    answer = func()
                else:
                    # check if args is subset of row keys
                    if set(args).issubset(set(row.keys())):
                        vals = tuple(row[args].values)
                    else:
                        continue
                    try:
                        answer = func(*vals)
                    except (TypeError, AttributeError) as e:
                        continue
                df_out[m] = answer
        return df_out.to_frame().T

    def write_tests(self, df, fileroot=None, rtol = 1.e-5, atol = 1.e-8):
        """
        write a  test file for use by pytest that compares the behavior of the current database against solutions
        stored in a pandas dataframe

        :param df: Dataframe containing input parameters and answers for a range of interfaces and functions in the database
        :type df: :class:`pandas.DataFrame`
        :param fileroot: rootname of output file. Defaults to None.
                    If None will set a generic name with the current timestamp and name of the db
        :type fileroot: str, optional
        :param rtol: relative tolerance for floating point tests (defaults to 1.e-5)
        :type rtol: float, optional
        :param atol: absolute tolerance for floating point tests (defaults to 1.e-8)
        :type atol: float,optional
        :return: None...outputs a pytest file named fileroot_<time_stamp>.py file
        """

        db_name = (self.db).database
        now = datetime.now().strftime("%d-%b-%Y_%H:%M:%S")

        if fileroot is None:
            fileroot = 'test_modelDB-{}'.format(db_name)
        file = '{}_{}.py'.format(fileroot,now)
        with open(file, 'w') as f:
            f.write('import numpy as np\nfrom numpy import array\n')
            f.write('from thermocodegen.testing import is_float_list, allclose_float_list\n')
            f.write('from thermoengine import model\n')
            if db_name == 'thermocodegen':
                module_name = self.db._thermocodegen_obj.__name__
                f.write('import {}\n'.format(module_name))
                f.write('db = model.Database.from_thermocodegen'
                        + '({})\n'.format(module_name))
            else:
                f.write('db = model.Database(database="{}")\n'.format(db_name))
            f.write('import pytest')

            # loop over rows in the dataframe and create a test class for each row
            for index, row in df.iterrows():
                name = row['name']
                phase = self.db.get_phase(name)
                print("Writing test for {}".format(name))
                f.write('\nclass Test{}:\n'.format(name))
                f.write('    phase = db.get_phase("{}")\n\n'.format(name))
                methods = list(row.keys())
                for m in methods:
                    try:
                        func = getattr(phase, m)
                    except (ValueError, AttributeError) as e:
                        continue
                    if inspect.ismethod(func):
                        args = self._get_args(func)[0]
                        # if no arguments just set vals to '()'
                        if len(args) == 0:
                            vals = '()'
                        else:
                            # check if args is subset of row keys
                            if set(args).issubset(set(row.keys())):
                                vals = tuple(row[args].values)
                            else:
                                continue
                        f.write('    def test_{}(self):\n'.format(m))
                        # extract function and arguments
                        test = 'self.phase.{}{}'.format(m, vals)
                        f.write('        test = {}\n'.format(test))
                        answer = row[m]
                        self._write_answer(f,answer,rtol,atol)



if __name__ == "__main__":
    """
    Internal  code for debugging within PyCharm
    """
    import sys
    import numpy.random as rand
    import time

    sys.path.append('../../examples/codegen/fo_fa_coder_db/database/fo_fa_coder_db/lib/python3.7/site-packages')
    import py_fo_fa_coder_db as db

    print(type(db))
    tester = Tester.factory(db)
    print('Endmembers: ', tester.endmembers)
    print('Phases: ', tester.phase_dict)

    print('Endmember_arguments', tester._get_endmember_args())
    print('Phase arguments', tester._get_phase_args())
    print()
    endmember_df_in = tester.get_endmember_df_in()
    print('Endmember_df_in:', endmember_df_in)

    # subset the input dataframes and fill with values
    n = len(tester.endmembers)
    endmember_df_in = endmember_df_in[['name','T','P']]
    endmember_df_in['T']=1700.*np.ones(n)
    endmember_df_in['P']=1000.*np.ones(n)
    df_tmp = endmember_df_in.copy()
    df_tmp['T']=1500.*np.ones(n)
    endmember_df_in = pd.concat((endmember_df_in,df_tmp),ignore_index=True).sort_values(by=['name','T'])
    endmember_df_in.reset_index(drop=True,inplace=True)
    print(endmember_df_in)


    endmember_df_out = tester.set_df_out(endmember_df_in)
    print(endmember_df_out)

    tester.write_tests(endmember_df_out)

    # repeat for phases

    phase_df_in = tester.get_phase_df_in()
    print('Phase_df_in:', phase_df_in)
    phase_df_in = phase_df_in[['name','K_endmembers','T','P','n','x']]
    print('Phase_df_in:', phase_df_in)
    n_phases = len(phase_df_in)
    phase_df_in['T'] = 1700.*np.ones(n_phases)
    phase_df_in['P'] = 1000.*np.ones(n_phases)
    phase_df_in['n'] = phase_df_in['K_endmembers'].apply(lambda x : rand.uniform(0.,1.,x)).apply(lambda x: list(x/np.sum(x)))
    phase_df_in['x'] = phase_df_in['n']
    print('Phase_df_in:', phase_df_in)
    phase_df_out = tester.set_df_out(phase_df_in)
    phase_df_in['c'] = phase_df_out['x_to_c']
    phase_df_out = tester.set_df_out(phase_df_in)
    print('Phase_df_out:', phase_df_out)

    time.sleep(1)
    tester.write_tests(phase_df_out)

    # now start building tester for ThermoEngine modelDB's
    from thermoengine import model

    modelDB = model.Database()
    tetester = Tester.factory(modelDB)
    print('Endmembers: ', tetester.endmembers)
    print('Phases: ', tetester.phase_dict)
    print()
    print('Endmember_arguments', tetester._get_endmember_args())
    print('Phase arguments', tetester._get_phase_args())
    print()
    endmember_df_in = tetester.get_endmember_df_in()
    print('Endmember_df_in:', endmember_df_in)

    # # subset the input dataframes and fill with values
    n = len(tetester.endmembers)
    endmember_df_in = endmember_df_in[['name','T','P']]
    endmember_df_in['T']=1700.*np.ones(n)
    endmember_df_in['P']=1000.*np.ones(n)
    endmember_df_in['mol'] = np.ones(n)
    endmember_df_in['V'] = [ None for i in range(n)]
    print(endmember_df_in)
    endmember_df_out = tetester.set_df_out(endmember_df_in)
    endmember_df_out.to_json('endmember_df_out.json')
    print(endmember_df_out)

    tetester.write_tests(endmember_df_out)

    # repeat for phases
    phase_df_in = tetester.get_phase_df_in()
    print('Phase_df_in:', phase_df_in)
    phase_df_in = phase_df_in[['name','K_endmembers','T','P','mol','V']]
    print('Phase_df_in:', phase_df_in)
    n_phases = len(phase_df_in)
    phase_df_in['T'] = 1700.*np.ones(n_phases)
    phase_df_in['P'] = 1000.*np.ones(n_phases)
    phase_df_in['mol'] = phase_df_in['K_endmembers'].apply(lambda x : rand.uniform(0.,1.,x)).apply(lambda x: np.array(x)/np.sum(x))
    phase_df_in['V'] = [None for i in range(n_phases)]
    print('Phase_df_in:', phase_df_in)
    phase_df_out = tetester.set_df_out(phase_df_in)
    print('Phase_df_out:', phase_df_out)

    phase_df_out.to_json('model_DB_solution_phases.json')
    time.sleep(1)
    tetester.write_tests(phase_df_out)

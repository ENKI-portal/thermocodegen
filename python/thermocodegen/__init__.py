""" file:thermocodegen/__init__.py
    author: Marc Spiegelman
    description: Python package thermocodegen module
"""
# Load all  submodules and place in thermocodegen namespace
from . import codegen
from . import coder
from . import spudio
from .version import version as __version__
try:
  from .git_sha import git_sha as __git_sha__
except ImportError:
  __git_sha__ = "git_sha_not_set"

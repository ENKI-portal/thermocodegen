#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
   reaction:

    collection of python routines for parsing Spud .kxml files
    to (and eventually from) nested python dictionaries for use in general
    code-generation schemes
"""

# import libspud for reading SPuD xml files
import libspud

import molmass as mm
from . import optionparsers, parser

import os
import sys
import shutil
import requests
import tarfile
import tempfile
import sympy as sym
import textwrap

# utilities for extracting and validating database urls

def parse_database_name(file):
    """
    reads a SPud derived reaction description file and
    extracts the database name.

    Parameters
    ----------
    file: str
        rxml reaction file

    Returns
    -------
    database_url: str

    """
    if file is not None:
        libspud.load_options(file)
    database_url = libspud.get_option('/database')
    if file is not None:
        libspud.clear_options()
    return database_url

def _isremoteurl(url):
    """
    checks database url to see if it is remote (starts with http or https)
    """
    isremote = False
    if  url.startswith('http://') or url.startswith('https://'):
        isremote = True
    return isremote

def fetch_database_message(rxfilename):
    """
    utility program to check database name in rxml file and print simple message for fetching remote databases
    Parameters
    ----------
    rxfilename: str
        rxml file name

    Returns
    -------

    message: str
        string describing type of database (remote) or local

    """

    url = parse_database_name(rxfilename)
    if _isremoteurl(url):
        message = 'Fetching remote database: {}'.format(url)
    else:
        message = 'Using local database: {}'.format(url)

    return message

def validate_database_url(url):
    """
    checks database url to see if it exists and returns fully qualified path for local files

    Parameters
    ----------
    url: str
        url string as recorded in the rxml file

    Returns
    -------
    database_url: str
        database_url with absolute path

    """
    if _isremoteurl(url):
        params = {}
        if "dropbox" in url:
            params = {'dl': 1}
        try:
            req = requests.get(url, params=params)
            req.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
            raise TypeError('Remote database file not found:\n {}'.format(url))
        database_url = url
    else:
        database_url = os.path.abspath(url.replace('file://', ''))
        if not os.path.isfile(database_url):
            raise TypeError('Local database file not found:\n {} '.format(database_url))
        if url.startswith('file://'):
            database_url = 'file://'+database_url

    return database_url


def copy_rxml_file(file,dest_dir):
    """
    copies a rxml file to destination directory, making sure the copied file has a fully
    qualified database url (i.e. resolves relative paths to absolute paths)
    Parameters
    ----------
    file: str
        name of rxml file
    dest_dir: str
        destination directory for copy

    """
    assert(os.path.exists(dest_dir))
    assert(os.path.isfile(file))

    url = parse_database_name(file)
    full_url = validate_database_url(url)

    libspud.load_options(file)
    libspud.set_option('/database',full_url)

    dest_file = os.path.join(os.path.abspath(dest_dir),os.path.basename(file))
    libspud.write_options(dest_file)
    libspud.clear_options()


def parse_phase_names():
    """ reads a SPuD derive rxn description file and returns a list of
    phase  names
    """
    N_phases = libspud.option_count('/phase')
    phases = []
    for n in range(N_phases):
        name = libspud.get_option('/phase[{}]/name'.format(n))
        phases.append(name)
    return phases


def parse_reaction_rate_model(dict):
    """ parse the reaction rate model part of the spud file and
    return a dictionary including name, lists of model variables and Parameters

    input:
        dict: current reaction dictionary
    """

    # Local and global dicts
    local_dict = dict['local_dict']
    global_dict = dict['global_dict']

    # Nested dictionary for model
    model = {}
    model['name'] = libspud.get_option('reaction_rate/name')

    # Collect list of variables
    str = 'reaction_rate/variable'
    nvar = libspud.option_count(str)
    variables = []
    for i in range(nvar):
        variables.append(optionparsers.parse_variable(
                         str+'[{}]'.format(i),
                         dict,
                         symbolic=True))

    model['variables'] = variables

    # Collect list of functions
    tmp_dict = dict.copy()
    tmp_dict['model'] = model
    str = 'reaction_rate/function'
    nfunc = libspud.option_count(str)
    funcs = []
    for i in range(nfunc):
        funcs.append(optionparsers.parse_function(str+'[{}]'.format(i),
                                                  tmp_dict,
                                                  global_dict,
                                                  symbolic=True))

    model['functions'] = funcs

    return model


def parse_reaction_rate(path, dict, global_dict):
    """ It assumed that all parameters appear symbolically
    (not substituted for values).
    """
    rate = {}
    rate['name'] = libspud.get_option(path+'/name')
    rate['symbol'] = sym.Symbol(rate['name'])

    # sequentially evaluate explicit functions with substitution and build up
    # expression
    n_func = libspud.option_count(path+'/function')
    explicit_functions = []
    functions = []
    for i in range(n_func):
        str = path+'/function[{}]'.format(i)
        function = optionparsers.parse_function(str, dict, global_dict,
                                                symbolic=True)
        print('\tProcessed {} local function {}'.format(function['type'],
                                                        function['name']))
        if function['type'] == 'explicit':
            # add the explicit function symbol to the subs dictionary
            global_dict.update({function['name']: function['expression']})
            explicit_functions.append(function)
        else:
            functions.append(function)

    rate['functions'] = functions
    rate['explicit_functions'] = explicit_functions

    # evaluate the potential expression
    exp_str = libspud.get_option(path+'/expression')
    # print('\tProcessing Rate...',end='')
    local_dict = {}
    exec(exp_str, global_dict, local_dict)

    # Cast as sympy expression if rate is just a constant (float or int)
    _rate = local_dict[rate['name']]
    if isinstance(_rate, float) or isinstance(_rate, int):
        local_dict[rate['name']] = sym.sympify(_rate)

    expression = local_dict[rate['name']].subs(dict['subs_dict'])

    # try simplifying and check for None
    exp_simp = expression.simplify()
    if exp_simp is None:
        exp_simp = expression
    rate['expression'] = exp_simp

    # update global dictionary
    global_dict.update({rate['name']: rate['expression']})
    return rate


def parse_reactions(file, db_dict):
    """ reads reactants, and products  for a reaction description
    starting at path and  returns a reaction dictionary

    Input:
        path  Spud option path to start of reaction option file
        db_dict:  dictionary of parsers from underlying thermo database

        Tries validating reaction pairs as they are read

    returns reaction dictionary
    """

    # set up some validation dictionaries from db_dict
    # legal phase names from the database
    phase_set = set(db_dict['phases'].keys())

    # dictionary of phase and endmember pair
    p_dict = {p.name: p.endmembers for p in db_dict['phases'].values()}

    # clear options for safety
    libspud.clear_options()
    libspud.load_options(file)

    J_reactions = libspud.option_count('reaction')
    reactions = []
    for j in range(J_reactions):
        reaction = {}
        path = 'reaction[{}]'.format(j)
        reaction['name'] = libspud.get_option(path+"/name")

        # get reactants
        N = libspud.option_count(path+'/reactant')
        reactants = []
        for i in range(N):
            reactant_path = path + "/reactant[{}]/".format(i)
            phase = libspud.get_option(reactant_path + "phase")
            endmember = libspud.get_option(reactant_path + "endmember")
            if phase not in phase_set or endmember not in p_dict[phase]:
                print('\n  Error in reaction:{} Bad reactant: ({}, {})'.
                      format(reaction['name'], phase, endmember))
                reactants.append((None, None, None))
            else:
                if endmember == 'SWIM_water':
                    formula = 'H(2)O(1)'
                else:
                    formula = db_dict['endmembers'][endmember].formula
                reactants.append((formula, phase, endmember))
        reaction['reactants'] = reactants

        N = libspud.option_count(path+'/product')
        products = []
        for i in range(N):
            product_path = path + "/product[{}]/".format(i)
            phase = libspud.get_option(product_path + "phase")
            endmember = libspud.get_option(product_path + "endmember")
            if phase not in phase_set or endmember not in p_dict[phase]:
                print('\n  Error in reaction:{} Bad product: ({}, {})'.
                      format(reaction['name'], phase, endmember))
                products.append((None, None, None))
            else:
                if endmember == 'SWIM_water':
                    formula = 'H(2)O(1)'
                else:
                    formula = db_dict['endmembers'][endmember].formula
                products.append((formula, phase, endmember))
        reaction['products'] = products

        reactions.append(reaction)

    return reactions


# utilities for interacting with databases of endmembers and phases
def get_files_parsers(topdir, ext):
    """ returns a list of spud parsers in a nested directory starting at topdir
        that match the extension ext

        Usage: endmembers = get_parsers(os.getcwd(),'.emml')

        should return an empty list if no matching files

    """
    parsers = []
    filenames = []
    for dirname, dirnames, files in os.walk(topdir, topdown=True):
        builddirs = [d for d in dirnames if d.endswith('.build')]
        for d in builddirs:
            dirnames.remove(d)
        for file in files:
            if file.endswith(ext):
                ffull = dirname+'/'+file
                filenames.append(ffull)
                parsers.append(parser.Parser(ffull))

    return filenames, parsers


def fetch_database(url, dest_dir):
    """
    locates and validates a compressed thermocodegen database.tar.gz file and writes it to the destination
    directory dest_dir

    Parameters
    ----------
    url, str
        string containing path of local file or fully qualified URL

        Currently supports:
            local files (including relative paths):    ../database/db.tar.gz
            local files with file:// url descriptor:    file://path-to-file/db.tar.gz
            remote files:
                zenodo:       https://zenodo.org/record/2537988/files/fo_fa.tar.gz
                gitlab:       https://gitlab.com/mspieg/ThermoCodegen/-/blob/master/examples/Systems/fo_fa/database/fo_fa.tar.gz'
                github raw:   http://github.com/user/repo/raw/branch/path/db.tar.gz
                dropbox:      http://www.dropbox.com/blah/db.tar.gz

    dest_dir, path
        directory for unpacking

    Returns
    -------
    db_path: str
        path to unpacked tar file

    Raises
    ______
    TypeError:
        if path to database file doesn't exist

    """

    assert(os.path.exists(dest_dir))
    full_url = validate_database_url(url)

    # try to fetch remote url using http
    if _isremoteurl(full_url):
        filename = full_url.strip().split('/')[-1]
        params = {}
        if "dropbox" in full_url:
            params = {'dl': 1}
        req = requests.get(full_url,params=params)
        req.raise_for_status()
        db_path = dest_dir+'/'+filename
        with open(db_path, 'wb') as db:
            db.write(req.content)
    else:
        path = full_url.replace('file://', '')
        filename = os.path.basename(path)
        shutil.copy(path, dest_dir)
        db_path = dest_dir + '/' + filename

    return os.path.abspath(db_path)


def get_parsers_database(file):
    """ downloads a temporary database tarball from rxml file and a dict
    containing lists of  endmember and phase parsers.
    """
    database_url = parse_database_name(file)

    # open the database URL
#    try:
#        database_stream = urllib.request.urlopen(database_url)
#    except ValueError as e:
#        print(e)
#        exit(keep_kernel=True)

    # FIXME: need to be more clever about valid tarfiles that aren't gzipped
    # open a temporary directory and dump the tarball in it
    with tempfile.TemporaryDirectory() as tmpdirname:
        try:
            path = fetch_database(database_url, tmpdirname)
        except TypeError as err:
            print(err)
            sys.exit(1)

        # print('created temporary directory', tmpdirname)
        tar = tarfile.open(path, mode="r")
        tar.extractall(path=tmpdirname)
        tar.close()
        #  get parsers for endmembers and phases
        efiles, endmembers = get_files_parsers(tmpdirname, '.emml')
        pfiles, phases = get_files_parsers(tmpdirname, '.phml')

    # return a dictionary of lists of endmembers and phases.
    edict = {e.name: e for e in endmembers}
    pdict = {p.name: p for p in phases}
    return {'endmembers': edict, 'phases': pdict}


def get_phase_dict(phase, db_dict):
    """ return a dictionary of information from each phase file

        db_dict: dictionary of endmember and phase parsers from thermodyanmic
        database

        FIXME:  needs better error checking but should raise a KeyError if
        the phase is not in the database
    """
    phase_dict = {}
    phase_dict['name'] = db_dict['phases'][phase].name
    phase_dict['abbrev'] = db_dict['phases'][phase].phase_abbrev
    phase_dict['endmembers'] = db_dict['phases'][phase].endmembers
    phase_dict['formula'] = [db_dict['endmembers'][e].formula if e != 'SWIM_water'
                             else 'H2O' for e in phase_dict['endmembers']]

    def condensed(x):
        return x.title().replace("(1)", "").replace("(", "").replace(")", "")

    phase_dict['formula'] = [condensed(f) for f in phase_dict['formula']]
    phase_dict['Mik'] = [mm.Formula(formula).mass
                         for formula in phase_dict['formula']]
    return phase_dict


def init_dictionary():
    """Initializes dictionary with reaction name, """
    rx_dict = {}
    rx_dict['name'] = libspud.get_option('name/name')
    rx_dict['database'] = libspud.get_option('database')

    # Number of reactions
    rx_dict["J"] = libspud.option_count('reaction')

    # Local and global dicts
    rx_dict['local_dict'] = {}
    rx_dict['global_dict'] = {}
    rx_dict['global_dict']["sym"] = sym

    return rx_dict


def read_reactions(file):
    """Reads a SPuD derived reaction description file (.rxml file)
    and returns a nested python dictionary of data, phases and
    endmembers for

    On input file is the name of an .rxml files
    On output, the function returns a python dictionary

    Uses the libspud to read spud files

    Parameters
    ----------
    file : string containing name of endmember markup language file (.emml)



Returns
    -------
    dict : Nested dictionary with fields
        ['name']
        ['filename'] name of rxml file
        ['database'] url of thermodynamic database
        ['phases'] [ list of  phase dictionaries ]
        ['reactions'] [ list of reaction dictionaries ]

    phase dictionary has structure
        phase['name']
        phase['endmembers'] [ list of endmembers ]
        phase['formulas'] [ list of endmember formulas]
        phase['Mik'] [ list of molecular masses for each endmember]

    reaction dictionary  has structure
        ['name']
        ['reactants'] [list of reactant tuples]
        ['products'] [list of product tuples ] (but apparently only need
        the sum of them)

    each tuple consists of (formula,phase,endmember) where formula is the
    endmember formula


    Examples
    --------
    >>> from thermocodegen.spudio.reaction import read_reactions
    >>> rx_dict = read_reactions('file.rxml')
    >>> print (rx_dict)

    """

    # make file paths unique
    dirname, basename = os.path.split(os.path.abspath(file))

    # Extract all parameters
    parameters = optionparsers.parse_all_parameters(file)

    # load .rxml files (will raise exception if no appropriate file)
    libspud.load_options(file)

    # initialize dictionary with name, formula, model and any global parameters
    rx_dict = init_dictionary()

    # add filename to dictionary
    rx_dict['filename'] = basename
    ##FIXED: phase_names is not included in the dictionary created by codegen.Reaction._from_database)
    #rx_dict['phase_names'] = parse_phase_names()
    phase_names = parse_phase_names()

    # attach all parameters
    rx_dict['parameters'] = parameters

    # All parameters are assumed to be active
    s, a = optionparsers.set_subs_parameters(parameters, active='All')
    rx_dict['active_parameters'] = a
    rx_dict['subs_dict'] = s

    # clear the options file for safety
    libspud.clear_options()

    # get database dictionary of parsers
    db = get_parsers_database(file)
    #rx_dict['phases'] = [get_phase_dict(p, db) for p in rx_dict['phase_phase_namesnames']]
    rx_dict['phases'] = [get_phase_dict(p, db) for p in phase_names]


    # Set number of phases
    rx_dict['N'] = len(rx_dict['phases'])

    # Set max number of endmembers (Nmax)
    Kmax = 0
    for ph in rx_dict['phases']:
        ems = len(ph['endmembers'])
        if ems > Kmax:
            Kmax = ems

    rx_dict['Kmax'] = Kmax
    libspud.clear_options()

    # Reload .rxml file
    libspud.load_options(file)

    # Parse variables and external functions associated with rate vector Gamma
    model = parse_reaction_rate_model(rx_dict)
    rx_dict['model'] = model

    # set up a global dictionary of symbols for resolving symbols in exec
    global_dict = {}
    global_dict['sym'] = sym
    global_dict.update(sym.__dict__)
    global_dict.update({parameter['name']: parameter['symbol'] for
                        parameter in parameters})
    global_dict.update({variable['name']: variable['symbol'] for variable
                        in rx_dict['model']['variables']})
    global_dict.update({function['name']: function['symbol'] for function
                        in rx_dict['model']['functions']})

    # get  reaction dictionaries
    rx_dict['reactions'] = parse_reactions(file, db)

    # Loop over reactions and add rate expression to dictionary
    for i in range(rx_dict['J']):
        R_i = parse_reaction_rate('reaction[{}]'.format(i), rx_dict,
                                  global_dict)
        rx_dict['reactions'][i]['rate'] = R_i

    # clear options for safety
    libspud.clear_options()
    return rx_dict


def write_reactions(model_dict, filename=None, path=None):
    """Writes the data in a reactions model dictionary to an .rxml file.

    Parameters
    ----------
    model_dict : dict
        a dictionary of data for the set of reactions in questions. This
        includes a list of reactions, their reactans and products, SymPy
        expressions for the rate terms, and various metadata.

    filename : str, optional
        the desired filename for the output .rxml file. If 'None', this
        defaults to the 'name' field in the model_dict.

    path : str, optional
        path locating the output location. If 'None', the file will be written
        to the current directory.

    Returns
    -------
    None
    """

    md = model_dict.copy()

    # Path to templates
    tcg_home = os.environ.get('THERMOCODEGEN_HOME')
    if tcg_home is None:
        raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

    template_dir = tcg_home + '/share/thermocodegen/templates/spud'

    spud_template = template_dir + '/Reaction.rxml'
    libspud.load_options(spud_template)

    # Write name, database and reference attributes
    optionparsers.write_attribute_name('/name', md['name'])
    optionparsers.write_str('/database', md['database'])
    optionparsers.write_str('/reference', md['reference'])

    # Write phases
    phase_names = [phase['name'] for phase in md['phases']]
    write_phases(phase_names)

    # Add global parameters
    for p in md['parameters']:
        spud_path = '/global_parameters/parameter::{}'.format(p['name'])
        optionparsers.write_parameter(spud_path, p)

    # Write reactions
    for i, rxn in enumerate(md['reactions']):
        write_reaction(rxn, i)

    # Output options file
    if filename is None:
        filename = md['name'] + '.rxml'

    if path is not None:
        filename = "{}/{}".format(path, filename)

    libspud.write_options(filename)
    libspud.clear_options()


def write_phases(phase_names):
    """Writes each provided phase to the .rxml file"""
    if not phase_names or not all(isinstance(s, str) for s in phase_names):
        raise TypeError('Input must be a list of length > 0'
                        ' containing strings only.')

    for k, phase in enumerate(phase_names):
        try:
            if k == 0:
                libspud.set_option_attribute('/phase/name', phase)
            else:
                libspud.add_option('/phase::{}'.format(phase))
        except libspud.SpudNewKeyWarning:
            pass


def add_reaction_members(spud_path, member_list, member_type):
    """Writes the name, phase and specific endmember for each of the components
     in the reactants and products."""

    assert member_type in ('reactant', 'product'), 'Member type must be'\
                                                   'reactant or product'

    keys = ['name', 'phase', 'endmember']
    for k, m in enumerate(member_list):
        if k == 0:
            member_path = '{}/{}'.format(spud_path, member_type)
        else:
            member_path = '{}/{}::{}'.format(spud_path, member_type, m['name'])
            try:
                libspud.add_option(member_path)
            except libspud.SpudNewKeyWarning:
                pass
        for key in keys:
            try:
                libspud.set_option_attribute(member_path + '/' + key, m[key])
            except libspud.SpudNewKeyWarning:
                pass


# def write_reaction(name, reactants, products, parameters, expression, index):
def write_reaction(reaction_dict, index):
    """Writes the reaction appearing at the given 'index' in the reaction
    dictionary."""

    name = reaction_dict['name']

    try:
        if index == 0:
            spud_path = '/reaction'
            libspud.set_option_attribute(spud_path + '/name', name)
        else:
            spud_path = '/reaction::{}'.format(name)
            libspud.add_option(spud_path)
    except libspud.SpudNewKeyWarning:
        pass

    reactants = reaction_dict['reactants']
    products = reaction_dict['products']
    expression = reaction_dict['expression']

    # Add reactants and products
    add_reaction_members(spud_path, reactants, 'reactant')
    add_reaction_members(spud_path, products, 'product')

    # Expression for reaction rate
    write_reaction_rate(name, spud_path, expression)


def write_reaction_rate(name, spud_path, rate):
    """Writes the SymPy expression for the reaction rate."""
    try:
        libspud.add_option(spud_path)
    except libspud.SpudNewKeyWarning as err:
        pass

    # Add expression
    expression = '{} = (\n{}\n)'.format(name, textwrap.fill(str(rate)))
    optionparsers.write_expression(spud_path, expression)


if __name__ == "__main__":
    # valid_dict = get_valid_reactions()

    rx_dict = read_reactions('fo_fa_binary.rxml')
    print(rx_dict)

    # dictD = read_endmember('test_GD.emml')

#############
#   functionalized version of spud-update options from spud
#############

import os
import sys

import diamond.debug as debug
import diamond.schema as schema


def schemaerr():
	debug.deprint(
		"Have you registered it in /usr/share/diamond/schemata, /etc/diamond/schemata, $HOME/.diamond/schemata", 0)
	debug.deprint("or a schemata directory beneath a location listed in the environment variable $DIAMOND_CONFIG_PATH?",
	              0)
	debug.deprint(
		"To register a schema, place a file in one of those directories, and let its name be the suffix of your language.",
		0)
	debug.deprint("The file should consist of:", 0)
	debug.deprint(" A Verbal Description Of The Language Purpose", 0)
	debug.deprint(" alias1=/path/to/schema1/file.rng", 0)
	debug.deprint(" alias2=/path/to/schema2/file.rng", 0)
	sys.exit(1)


def validate_spudfile(file, verbose=False):
	"""
	validate and update spudfile against schema using diamond

	:param file: str
		filename including schema extension (e.g. endmember.emml, phase.phml, reaction.rxml
	:param verbose: bool
		set debug
	:return: bool
		returns true if spudfile is valid and updated
	"""

	if not verbose:
		debug.SetDebugLevel(0)

	filename = os.path.abspath(file)
	ext = filename.split('.')[-1]

	import diamond.config as config
	# only import config after the debug level has been set

	if len(config.schemata) == 0:
		debug.deprint("Could not find a schema file.", 0)
		schemaerr()

	#print(config.schemata[ext])
	extdict = {}
	try:
		extdict[ext] = config.schemata[ext][1][None]
	except:
		debug.deprint("Could not find schema matching suffix {}".format(ext), 0)
		schemaerr()

	# cache parsed schema files
	schemadict = {}
	for k, v in extdict.items():
		schemadict[k] = schema.Schema(v)

	debug.dprint("Processing " + str(filename), 1)
	sch = schemadict[ext]

	# Read the file and check that either the file is valid, or diamond.schema
	# can make the file valid by adding in the missing elements
	optionsTree = sch.read(filename)
	lost_eles, added_eles, lost_attrs, added_attrs = sch.read_errors()
	if len(lost_eles) + len(lost_attrs) > 0 or not optionsTree.valid:
		isvalid = False
		debug.deprint(str(filename) + ": Invalid", 0)
		debug.deprint(str(filename) + " errors: " + str((lost_eles, added_eles, lost_attrs, added_attrs)), 1)
	else:
		optionsTree.write(filename)
		isvalid = True

	return isvalid

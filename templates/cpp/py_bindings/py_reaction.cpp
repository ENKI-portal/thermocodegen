py::class_<${name},  std::shared_ptr<${name}>>(m, "${name}", py::module_local())
    .def(py::init<>())
    .def("name", &${name}::name)
    .def("tcg_build_version", &${name}::tcg_build_version, "Version of TCG used to build endmember")
    .def("tcg_build_git_sha", &${name}::tcg_build_git_sha, "Git SHA of TCG used to build endmember")
    .def("tcg_generation_version", &${name}::tcg_generation_version, "Version of TCG used to generate endmember")
    .def("tcg_generation_git_sha", &${name}::tcg_generation_git_sha, "Git SHA of TCG used to generate endmember")
    .def("phases", &${name}::phases)
    .def("zero_C", &${name}::zero_C,"returns zero'd compositional 'matrix' of correct shape for phases and endmembers")
    .def("M", &${name}::M,"compositional 'matrix' M[i][k] of molecular weights of Endmembers in phases")
    .def("nu", &${name}::nu," 'matrix' of molar stoichiometric coefficients nu[j][i][k]")
    .def("nu_m", &${name}::nu_m," 'matrix' of mass weighted stoichiometric coefficients nu_m[j][i][k]")
    .def("A", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::A, py::const_),
        "Vector of Affinities for J reactions",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("A", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& , int > (&${name}::A, py::const_),
        "Affinity for reaction j",py::arg("T"),py::arg("P"),py::arg("C"),py::arg("j"))
    .def("dA_dT", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::dA_dT, py::const_),
        "Vector of derivatives of affinities with respect to Temperature",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("dA_dT", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& , int > (&${name}::dA_dT, py::const_),
        "Derivative of jth affinity with respect to Temperature",py::arg("T"),py::arg("P"),py::arg("C"),py::arg("j"))
    .def("dA_dP", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::dA_dP, py::const_),
        "Vector of derivatives of affinities with respect to Pressure",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("dA_dP", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& , int > (&${name}::dA_dP, py::const_),
        "Derivative of jth affinity with respect to Pressure",py::arg("T"),py::arg("P"),py::arg("C"),py::arg("j"))
    .def("dA_dC", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::dA_dC, py::const_),
        "Derivative of all affinities with respect to all components C[i][k]",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("dAj_dCik", &${name}::dAj_dCik,"derivative of the affinity of reaction J with respect to component k in phase i",
       py::arg("T"),py::arg("P"),py::arg("C"),py::arg("j"),py::arg("i"),py::arg("k"))
    .def("Gamma_i", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>& > (&${name}::Gamma_i, py::const_),
        "Vector of mass transfer rates for each phase",py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"))
    .def("Gamma_i", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, int > (&${name}::Gamma_i, py::const_),
        "Mass transfer rate for phase i",py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"))
    .def("dGamma_i_dT", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>& > (&${name}::dGamma_i_dT, py::const_),
        "Derivative of mass transfer rate for all phases with respect to Temperature",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"))
    .def("dGamma_i_dT", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, int > (&${name}::dGamma_i_dT, py::const_),
        "Derivative of mass transfer rate for phase i with respect to Temperature",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"))
    .def("dGamma_i_dP", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>& > (&${name}::dGamma_i_dP, py::const_),
        "Derivative of mass transfer rate for all phases with respect to Pressure",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"))
    .def("dGamma_i_dP", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, int > (&${name}::dGamma_i_dP, py::const_),
        "Derivative of mass transfer rate for phase i with respect to Pressure",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"))
    .def("dGamma_i_dPhi", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>& > (&${name}::dGamma_i_dPhi, py::const_),
        "Derivative of mass transfer rate for for all phases with respect to phase fraction",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi") )
    .def("dGamma_i_dPhi", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, unsigned int, unsigned int > (&${name}::dGamma_i_dPhi, py::const_),
        "Derivative of mass transfer rate for phase i with respect to phase fraction l",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"),py::arg("l"))
    .def("dGamma_i_dC", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>& > (&${name}::dGamma_i_dC, py::const_),
        "Derivative of phase mass transfer rate for a change in composition ",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"))
    .def("dGamma_i_dC", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, unsigned int, unsigned int, unsigned int > (&${name}::dGamma_i_dC, py::const_),
        "Derivative of mass transfer rate for phase i with respect to component k in phase l",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"),py::arg("l"),py::arg("k"))
    .def("Gamma_ik", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&  > (&${name}::Gamma_ik, py::const_),
        "Vector of vectors of mass transfer rates",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"))
    .def("Gamma_ik", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, int, int > (&${name}::Gamma_ik, py::const_),
        "Mass transfer rate for component k in phase i",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"),py::arg("k"))
    .def("dGamma_ik_dT", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>& > (&${name}::dGamma_ik_dT, py::const_),
        "Derivative of mass transfer rate for every component with respect to Temperature",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi") )
    .def("dGamma_ik_dT", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, int, int > (&${name}::dGamma_ik_dT, py::const_),
        "Derivative of mass transfer rate for component k in phase i with respect to Temperature",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"),py::arg("k"))
    .def("dGamma_ik_dP", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>& > (&${name}::dGamma_ik_dP, py::const_),
        "Derivative of mass transfer rate for component k in phase i with respect to Pressure",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"))
    .def("dGamma_ik_dP", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, int, int > (&${name}::dGamma_ik_dP, py::const_),
        "Derivative of mass transfer rate for component k in phase i with respect to Pressure",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"),py::arg("k"))
    .def("dGamma_ik_dC", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, int  > (&${name}::dGamma_ik_dC, py::const_),
        "Derivative of mass transfer rate component i,k with respect to composition of phase i",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"))
    .def("dGamma_ik_dC", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, unsigned, unsigned, unsigned, unsigned > (&${name}::dGamma_ik_dC, py::const_),
        "Derivative of mass transfer rate for component i,k  with respect to component l,m",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),
        py::arg("i"),py::arg("k"),py::arg("l"),py::arg("m"))
    .def("dGamma_ik_dPhi", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, int  > (&${name}::dGamma_ik_dPhi, py::const_),
        "Derivative of mass transfer rate for all components i phase i  with respect to phase fraction Phi",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),py::arg("i"))
    .def("dGamma_ik_dPhi", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& ,
        std::vector<double>&, unsigned, unsigned, unsigned  > (&${name}::dGamma_ik_dPhi, py::const_),
        "Derivative of mass transfer rate for component i,k with respect to phase fraction l",
        py::arg("T"),py::arg("P"),py::arg("C"),py::arg("Phi"),
        py::arg("i"),py::arg("k"),py::arg("l"))
    .def("rho", py::overload_cast<const double& , const double& , 
        std::vector<std::vector<double> >& > (&${name}::rho, py::const_),
        "density of phases",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("Cp", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::Cp, py::const_),
        "Vector of heat capacities of phases",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("s", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::s, py::const_),
        "Vector of entropies of phases",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("ds_dC", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::ds_dC, py::const_),
        "Vector of ds_dc for each phase of phases",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("alpha", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::alpha, py::const_),
        "Vector of thermal expansivity of phases",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("beta", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::beta, py::const_),
        "Vector of compressibility of phases",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("Mu", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::Mu, py::const_),
        "Matrix of chemical potentials mu_i^k(T,P,C)",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("dMu_dC", py::overload_cast<const double& , const double& , std::vector<std::vector<double> >& > (&${name}::dMu_dC, py::const_),
        "Compositional Jacobian  dmu_i^k_dC(T,P,C)",py::arg("T"),py::arg("P"),py::arg("C"))
    .def("C_to_X",  py::overload_cast<std::vector<std::vector<double> >& > (&${name}::C_to_X, py::const_),
        "convert all weight fractions to mole fractions",py::arg("C"))
    .def("X_to_C",  py::overload_cast<std::vector<std::vector<double> >& > (&${name}::X_to_C, py::const_),
        "convert all mole fractions to weight fractions",py::arg("X"))
    .def("set_parameter", &${name}::set_parameter,"Set value of parameter",py::arg("p"),py::arg("val"))
    .def("get_parameter", &${name}::get_parameter,"Get value of parameter",py::arg("p"),
         py::call_guard<py::scoped_ostream_redirect, py::scoped_estream_redirect>())
    .def("list_parameters", &${name}::list_parameters,"List available parameters",
         py::call_guard<py::scoped_ostream_redirect, py::scoped_estream_redirect>())
    .def("report", &${name}::report,"dump phases and end members",
         py::call_guard<py::scoped_ostream_redirect, py::scoped_estream_redirect>());

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/iostream.h>
#include <pybind11/numpy.h>
#include "tcgversion.h"
${include_files}

namespace py = pybind11;

PYBIND11_MODULE(${module_name}, m) {
  //Return the TCG version at build time
  m.def("tcg_build_version",[]() {
    return TCG_VERSION;
  }, "Return the TCG version at build time");

  //Return the TCG git sha at build time
  m.def("tcg_build_git_sha",[]() {
    return TCG_GIT_SHA;
  }, "Return the TCG git sha at build time");

  //Return the TCG version at generation time
  m.def("tcg_generation_version",[]() {
    return "${tcg_version}";
  }, "Return the TCG version at generation time");

  //Return the TCG git sha at generation time
  m.def("tcg_generation_git_sha",[]() {
    return "${tcg_git_sha}";
  }, "Return the TCG git sha at generation time");

  // Returns a dictionary of phase attributes
  m.def("phase_info",[]() {
    std::vector<std::shared_ptr<Phase> > _phases = ${phases};
    std::vector<std::string> _names, _abbrevs, _types;
    
    for(int i = 0; i < _phases.size(); i++){
      _names.push_back(_phases[i]->name());
      _abbrevs.push_back(_phases[i]->abbrev());
      
      if(_phases[i]->endmember_number() == 1){
        _types.push_back("pure");
      }
      else{
        _types.push_back("solution");
      }
    }
    
    std::map<std::string, std::vector<std::string> > phase_info;    
    phase_info.insert(std::make_pair("ClassName", _names));
    phase_info.insert(std::make_pair("Abbrev", _abbrevs));
    phase_info.insert(std::make_pair("PhaseType", _types));
  
    return phase_info; 
  });

  // EndMembers
  ${pybind_endmembers}

  // Phases
  ${pybind_phases}

  // Reactions
  ${pybind_reactions}
}

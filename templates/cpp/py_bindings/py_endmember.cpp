py::class_<${name}, std::shared_ptr<${name}> >(m, "${name}", py::module_local())
    .def(py::init<>())
    .def("name", &${name}::name, "Name of endmember")
    .def("identifier", &${name}::identifier, "Identifier")
    .def("tcg_build_version", &${name}::tcg_build_version, "Version of TCG used to build endmember")
    .def("tcg_build_git_sha", &${name}::tcg_build_git_sha, "Git SHA of TCG used to build endmember")
    .def("tcg_generation_version", &${name}::tcg_generation_version, "Version of TCG used to generate endmember")
    .def("tcg_generation_git_sha", &${name}::tcg_generation_git_sha, "Git SHA of TCG used to generate endmember")
    .def("formula", &${name}::formula, "Chemical formula of endmember")
    .def("mw", &${name}::molecular_weight, "Molecular weight (g/mol)")
    .def("elements", &${name}::elements, "Vector of elements")
    
    .def("g", &${name}::G, R"pbdoc(
        Return the Gibbs free energy :math:`G`

        Parameters
        ----------
        T: float
            Temperature (in Kelvin)
        P: float
            Pressure (in bars)

        Returns
        -------
        G: float
    )pbdoc", py::arg("T"), py::arg("P"))
    
    .def("dgdt", &${name}::dGdT, R"pbdoc(
        Return the Gibbs free energy derivative :math:`\partial G / \partial T`

        Parameters
        ----------
        T: float
            Temperature (in Kelvin)
        P: float
            Pressure (in bars)

        Returns
        -------
        dGdT: float
    )pbdoc", py::arg("T"), py::arg("P"))
    
    .def("dgdp", &${name}::dGdP, R"pbdoc(
        Return the Gibbs free energy derivative :math:`\partial G / \partial P`

        Parameters
        ----------
        T: float
            Temperature (in Kelvin)
        P: float
            Pressure (in bars)

        Returns
        -------
        dGdP: float
    )pbdoc", py::arg("T"), py::arg("P"))
    
    .def("d2gdt2", &${name}::d2GdT2, R"pbdoc(
        Return the Gibbs free energy second derivative :math:`\partial^2 G / \partial T^2`

        Parameters
        ----------
        T: float
            Temperature (in Kelvin)
        P: float
            Pressure (in bars)

        Returns
        -------
        d2GdT2: float
    )pbdoc", py::arg("T"), py::arg("P"))
    
    .def("d2gdtdp", &${name}::d2GdTdP, "Gibbs free energy derivative d2gdtdp", py::arg("T"), py::arg("P"))
    .def("d2gdp2", &${name}::d2GdP2, "Gibbs free energy derivative d2gdp2", py::arg("T"), py::arg("P"))
    .def("d3gdt3", &${name}::d3GdT3, "Gibbs free energy derivative d3gdt3", py::arg("T"), py::arg("P"))
    .def("d3gdt2dp", &${name}::d3GdT2dP,"Gibbs free energy derivative d3gdt2dp", py::arg("T"), py::arg("P"))
    .def("d3gdtdp2", &${name}::d3GdTdP2,"Gibbs free energy derivative d3gdtdp2", py::arg("T"), py::arg("P"))
    .def("d3gdp3", &${name}::d3GdP3, "Gibbs free energy derivative d3gdp3", py::arg("T"), py::arg("P"))
    .def("s", &${name}::S, "Endmember entropy (J/K)", py::arg("T"), py::arg("P"))
    .def("v", &${name}::V, "Endmember volume", py::arg("T"), py::arg("P"))
    .def("cv", &${name}::Cv, "Heat capacity at constant volume", py::arg("T"), py::arg("P"))
    .def("cp", &${name}::Cp, "Heat capacity a constant pressure", py::arg("T"), py::arg("P"))
    .def("dcpdt", &${name}::dCpdT, py::arg("T"), py::arg("P"))
    .def("alpha", &${name}::alpha, "Coefficient of thermal expansion", py::arg("T"), py::arg("P"))
    .def("beta", &${name}::beta, "Bulk compressibility", py::arg("T"), py::arg("P"))
    .def("K", &${name}::K, "Bulk modulus", py::arg("T"), py::arg("P"))
    .def("Kp", &${name}::Kp,"Derivative of K with respect to pressure", py::arg("T"), py::arg("P"))
    .def("get_param_number", &${name}::get_param_number, "Number of active parameters")
    .def("get_param_names", &${name}::get_param_names, "Active parameter names")
    .def("get_param_units", &${name}::get_param_units, "Active parameter units")
    .def("get_param_values", py::overload_cast<>(&${name}::get_param_values), "Get active parameter values")
    .def("set_param_values", &${name}::set_param_values, "Set active parameter values", py::arg("values"))
    .def("get_param_value", &${name}::get_param_value, "Return value for a particular active parameter", py::arg("index"))
    .def("set_param_value", &${name}::set_param_value, "Set value for a particular active parameter", py::arg("index"), py::arg("value"))
    .def("dparam_g", &${name}::dparam_g, "dparam_g", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_dgdt", &${name}::dparam_dgdt, "dparam_dgdt", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_dgdp", &${name}::dparam_dgdp, "dparam_dgdp", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_d2gdt2", &${name}::dparam_d2gdt2, "dparam_d2gdt2", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_d2gdtdp", &${name}::dparam_d2gdtdp, "dparam_d2gdtdp", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_d2gdp2", &${name}::dparam_d2gdp2, "dparam_d2gdp2", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_d3gdt3", &${name}::dparam_d3gdt3, "dparam_d3gdt3", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_d3gdt2dp", &${name}::dparam_d3gdt2dp, "dparam_d3gdt2dp", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_d3gdtdp2", &${name}::dparam_d3gdtdp2, "dparam_d3gdtdp2", py::arg("T"), py::arg("P"), py::arg("index"))
    .def("dparam_d3gdp3", &${name}::dparam_d3gdp3, "dparam_d3gdp3", py::arg("T"), py::arg("P"), py::arg("index"));

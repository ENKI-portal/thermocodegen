#include <math.h>
#include <numeric>
#include <algorithm>
#include <functional>
#include "phases/${phasename}.h"
#include "tcgversion.h"

//-----------------------------------------------------------------------------
${phasename}::${phasename}()
: _endmembers(${endmembers})
{
  // Set number of components
  C = _endmembers.size();

  // Assign vector of molecular weights for each component
  M = ${phasename}::get_M();
  // Calculate inverse Masses
  iM.resize(C, 0.);
  for (int i = 0; i < C; i++)
    iM[i] = 1./M[i];

  // allocate temporary storage vectors for vecs and  matrices
  _tmp.resize(C, 0.);
  _result.resize(C, 0.);
  _result_mat.resize(C*(C+1)/2, 0.);
  // temporary element vector
  _elm.resize(106, 0.);
  // compositional Jacobian
  _dmu_dc.resize(C);
  for (int i = 0; i < C; i++ )
  {
    _dmu_dc[i].resize(C, 0.);
  }
}
//-----------------------------------------------------------------------------
${phasename}::~${phasename}()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
std::string ${phasename}::identifier()
{
    std::string _str(${C_name}_calib_identifier());
    return _str;
}
//-----------------------------------------------------------------------------
std::string ${phasename}::name()
{
  std::string _str(${C_name}_calib_name());
  return _str;
}
//-----------------------------------------------------------------------------
std::string ${phasename}::tcg_build_version()
{
    return TCG_VERSION;
}
//-----------------------------------------------------------------------------
std::string ${phasename}::tcg_build_git_sha()
{
    return TCG_GIT_SHA;
}
//-----------------------------------------------------------------------------
std::string ${phasename}::tcg_generation_version()
{
    return "${tcg_version}";
}
//-----------------------------------------------------------------------------
std::string ${phasename}::tcg_generation_git_sha()
{
    return "${tcg_git_sha}";
}
//-----------------------------------------------------------------------------
std::string ${phasename}::formula(
    const double &T, const double &P,
    std::vector<double> &n)
{
  std::string _str(${C_name}_calib_formula(T,P,n.data()));
  return _str;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::conv_elm_to_moles(std::vector<double>& e)
{
  std::vector<double> n;
  double *_n = ${C_name}_calib_conv_elm_to_moles(e.data());
  n.assign(_n, _n + C);
  return n;
}
//-----------------------------------------------------------------------------
double ${phasename}::conv_elm_to_tot_moles(std::vector<double>& e)
{
  _tmp = conv_elm_to_moles(e);
  double result = std::accumulate(_tmp.begin(), _tmp.end(), 0.0);
  return result;
}
//-----------------------------------------------------------------------------
double ${phasename}::conv_elm_to_tot_grams(std::vector<double>& e)
{
  _tmp  = conv_elm_to_moles(e);
  double result = std::inner_product(_tmp.begin(), _tmp.end(), M.begin(), 0.0);
  return result;
}

//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::conv_moles_to_elm(std::vector<double>& n)
{
  std::vector<double> result(106, 0.);
  for (int i = 0; i< C; i++)
  {
    _elm  = endmember_elements(i);
    for (int j = 0; j < result.size(); j++)
    {
    //FIXME:: could use std::transform for this but its fugly
            result[j] += n[i]*_elm[j];
    }
  }
  return result;
}
//-----------------------------------------------------------------------------
double ${phasename}::conv_moles_to_tot_moles(std::vector<double>& n)
{
  return std::accumulate(n.begin(), n.end(), 0.0);
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::conv_moles_to_mole_frac(std::vector<double>& n)
{
  double n_tot = conv_moles_to_tot_moles(n);
  for (int i = 0; i < C; i++)
  {
    _tmp[i] = n[i]/n_tot;
  }
  return _tmp;
}

//-----------------------------------------------------------------------------
int ${phasename}::test_moles(std::vector<double>& n)
{
  return ${C_name}_calib_test_moles(n.data());
}
//-----------------------------------------------------------------------------
int ${phasename}::endmember_number() const
{
  return _endmembers.size();
}
//-----------------------------------------------------------------------------
std::string ${phasename}::endmember_name(const int &i) const
{
  return (*_endmembers[i]).name();
}
//-----------------------------------------------------------------------------
std::string ${phasename}::endmember_formula(const int &i) const
{
  return (*_endmembers[i]).formula();
}
//-----------------------------------------------------------------------------
double ${phasename}::endmember_mw(const int &i) const
{
  return (*_endmembers[i]).molecular_weight();
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::endmember_elements(const int &i) const
{
  return (*_endmembers[i]).elements();
}
//-----------------------------------------------------------------------------
int ${phasename}::species_number() const
{
  return _endmembers.size();
}
//-----------------------------------------------------------------------------
std::string ${phasename}::species_name(const int &i) const
{
  return (*_endmembers[i]).name();
}
//-----------------------------------------------------------------------------
std::string ${phasename}::species_formula(const int &i) const
{
  return (*_endmembers[i]).formula();
}
//-----------------------------------------------------------------------------
double ${phasename}::species_mw(const int &i) const
{
  return (*_endmembers[i]).molecular_weight();
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::species_elements(const int &i) const
{
  return (*_endmembers[i]).elements();
}
//-----------------------------------------------------------------------------
double ${phasename}::g(
    const double &T, const double &P,
    std::vector<double>& n) const
{
  return ${C_name}_calib_g(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::dgdt(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_dgdt(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::dgdp(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_dgdp(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::d2gdt2(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_d2gdt2(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::d2gdtdp(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_d2gdtdp(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::d2gdp2(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_d2gdp2(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::d3gdt3(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_d3gdt3(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::d3gdt2dp(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_d3gdt2dp(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::d3gdtdp2(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_d3gdtdp2(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::d3gdp3(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_d3gdp3(T,P,n.data());
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dgdn(
    const double &T, const double &P, std::vector<double> &n) const
{
  ${C_name}_calib_dgdn(T,P,n.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d2gdndt(
    const double &T, const double &P, std::vector<double> &n) const
{
  ${C_name}_calib_d2gdndt(T,P,n.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d2gdndp(
    const double &T, const double &P, std::vector<double> &n) const
{
  ${C_name}_calib_d2gdndp(T,P,n.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d2gdn2(
    const double &T, const double &P, std::vector<double> &n) const
{
  //std::vector<double> _d2gdn2(C*(C+1)/2);
  ${C_name}_calib_d2gdn2(T,P,n.data(),_result_mat.data());
  return _result_mat;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d3gdndt2(
    const double &T, const double &P, std::vector<double> &n) const
{
  ${C_name}_calib_d3gdndt2(T,P,n.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d3gdndtdp(
    const double &T, const double &P, std::vector<double> &n) const
{
  ${C_name}_calib_d3gdndtdp(T,P,n.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d3gdn2dt(
    const double &T, const double &P, std::vector<double> &n) const
{
  ${C_name}_calib_d3gdn2dt(T,P,n.data(),_result_mat.data());
  return _result_mat;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d3gdndp2(
    const double &T, const double &P, std::vector<double> &n) const
{
  ${C_name}_calib_d3gdndp2(T,P,n.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d3gdn2dp(
    const double &T, const double &P, std::vector<double> &n) const
{
  ${C_name}_calib_d3gdn2dp(T,P,n.data(),_result_mat.data());
  return _result_mat;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d3gdn3(
    const double &T, const double &P, std::vector<double> &n) const
{
  //FIXME: see if we need this vector as well
  std::vector<double> _d3gdn3(C*(C+1)*(C+2)/6);
  ${C_name}_calib_d3gdn3(T,P,n.data(),_d3gdn3.data());
  return _d3gdn3;
}
//-----------------------------------------------------------------------------
double ${phasename}::v(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_v(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::s(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_s(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::alpha(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_alpha(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::cv(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_cv(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::cp(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_cp(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::dcpdt(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_dcpdt(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::beta(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_beta(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::K(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_K(T,P,n.data());
}
//-----------------------------------------------------------------------------
double ${phasename}::Kp(
    const double &T, const double &P,
    std::vector<double> &n) const
{
  return ${C_name}_calib_Kp(T,P,n.data());
}
//-----------------------------------------------------------------------------
int ${phasename}::get_param_number()
{
   return ${C_name}_get_param_number();
}
//-----------------------------------------------------------------------------
std::vector<std::string> ${phasename}::get_param_names()
{
  std::vector<std::string> _param_names;
  const char **p = ${C_name}_get_param_names();
  _param_names.assign(p, p + ${C_name}_get_param_number());
  return _param_names;
}
//-----------------------------------------------------------------------------
std::vector<std::string> ${phasename}::get_param_units()
{
  std::vector<std::string> _param_units;
  const char **p = ${C_name}_get_param_units();
  _param_units.assign(p, p + ${C_name}_get_param_number());
  return _param_units;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::get_param_values()
{
  std::vector<double> values(${C_name}_get_param_number());
  double* v = values.data();
  double** v_ptr = &v;
  ${C_name}_get_param_values(v_ptr);
  return values;
}
//-----------------------------------------------------------------------------
void ${phasename}::get_param_values(std::vector<double>& values)
{
  double* v = values.data();
  double** v_ptr = &v;
  ${C_name}_get_param_values(v_ptr);
}
//-----------------------------------------------------------------------------
int ${phasename}::set_param_values(std::vector<double>& values)
{
  ${C_name}_set_param_values(values.data());
  return 1;
}
//-----------------------------------------------------------------------------
double ${phasename}::get_param_value(int& index)
{
  return ${C_name}_get_param_value(index);
}
//-----------------------------------------------------------------------------
int ${phasename}::set_param_value(int& index, double& value)
{
  return ${C_name}_set_param_value(index,value);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_g(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_g(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_dgdt(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_dgdt(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_dgdp(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_dgdp(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dparam_dgdn(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  ${C_name}_dparam_dgdn(T,P,n.data(),index,_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_d2gdt2(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_d2gdt2(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_d2gdtdp(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_d2gdtdp(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_d2gdp2(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_d2gdp2(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_d3gdt3(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_d3gdt3(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_d3gdt2dp(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_d3gdt2dp(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_d3gdtdp2(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_d3gdtdp2(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
double ${phasename}::dparam_d3gdp3(
    double& T, double& P,
    std::vector<double> &n, int& index)
{
  return ${C_name}_dparam_d3gdp3(T,P,n.data(),index);
}
//-----------------------------------------------------------------------------
std::vector<std::shared_ptr<EndMember> > ${phasename}::endmembers() const
{
  return _endmembers;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::get_M() const
{
  std::vector<double> M(C);

  for(int k = 0; k < C; k++)
  {
    M[k] = (*_endmembers[k]).molecular_weight();
  }
  return M;
}
//-----------------------------------------------------------------------------
std::string ${phasename}::abbrev() const
{
  return "${abbrev}";
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::mu(
    const double &T, const double &P, std::vector<double> &x) const
{
  ${C_name}_calib_dgdn(T,P,x.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dmu_dT(
    const double &T, const double &P, std::vector<double> &x) const
{
  ${C_name}_calib_d2gdndt(T,P,x.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dmu_dP(
    const double &T, const double &P, std::vector<double> &x) const
{
  ${C_name}_calib_d2gdndp(T,P,x.data(),_result.data());
  return _result;
}
//-----------------------------------------------------------------------------
std::vector<std::vector<double> > ${phasename}::dmu_dc(
    const double &T, const double &P, std::vector<double> &c) const
{
  // if C < 1 _dmu_dc = {{0.}} by initialization already
  if ( C > 1 )
  {
    // Convert c to x and calculate c.(1/M)
    c_to_x(c, _tmp);
    double cdotiM = std::inner_product(c.begin(), c.end(), iM.begin(), 0.0);

    // extract compressed dmu/dn matrix from coder
    ${C_name}_calib_d2gdn2(T,P,_tmp.data(),_result_mat.data());

    // loop over components
    for (int j = 0; j < C; j++)
    {
        // map from compressed matrix to calculate \grad_n mu^j
      for(int k = 0; k < C; k++)
      {
        int m = (j <= k) ? j*(C - 1) - (j - 1)*j/2 + k : k*(C - 1) - (k - 1)*k/2 + j ;
        _dmu_dc[j][k] = _result_mat[m];
      }
      // right multiply by  jacobian dx/dc = 1/cdotiM * ( I - x 1^T) diag(iM)
      double gdotx = std::inner_product(_dmu_dc[j].begin(), _dmu_dc[j].end(), _tmp.begin(), 0.0);
      for (int k = 0; k < C; k++)
      {
        _dmu_dc[j][k] = ( _dmu_dc[j][k] - gdotx ) * iM[k] /cdotiM;
      }
    }
  }
  return _dmu_dc;
}
//-----------------------------------------------------------------------------
double ${phasename}::Mass(std::vector<double> &x) const
{
  //return dot product of x M
  double _M = std::inner_product(x.begin(),x.end(), M.begin(), 0.0);
  return _M;
}
//-----------------------------------------------------------------------------
double ${phasename}::rho(
    const double &T, const double &P,
    std::vector<double> &c) const
{
  c_to_x(c,_tmp);
  double _rho = Mass(_tmp)/v(T,P,_tmp);
  return _rho;
}
//-----------------------------------------------------------------------------
double ${phasename}::drho_dT(
    const double &T, const double &P,
    std::vector<double> &c) const
{
  c_to_x(c, _tmp);
  double _V = v(T,P,_tmp);
  double _drho_dT = -d2gdtdp(T,P,_tmp)*Mass(_tmp)/(_V*_V);
  return _drho_dT;
}
//-----------------------------------------------------------------------------
double ${phasename}::drho_dP(
    const double &T, const double &P,
    std::vector<double> &c) const
{
  c_to_x(c, _tmp);
  double _V = v(T,P,_tmp);
  double _drho_dP = -d2gdp2(T,P,_tmp)*Mass(_tmp)/(_V*_V);
  return _drho_dP;
}
//-----------------------------------------------------------------------------
std::vector<double>  ${phasename}::drho_dc(
    const double &T, const double &P,
    std::vector<double> &c) const
{
  // if C=1, dv_dC will return [0].
  std::vector<double> _drho_dc = dv_dc(T, P, c);
  if ( C > 1 )
  {
    c_to_x(c, _tmp);
    double _V = v(T,P,_tmp);
    double _M = Mass(_tmp);
    double cdotiM = std::inner_product(c.begin(), c.end(), iM.begin(), 0.0);

    for (int k = 0; k < C; k++)
    {
        _drho_dc[k] = (-_M/_V*_drho_dc[k] +  (1. - _M/M[k])/cdotiM)/_V;
    }
  }
  return _drho_dc;
}
//-----------------------------------------------------------------------------
std::vector<double>  ${phasename}::ds_dc(
    const double &T, const double &P, std::vector<double> &c) const
{
  std::vector<double> _ds_dc(C, 0.);
  if ( C > 1 )
  {
    // Convert c to x and calculate c.(1/M)
    c_to_x(c, _tmp);
    double cdotiM = std::inner_product(c.begin(), c.end(), iM.begin(), 0.0);

    // extract  -ds/dx vector from coder
    ${C_name}_calib_d2gdndt(T,P,_tmp.data(),_result.data());

    // right multiply by  jacobian dx/dc = 1/cdotiM * ( I - x 1^T) diag(iM)
    double gdotx = std::inner_product(_result.begin(), _result.end(), _tmp.begin(), 0.0);
    for (int k = 0; k < C; k++)
    {
      _ds_dc[k] = ( -_result[k] + gdotx ) * iM[k] /cdotiM;
    }
  }
  return _ds_dc;
}
//-----------------------------------------------------------------------------
std::vector<double>  ${phasename}::dv_dc(
    const double &T, const double &P, std::vector<double> &c) const
{
  std::vector<double> _dv_dc(C, 0.);
  if ( C > 1 )
  {
    // Convert c to x and calculate c.(1/M)
    c_to_x(c, _tmp);
    double cdotiM = std::inner_product(c.begin(), c.end(), iM.begin(), 0.0);

    // extract  dv/dx vector from coder
    ${C_name}_calib_d2gdndp(T,P,_tmp.data(),_result.data());

    // right multiply by  jacobian dx/dc = 1/cdotiM * ( I - x 1^T) diag(iM)
    double gdotx = std::inner_product(_result.begin(), _result.end(), _tmp.begin(), 0.0);
    for (int k = 0; k < C; k++)
    {
      _dv_dc[k] = ( _result[k] - gdotx ) * iM[k] /cdotiM;
    }
  }
  return _dv_dc;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::c_to_x(std::vector<double> &c) const
{
  c_to_x(c, _tmp);
  return _tmp;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::x_to_c(std::vector<double> &x) const
{
  x_to_c(x, _tmp);
  return _tmp;
}
//pass by reference versions
//-----------------------------------------------------------------------------
void  ${phasename}::c_to_x(std::vector<double> &c, std::vector<double> &x) const
{
  // stl version of c to x calculate x_i = c_i/M_i / (sum c_k/M_k)
  std::transform(c.begin(), c.end(), iM.begin(), x.begin(), std::multiplies<double>());
  double sum  = std::accumulate(x.begin(), x.end(), 0.0);
  std::transform(x.begin(), x.end(), x.begin(),
               std::bind(std::divides<double>(), std::placeholders::_1, sum));
}
//-----------------------------------------------------------------------------
void  ${phasename}::x_to_c(std::vector<double> &x, std::vector<double> &c) const
{
  // stl version of x to c calculate c_i = x_i*M_i / (sum x_k*M_k)
  std::transform(x.begin(), x.end(), M.begin(), c.begin(), std::multiplies<double>());
  double sum  = std::accumulate(c.begin(), c.end(), 0.0);
  std::transform(c.begin(), c.end(), c.begin(),
               std::bind(std::divides<double>(), std::placeholders::_1, sum));
}
//-----------------------------------------------------------------------------

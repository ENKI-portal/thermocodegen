#include <math.h>
#include "phases/${phasename}.h"

//-----------------------------------------------------------------------------
${phasename}::${phasename}()
: _name("${name}"), _endmembers(${endmembers})
{
  // Set number of components
  C = _endmembers.size();

  // Assign vector of molecular weights for each component
  M = ${phasename}::get_M();
}
//-----------------------------------------------------------------------------
${phasename}::~${phasename}()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
std::string ${phasename}::name() const
{
  return _name;
}
//-----------------------------------------------------------------------------
std::vector<std::shared_ptr<EndMember> > ${phasename}::endmembers() const
{
  return _endmembers;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::get_M() const
{
  std::vector<double> M(C);

  for(int k = 0; k < C; k++)
  {
    M[k] = (*_endmembers[k]).molecular_weight();
  }
  return M;
}
//-----------------------------------------------------------------------------
double ${phasename}::G(
    const double &T, const double &P,
    const std::vector<double> &n) const
{
  double _G = ${G};
  return _G;
}
//-----------------------------------------------------------------------------
double ${phasename}::dGdT(
    const double &T, const double &P,
    const std::vector<double> &n) const
{
  double _dGdT = ${dGdT};
  return _dGdT;
}
//-----------------------------------------------------------------------------
double ${phasename}::dGdP(
    const double &T, const double &P,
    const std::vector<double> &n) const
{
  double _dGdP = ${dGdP};
  return _dGdP;
}
//-----------------------------------------------------------------------------
double ${phasename}::d2GdT2(
    const double &T, const double &P,
    const std::vector<double> &n) const
{
  double _d2GdT2 = ${d2GdT2};
  return _d2GdT2;
}
//-----------------------------------------------------------------------------
double ${phasename}::d2GdTdP(
    const double &T, const double &P,
    const std::vector<double> &n) const
{
  double _d2GdTdP = ${d2GdTdP};
  return _d2GdTdP;
}
//-----------------------------------------------------------------------------
double ${phasename}::d2GdP2(
    const double &T, const double &P,
    const std::vector<double> &n) const
{
  double _d2GdP2 = ${d2GdP2};
  return _d2GdP2;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::mu(
    const double &T, const double &P,
    const std::vector<double> &x) const
{
  std::vector<double> _mu = {
  ${mu}};
  return _mu;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dmu_dT(
    const double &T, const double &P,
    const std::vector<double> &x) const
{
  std::vector<double> _dmu_dT = {
  ${dmu_dT}};
  return _dmu_dT;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dmu_dP(
    const double &T, const double &P,
    const std::vector<double> &x) const
{
  std::vector<double> _dmu_dP = {
  ${dmu_dP}};
  return _dmu_dP;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dmu_dxi(
    const double &T, const double &P,
    const std::vector<double> &x, const int &i) const
{
  std::vector<double> _dx_dxi = dx_dxi(x, i);

  std::vector<double> _dmu_dxi = {
  ${dmu_dxi}};
  return _dmu_dxi;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d2mu_dPdxi(
    const double &T, const double &P,
    const std::vector<double> &x, const int &i) const
{
  std::vector<double> _dx_dxi = dx_dxi(x, i);

  std::vector<double> _d2mu_dPdxi = {
  ${d2mu_dPdxi}};
  return _d2mu_dPdxi;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dmu_dci(
    const double &T, const double &P,
    const std::vector<double> &c, const int &i) const
{
  std::vector<double> _dmu_dci(C);
  std::vector<double> x = c_to_x(c);
  std::vector<double> _dmu_dxi = dmu_dxi(T, P, x, i);
  double dxi_dci = dxk_dcl(c, i, i);

  for(int k = 0; k < C; k++)
  {
    _dmu_dci[k] = _dmu_dxi[k]*dxi_dci;
  }
  return _dmu_dci;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::d2mu_dPdci(
    const double &T, const double &P,
    const std::vector<double> &c, const int &i) const
{
  std::vector<double> _d2mu_dPdci(C);
  std::vector<double> x = c_to_x(c);
  std::vector<double> _d2mu_dPdxi = d2mu_dPdxi(T, P, x, i);
  double dxi_dci = dxk_dcl(c, i, i);

  for(int k = 0; k < C; k++)
  {
    _d2mu_dPdci[k] = _d2mu_dPdxi[k]*dxi_dci;
  }
  return _d2mu_dPdci;
}
//-----------------------------------------------------------------------------
double ${phasename}::Mass(const std::vector<double> &x) const
{
  double _M = 0.;

  for(int k = 0; k < C; k++)
  {
    _M += x[k]*M[k];
  }
  return _M;
}
//-----------------------------------------------------------------------------
double ${phasename}::V(
    const double &T, const double &P,
    const std::vector<double> &x) const
{
  double _V = dGdP(T, P, x);
  return _V;
}
//-----------------------------------------------------------------------------
double ${phasename}::s(
    const double &T, const double &P,
    const std::vector<double> &x) const
{
  double _s = -dGdT(T, P, x);
  return _s;
}
//-----------------------------------------------------------------------------
double ${phasename}::alpha(
    const double &T, const double &P,
    const std::vector<double> &x) const
{
  double _alpha = d2GdTdP(T, P, x)/dGdP(T, P, x);
  return _alpha;
}
//-----------------------------------------------------------------------------
double ${phasename}::Cp(
    const double &T, const double &P,
    const std::vector<double> &x) const
{
  double _Cp = -T*d2GdT2(T, P, x);
  return _Cp;
}
//-----------------------------------------------------------------------------
double ${phasename}::rho(
    const double &T, const double &P,
    const std::vector<double> &c) const
{
  std::vector<double> x = c_to_x(c);
  double _rho = 0.;

  _rho = Mass(x)/V(T,P,x);

  return _rho;
}
//-----------------------------------------------------------------------------
double ${phasename}::drho_dT(
    const double &T, const double &P,
    const std::vector<double> &c) const
{
  std::vector<double> x = c_to_x(c);
  double _drho_dT = 0.;
  double _V = V(T, P, x);

  for(int k = 0; k < C; k++)
  {
    _drho_dT += x[k]*M[k];
  }
  _drho_dT *= -d2GdTdP(T, P, x)/(_V*_V);

  return _drho_dT;
}
//-----------------------------------------------------------------------------
double ${phasename}::drho_dP(
    const double &T, const double &P,
    const std::vector<double> &c) const
{
  std::vector<double> x = c_to_x(c);
  double _drho_dP = 0.;
  double _V = V(T, P, x);

  for(int k = 0; k < C; k++)
  {
    _drho_dP += x[k]*M[k];
  }
  _drho_dP *= -d2GdP2(T, P, x)/(_V*_V);

  return _drho_dP;
}
//-----------------------------------------------------------------------------
double ${phasename}::drho_dci(
    const double &T, const double &P,
    const std::vector<double> &c,
    const int &i) const
{
  // if a pure phase return 0.
  if (  C == 1 ) {
    return 0.;
  }
  else {
    std::vector<double> x = c_to_x(c);
    std::vector<double> _dmu_dP = dmu_dP(T, P, x);
    double vi = _dmu_dP[i];
    const double eps = 1.e-4;
      
    // density and its derivative with c[i]
    double _rho = Mass(x)/V(T,P,x);
    double _drho_dci = -_rho*(_rho*vi/M[i] - 1.);
    // check for near pure phases and regularize if necessary
    if ((1. - c[i]) > eps) {
      _drho_dci = _drho_dci/(1.-c[i]);
    }
    else {
      _drho_dci = _drho_dci/eps;
    }
    return _drho_dci;
  }
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::dx_dxi(
    const std::vector<double> &x,
    const double &i) const
{
  double epsilon = 1.e-4;
  std::vector<double> _dx_dxi(C);
  if((1. - x[i]) > epsilon)
  {
    for(int k = 0; k < C; k++)
    {
      _dx_dxi[k] = -x[k]/(1. - x[i]);
    }
    // Fix dx_k/dx_i = 1 for k = i
    _dx_dxi[i] = 1.;
    }
  else
  {
    std::fill(_dx_dxi.begin(), _dx_dxi.end(), -1./(C - 1.));
    _dx_dxi[i] = 1.;
  }
  return _dx_dxi;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::c_to_x(const std::vector<double> &c) const
{
  double sum_c_invM = 0.0;
  for(int i = 0; i < C; i++)
  {
    sum_c_invM += c[i]/M[i];
  }

  std::vector<double> x(C);
  for(int i = 0; i < C; i++)
  {
    x[i] = (c[i]/M[i])/sum_c_invM;
  }
  return x;
}
//-----------------------------------------------------------------------------
std::vector<double> ${phasename}::x_to_c(const std::vector<double> &x) const
{
  double sum_x_M = 0.0;
  for(int i = 0; i < C; i++)
  {
    sum_x_M += x[i]*M[i];
  }

  std::vector<double> c(C);
  for(int i = 0; i < C; i++)
  {
    c[i] = (x[i]*M[i])/sum_x_M;
  }
  return c;
}
//-----------------------------------------------------------------------------
double ${phasename}::dxk_dcl(const std::vector<double> &c,
    const int &k, const int &l) const
{
  double _dxk_dcl;
  double sum_c_invM = 0;
  for(int i = 0; i < C; i++)
  {
    sum_c_invM += c[i]/M[i];
  }

  double inv_Mk_sum_c_invM = (1./M[k])/sum_c_invM;
  double xk = c[k]*inv_Mk_sum_c_invM;
  std::vector<double> dc_dck = dx_dxi(c, l);

  double sum_dcdk_invM = 0;
  for(int i = 0; i < C; i++)
  {
    sum_dcdk_invM += dc_dck[i]/M[i];
  }

  double Mk_sum_cinvM_dck = M[k]*sum_dcdk_invM;

  if(l == k)
  {
    _dxk_dcl = inv_Mk_sum_c_invM*(1. - xk*Mk_sum_cinvM_dck);
  }
  else
  {
    _dxk_dcl = -inv_Mk_sum_c_invM*xk*Mk_sum_cinvM_dck;
  }

  return _dxk_dcl;
}
//-----------------------------------------------------------------------------

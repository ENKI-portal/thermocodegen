#ifndef PHASE_H
#define PHASE_H

#include <vector>
#include <memory>

#include "EndMember.h"

// Abstract class for phases
class Phase
{
public:
  // Constructor
  Phase();

  // Destructor
  virtual ~Phase();

  // phase name
  virtual std::string name() const = 0;

  // Vector of molecular weights
  virtual std::vector<double> get_M() const = 0;

  //return pointers to endmembers
  virtual std::vector<std::shared_ptr<EndMember> > endmembers() const = 0;

  // Gibbs free energy
  virtual double G(const double& T, const double& P,
                   const std::vector<double>& n) const = 0;

  // dGdT
  virtual double dGdT(const double& T, const double& P,
                      const std::vector<double>& n) const = 0;

  // dGdP
  virtual double dGdP(const double& T, const double& P,
                      const std::vector<double>& n) const = 0;

  // d2GdT2
  virtual double d2GdT2(const double& T, const double& P,
                        const std::vector<double>& n) const = 0;

  // d2GdTdP
  virtual double d2GdTdP(const double& T, const double& P,
                         const std::vector<double>& n) const = 0;

  // d2GdP2
  virtual double d2GdP2(const double& T, const double& P,
                        const std::vector<double>& n) const = 0;

  // Vector of chemical potentials for each component
  virtual std::vector<double> mu(const double& T, const double& P,
                                 const std::vector<double>& x) const = 0;
  // dmu_dT
  virtual std::vector<double> dmu_dT(const double& T, const double& P,
                                     const std::vector<double>& x) const = 0;

  // dmu_dP
  virtual std::vector<double> dmu_dP(const double& T, const double& P,
                                     const std::vector<double>& x) const = 0;

  // dmu_dxi (where x[i] is the molar fraction of the ith component)
  virtual std::vector<double> dmu_dxi(const double& T, const double& P,
                                      const std::vector<double>& x,
                                      const int &i) const = 0;

  // dmu_dci (where c[i] is the concentration of the ith component)
  virtual std::vector<double> dmu_dci(const double& T, const double& P,
                                      const std::vector<double>& c,
                                      const int &i) const = 0;

  // Molar Mass
  virtual double Mass(const std::vector<double>& x) const = 0;

  // Molar Volume
  virtual double V(const double& T, const double& P,
                   const std::vector<double>& x) const = 0;

  // Molar Entropy
  virtual double s(const double& T, const double& P,
                   const std::vector<double>& x) const = 0;

  // Thermal expansivity
  virtual double alpha(const double& T, const double& P,
                       const std::vector<double>& x) const = 0;

  // Molar Heat capacity
  virtual double Cp(const double& T, const double& P,
                    const std::vector<double>& x) const = 0;

  // Mean density of the phase in mass units
  virtual double rho(const double& T, const double& P,
                     const std::vector<double>& c) const = 0;

  // drho_dT
  virtual double drho_dT(const double& T, const double& P,
                         const std::vector<double>& c) const = 0;

  // drho_dP
  virtual double drho_dP(const double& T, const double& P,
                         const std::vector<double>& c) const = 0;

  // drho_dci
  virtual double drho_dci(const double& T, const double& P,
                          const std::vector<double>& c,
                          const int& i) const = 0;

  // Calculate derivative of vector x with respect to component x[i]
  virtual std::vector<double> dx_dxi(const std::vector<double>& x,
                                     const double& i) const = 0;

  // Returns weight percent concentrations given mole fraction
  virtual std::vector<double> c_to_x(const std::vector<double> &c) const = 0;

  // Returns  mole fraction given weight percent concentrations
  virtual std::vector<double> x_to_c(const std::vector<double> &x) const = 0;

  // Calculates derivative dx_k/dc_l of x[k] with c[l]
  virtual double dxk_dcl(const std::vector<double>& c, const int &k,
                         const int &l) const = 0;
};

#endif

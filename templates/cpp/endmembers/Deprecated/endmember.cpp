#include <math.h>
#include <string>
#include <map>
#include "endmembers/${endmembername}.h"

//-----------------------------------------------------------------------------
${endmembername}::${endmembername}() ${parameters} ${initialized_parameters}
{
  ${constructor};
}
//-----------------------------------------------------------------------------
${endmembername}::~${endmembername}()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::name()
{
  return "${endmembername}";
}
//-----------------------------------------------------------------------------
std::string ${endmembername}::formula()
{
  return "${formula}";
}
//-----------------------------------------------------------------------------
double ${endmembername}::molecular_weight()
{
  return ${molecular_weight};
}
//-----------------------------------------------------------------------------
double ${endmembername}::G(const double &T, const double &P)
{
  double result = ${G};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::dGdT(const double &T, const double &P)
{
  double result = ${dGdT};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::dGdP(const double &T, const double &P)
{
  double result = ${dGdP};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d2GdT2(const double &T, const double &P)
{
  double result = ${d2GdT2};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d2GdTdP(const double &T, const double &P)
{
  double result = ${d2GdTdP};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d2GdP2(const double &T, const double &P)
{
  double result = ${d2GdP2};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d3GdT3(const double &T, const double &P)
{
  double result = ${d3GdT3};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d3GdT2dP(const double &T, const double &P)
{
  double result = ${d3GdT2dP};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d3GdTdP2(const double &T, const double &P)
{
  double result = ${d3GdTdP2};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d3GdP3(const double &T, const double &P)
{
  double result = ${d3GdP3};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::S(const double& T, const double& P)
{
  double result = -${endmembername}::dGdT(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::V(const double& T, const double& P)
{
  double result = ${endmembername}::dGdP(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::dVdT(const double& T, const double& P)
{
  double result = ${endmembername}::d2GdTdP(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::dVdP(const double& T, const double& P)
{
  double result = ${endmembername}::d2GdP2(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::Cv(const double& T, const double& P)
{
  double result = -T*${endmembername}::d2GdT2(T, P);
  double dVdT = ${endmembername}::d2GdTdP(T, P);
  double dVdP = ${endmembername}::d2GdP2(T, P);
  result += T*dVdT*dVdT/dVdP;
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::Cp(const double& T, const double& P)
{
  double result = -T*${endmembername}::d2GdT2(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::dCpdT(const double& T, const double& P)
{
  double result = -T*${endmembername}::d3GdT3(T, P) - ${endmembername}::d2GdT2(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::alpha(const double& T, const double& P)
{
  double result = ${endmembername}::d2GdTdP(T, P)/${endmembername}::dGdP(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::beta(const double& T, const double& P)
{
  double result = -${endmembername}::d2GdP2(T, P)/${endmembername}::dGdP(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::K(const double& T, const double& P)
{
  double result = -${endmembername}::dGdP(T,P)/${endmembername}::d2GdP2(T, P);
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::Kp(const double& T, const double& P)
{
  double result = -${endmembername}::dGdP(T, P)/${endmembername}::d2GdP2(T, P);
  return result;
}
//-----------------------------------------------------------------------------
void ${endmembername}::set_parameter(const std::string& p, const double& val)
{
  *parameters[p] = val;
}
//-----------------------------------------------------------------------------
void ${endmembername}::get_parameter(const std::string& p)
{
  std::cout << p << " = " << *parameters[p] << std::endl;
}
//-----------------------------------------------------------------------------
void ${endmembername}::list_active_parameters()
{
  std::cout << "Active parameters: \n" << std::endl;
  for (auto const& x : parameters)
  {
    std::cout << x.first << " = "  << *x.second << std::endl;
  }
}
//-----------------------------------------------------------------------------
${calibration_src}

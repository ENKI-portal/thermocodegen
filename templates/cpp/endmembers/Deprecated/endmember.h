#ifndef ${endmembername_uppercase}_H
#define ${endmembername_uppercase}_H

#include "EndMember.h"
#include <map>

class ${endmembername}: public EndMember
{
public:
  // Constructor
  ${endmembername}();

  // Destructor
  virtual ~${endmembername}();

  //**************************************************************************
  // EndMember data
  //**************************************************************************

  // EndMember name
  std::string name();

  // Formula
  std::string formula();

  // Molecular weight
  double molecular_weight();

  //**************************************************************************
  // Gibbs Free Energy G and its derivatives as functions of T and P
  //**************************************************************************

  // G
  double G(const double& T, const double& P);

  // dGdT
  double dGdT(const double& T, const double& P);

  // dGdP
  double dGdP(const double& T, const double& P);

  // d2GdT2
  double d2GdT2(const double& T, const double& P);

  // d2GdTdP
  double d2GdTdP(const double& T, const double& P);

  // d2GdP2
  double d2GdP2(const double& T, const double& P);

  // d3GdT3
  double d3GdT3(const double& T, const double& P);

  // d3GdT2dP
  double d3GdT2dP(const double& T, const double& P);

  // d3GdTdP2
  double d3GdTdP2(const double& T, const double& P);

  // d3GdP3
  double d3GdP3(const double& T, const double& P);

  //**************************************************************************
  // Convenience functions of T and P
  //**************************************************************************

  // Entropy
  double S(const double& T, const double& P);

  // Volume
  double V(const double& T, const double& P);

  // dVdT
  double dVdT(const double& T, const double& P);

  // dVdP
  double dVdP(const double& T, const double& P);

  // Heat capacity at constant volume
  double Cv(const double& T, const double& P);

  // Heat capacity at constant pressure
  double Cp(const double& T, const double& P);

  // dCp/dT (derivative of Cp wrt to temperature T)
  double dCpdT(const double& T, const double& P);

  // Volume thermal expansion coefficient
  double alpha(const double& T, const double& P);

  // Isothermal compressibility
  double beta(const double& T, const double& P);

  // Bulk modulus
  double K(const double& T, const double& P);

  // ...
  double Kp(const double& T, const double& P);

  //**************************************************************************
  // Active parameter functions
  //**************************************************************************

  void set_parameter(const std::string& p, const double& val);

  void get_parameter(const std::string& p);

  void list_active_parameters();

${calibration_header}
private:
  // Active parameters
  ${parameter_declarations}

  // Container for pointers to active parameters and associated symbol strings
  std::map<std::string, double*> parameters;
};

#endif

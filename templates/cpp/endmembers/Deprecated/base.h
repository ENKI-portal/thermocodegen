#ifndef ENDMEMBER_H
#define ENDMEMBER_H

#include <iostream>
#include <string>
#include <cmath>

// Abstract class for thermodynamic endmembers
class EndMember
{
public:
  // Constructor
  EndMember();

  // Destructor
  virtual ~EndMember();

  //****************************************************************************
  // EndMember data
  //****************************************************************************

  // Endmember name
  virtual std::string name() = 0;

  // Formula
  virtual std::string formula() = 0;

  // Molecular weight
  virtual double molecular_weight()= 0;

  //**************************************************************************
  // Convenience functions of T and P
  //**************************************************************************

  // Entropy
  virtual double S(const double& T, const double& P) = 0;

  // Volume
  virtual double V(const double& T, const double& P)= 0;

  // dVdT
  virtual double dVdT(const double& T, const double& P)= 0;

  // dVdP
  virtual double dVdP(const double& T, const double& P)= 0;

  // Heat capacity at constant volume
  virtual double Cv(const double& T, const double& P) = 0;

  // Heat capacity at constant pressure
  virtual double Cp(const double& T, const double& P) = 0;

  // dCp/dT (derivative of Cp wrt to temperature T)
  virtual double dCpdT(const double& T, const double& P) = 0;

  // Volume thermal expansion coefficient
  virtual double alpha(const double& T, const double& P) = 0;

  // Isothermal compressibility
  virtual double beta(const double& T, const double& P) = 0;

  // Bulk modulus
  virtual double K(const double& T, const double& P) = 0;

  // ...
  virtual double Kp(const double& T, const double& P) = 0;

  //****************************************************************************
  // Gibbs Free Energy G and its derivatives as functions of T and P
  //****************************************************************************

  // G
  virtual double G(const double& T, const double& P) = 0;

  // dGdT
  virtual double dGdT(const double& T, const double& P) = 0;

  // dGdP
  virtual double dGdP(const double& T, const double& P) = 0;

  // d2GdT2
  virtual double d2GdT2(const double& T, const double& P) = 0;

  // d2GdTdP
  virtual double d2GdTdP(const double& T, const double& P) = 0;

  // d2GdP2
  virtual double d2GdP2(const double& T, const double& P) = 0;

  // d3GdT3
  virtual double d3GdT3(const double& T, const double& P) = 0;

  // d3GdT2dP
  virtual double d3GdT2dP(const double& T, const double& P) = 0;

  // d3GdTdP2
  virtual double d3GdTdP2(const double& T, const double& P) = 0;

  // d3GdP3
  virtual double d3GdP3(const double& T, const double& P) = 0;

};

#endif

#ifndef ${endmembername_uppercase}_H
#define ${endmembername_uppercase}_H

#include "EndMember.h"
#include <map>

extern "C" {
#include "${C_name}.h"
}


class ${endmembername}: public EndMember
{
public:
  // Constructor
  ${endmembername}();

  // Destructor
  virtual ~${endmembername}();

  //**************************************************************************
  // EndMember data
  //**************************************************************************

  // EndMember name
  std::string name() ;

  

  // Formula
  std::string formula() {
    std::string _formula( ${C_name}_formula()) ;
    return _formula;
  };

  // Molecular weight
  double molecular_weight() {
    return ${C_name}_mw();
  };

  //**************************************************************************
  // Gibbs Free Energy G and its derivatives as functions of T and P
  //**************************************************************************

  // G
  double G(const double& T, const double& P) {
    return ${C_name}_g(T,P);
  };



  //**************************************************************************
  // Convenience functions of T and P
  //**************************************************************************

  // Entropy
  double S(const double& T, const double& P) {
    return ${C_name}_s(T,P);
  };


};

#endif

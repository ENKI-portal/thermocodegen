double ${endmembername}::d${param}_G(const double &T, const double &P)
{
  double result = ${dparam_G};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_dGdT(const double &T, const double &P)
{
  double result = ${dparam_dGdT};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_dGdP(const double &T, const double &P)
{
  double result = ${dparam_dGdP};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_d2GdT2(const double &T, const double &P)
{
  double result = ${dparam_d2GdT2};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_d2GdTdP(const double &T, const double &P)
{
  double result = ${dparam_d2GdTdP};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_d2GdP2(const double &T, const double &P)
{
  double result = ${dparam_d2GdP2};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_d3GdT3(const double &T, const double &P)
{
  double result = ${dparam_d3GdT3};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_d3GdT2dP(const double &T, const double &P)
{
  double result = ${dparam_d3GdT2dP};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_d3GdTdP2(const double &T, const double &P)
{
  double result = ${dparam_d3GdTdP2};
  return result;
}
//-----------------------------------------------------------------------------
double ${endmembername}::d${param}_d3GdP3(const double &T, const double &P)
{
  double result = ${dparam_d3GdP3};
  return result;
}
//-----------------------------------------------------------------------------
